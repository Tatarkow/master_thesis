#include <assert.h>

int x, y;

void main() {
	while (x > 0) {
		if (y > 0) {
			y = x + y;
			x = x - 1;
		}
		else {
			y = x - y;
		}
	}

	if (x == y) {
		assert(0);
	}

	// End.
}

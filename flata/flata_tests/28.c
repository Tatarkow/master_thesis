#include <assert.h>

int x, y;

main() {
	while (x > 0) {
		if (y < 0) {
			y = y + 1;
		}
		else if (y == 0) {
			x = x + 2;
			y = 3;
		}
		else {
			x = x - y;
			y = y - 1;
		}
	}

	if (x == y) {
        assert(0);
    }

	// End.
}
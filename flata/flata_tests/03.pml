byte const RAM_SIZE = 32
byte const CACHE_FULL_SIZE = 15

proctype processor() {
	bit cache[CACHE_FULL_SIZE];
	byte ram_address;
	
	// Choose where to access data.
	do
	:: ram_address++;
	:: break;
	od;

	ram_address = ram_address % RAM_SIZE;
	printf("%d\n", ram_address);
}

// proctype monitor() {}

init {
	atomic {
		run processor();
		// run processor();
		// run monitor;
	}
from time import time
from wsummarizer import WSummarizer as ws


def input_loop(x, y, z):
    path = ""

    while x >= 1 and y <= z:
        if x >= 1 and y <= z:
            x = x - 1
            y = y + 1
            path += "A"

    return [x, y, z, path]


start_times = [time()]

x, y, z = ws.create_variables(3)

loop = ws.define_loop([x >= 1, y <= z], [
    ([x >= 1, y <= z], [
        [1, 0, 0, -1],
        [0, 1, 0, 1],
        [0, 0, 1, 0],
    ]),
])

loop.summarize()

end_times = [time()]

for z in range(101):
    start_times.append(time())

    initial_values = [1000, 1, z]
    loop.compute_terminal_values(*initial_values)

    end_times.append(time())

for start, end in zip(start_times, end_times):
    print(end - start)

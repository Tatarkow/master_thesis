(set-logic HORN)

(declare-fun f (Int Int Int Int Int Int) Bool)
(declare-fun g (Int Int Int Int Int Int) Bool)

(assert (forall ((x Int) (y Int) (z Int) (xt Int) (yt Int) (zt Int))
    (=> (and
        (= xt x)
        (= yt y)
        (= zt z)
    ) (f x y z xt yt zt)
)))

; A
(assert (forall ((x Int) (y Int) (z Int) (xt Int) (yt Int) (zt Int) (xtt Int) (ytt Int) (ztt Int))
    (=> (and
        (f x y z xt yt zt)
        (>= xt 1)
        (<= yt zt)
        (= xtt (- xt 1))
        (= ytt (+ yt 1))
        (= ztt zt)
    ) (f x y z xtt ytt ztt)
)))

(assert (forall ((x Int) (y Int) (z Int) (xt Int) (yt Int) (zt Int))
    (=> (and
        (f x y z xt yt zt)
        (not (and (>= xt 1) (<= yt zt)))
    ) (g x y z xt yt zt)
)))

(assert (forall ((x Int) (y Int) (z Int) (xt Int) (yt Int) (zt Int))
    (=> (and
            (g x y z xt yt zt)
            (= x $X)
            (= y $Y)
            (= z $Z)
            (= xt $XT)
            (= yt $YT)
            (= zt $ZT)
        )
        false
    )
))

(check-sat)
(exit)

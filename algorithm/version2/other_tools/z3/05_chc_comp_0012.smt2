(set-logic HORN)

(declare-fun f (Int Int Int Int) Bool)
(declare-fun g (Int Int Int Int) Bool)

(assert (forall ((x Int) (y Int) (xt Int) (yt Int))
    (=> (and
        (= xt x)
        (= yt y)
    ) (f x y xt yt)
)))

; A
(assert (forall ((x Int) (y Int) (xt Int) (yt Int) (xtt Int) (ytt Int))
    (=> (and
        (f x y xt yt)
        (<= xt yt)
        (= xtt (+ xt 2))
        (= ytt (- yt 1))
    ) (f x y xtt ytt)
)))

(assert (forall ((x Int) (y Int) (xt Int) (yt Int))
    (=> (and
        (f x y xt yt)
        (not (<= xt yt))
    ) (g x y xt yt)
)))

(assert (forall ((x Int) (y Int) (xt Int) (yt Int))
    (=> (and
            (g x y xt yt)
            (= x 2)
            (= y 8)
            (= xt 8)
            (= yt 5)
        )
        false
    )
))

(check-sat)
(exit)
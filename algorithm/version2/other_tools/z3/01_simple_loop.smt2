(set-logic HORN)

(declare-fun f (Int Int Int Int) Bool)
(declare-fun g (Int Int Int Int) Bool)

(assert (forall ((x Int) (y Int) (xt Int) (yt Int))
    (=> (and
        (= xt x)
        (= yt y)
    ) (f x y xt yt)
)))

; A
(assert (forall ((x Int) (y Int) (xt Int) (yt Int) (xtt Int) (ytt Int))
    (=> (and
        (f x y xt yt)
        (>= xt 1)
        (>= yt 1)
        (= xtt (- xt 1))
        (= ytt (+ xt yt))
    ) (f x y xtt ytt)
)))

; B
(assert (forall ((x Int) (y Int) (xt Int) (yt Int) (xtt Int) (ytt Int))
    (=> (and
        (f x y xt yt)
        (>= xt 1)
        (<= yt 0)
        (= xtt xt)
        (= ytt (- xt yt))
    ) (f x y xtt ytt)
)))

(assert (forall ((x Int) (y Int) (xt Int) (yt Int))
    (=> (and
        (f x y xt yt)
        (<= xt 0)
    ) (g x y xt yt)
)))

(assert (forall ((x Int) (y Int) (xt Int) (yt Int))
    (=> (and
            (g x y xt yt)
            (= x 12)
            (= y -3)
            (= xt 0)
            (= yt 93)
        )
        false
    )
))

(check-sat)
(exit)
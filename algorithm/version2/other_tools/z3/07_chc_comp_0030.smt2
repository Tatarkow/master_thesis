(set-logic HORN)

(declare-fun f (Int Int Int Int Int Int Int Int) Bool)
(declare-fun g (Int Int Int Int Int Int Int Int) Bool)

(assert (forall ((x Int) (y Int) (z Int) (w Int) (xt Int) (yt Int) (zt Int) (wt Int))
    (=> (and
        (= xt x)
        (= yt y)
        (= zt z)
        (= wt w)
    ) (f x y z w xt yt zt wt)
)))

; A
(assert (forall ((x Int) (y Int) (z Int) (w Int) (xt Int) (yt Int) (zt Int) (wt Int) (xtt Int) (ytt Int) (ztt Int) (wtt Int))
    (=> (and
        (f x y z w xt yt zt wt)
        (>= xt 1)
        (= xtt (- xt yt))
        (= ytt (- yt zt))
        (= ztt (- 0 zt))
        (= wtt (- wt 1))
    ) (f x y z w xtt ytt ztt wtt)
)))

(assert (forall ((x Int) (y Int) (z Int) (w Int) (xt Int) (yt Int) (zt Int) (wt Int))
    (=> (and
        (f x y z w xt yt zt wt)
        (<= xt 0)
    ) (g x y z w xt yt zt wt)
)))

(assert (forall ((x Int) (y Int) (z Int) (w Int) (xt Int) (yt Int) (zt Int) (wt Int))
    (=> (and
            (g x y z w xt yt zt wt)
            (= x 7)
            (= y 1)
            (= z -8)
            (= w 2)
            (= xt -3)
            (= yt 1)
            (= zt -8)
            (= wt 0)
        )
        false
    )
))

(check-sat)
(exit)
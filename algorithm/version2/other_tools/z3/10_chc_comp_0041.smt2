(set-logic HORN)

(declare-fun f (Int Int Int Int Int Int Int Int Int Int) Bool)
(declare-fun g (Int Int Int Int Int Int Int Int Int Int) Bool)

(assert (forall ((x Int) (y Int) (z Int) (w Int) (v Int) (xt Int) (yt Int) (zt Int) (wt Int) (vt Int))
    (=> (and
        (= xt x)
        (= yt y)
        (= zt z)
        (= wt w)
        (= vt v)
    ) (f x y z w v xt yt zt wt vt)
)))

; A
(assert (forall ((x Int) (y Int) (z Int) (w Int) (v Int) (xt Int) (yt Int) (zt Int) (wt Int) (vt Int) (xtt Int) (ytt Int) (ztt Int) (wtt Int) (vtt Int))
    (=> (and
        (f x y z w v xt yt zt wt vt)
        (<= wt (- (* 2 xt) 1))
        (= vt 0)
        (= xtt xt)
        (= ytt (+ yt 1))
        (= ztt zt)
        (= wtt (+ wt 1))
        (= vtt 1)
    ) (f x y z w v xtt ytt ztt wtt vtt)
)))

; A
(assert (forall ((x Int) (y Int) (z Int) (w Int) (v Int) (xt Int) (yt Int) (zt Int) (wt Int) (vt Int) (xtt Int) (ytt Int) (ztt Int) (wtt Int) (vtt Int))
    (=> (and
        (f x y z w v xt yt zt wt vt)
        (<= wt (- (* 2 xt) 1))
        (not (= vt 0))
        (= xtt xt)
        (= ytt yt)
        (= ztt (+ zt 1))
        (= wtt (+ wt 1))
        (= vtt 0)
    ) (f x y z w v xtt ytt ztt wtt vtt)
)))

(assert (forall ((x Int) (y Int) (z Int) (w Int) (v Int) (xt Int) (yt Int) (zt Int) (wt Int) (vt Int))
    (=> (and
        (f x y z w v xt yt zt wt vt)
        (not (<= wt (- (* 2 xt) 1)))
    ) (g x y z w v xt yt zt wt vt)
)))

(assert (forall ((x Int) (y Int) (z Int) (w Int) (v Int) (xt Int) (yt Int) (zt Int) (wt Int) (vt Int))
    (=> (and
            (g x y z w v xt yt zt wt vt)
            (= x -2)
            (= y 0)
            (= z -5)
            (= w -9)
            (= v 2)
            (= xt -2)
            (= yt 2)
            (= zt -2)
            (= wt -4)
            (= vt 0)
        )
        false
    )
))

(check-sat)
(exit)
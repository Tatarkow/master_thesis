(set-logic HORN)

(declare-fun f (Int Int Int Int Int Int Int Int) Bool)
(declare-fun g (Int Int Int Int Int Int Int Int) Bool)

(assert (forall ((x Int) (y Int) (z Int) (w Int) (xt Int) (yt Int) (zt Int) (wt Int))
    (=> (and
        (= xt x)
        (= yt y)
        (= zt z)
        (= wt w)
    ) (f x y z w xt yt zt wt)
)))

; A
(assert (forall ((x Int) (y Int) (z Int) (w Int) (xt Int) (yt Int) (zt Int) (wt Int) (xtt Int) (ytt Int) (ztt Int) (wtt Int))
    (=> (and
        (f x y z w xt yt zt wt)
        (<= xt (- (* 2 yt) 1))
        (= wt 0)
        (= xtt (+ xt 1))
        (= ytt yt)
        (= ztt (+ zt 1))
        (= wtt 1)
    ) (f x y z w xtt ytt ztt wtt)
)))

; A
(assert (forall ((x Int) (y Int) (z Int) (w Int) (xt Int) (yt Int) (zt Int) (wt Int) (xtt Int) (ytt Int) (ztt Int) (wtt Int))
    (=> (and
        (f x y z w xt yt zt wt)
        (<= xt (- (* 2 yt) 1))
        (not (= wt 0))
        (= xtt (+ xt 1))
        (= ytt yt)
        (= ztt zt)
        (= wtt 0)
    ) (f x y z w xtt ytt ztt wtt)
)))

(assert (forall ((x Int) (y Int) (z Int) (w Int) (xt Int) (yt Int) (zt Int) (wt Int))
    (=> (and
        (f x y z w xt yt zt wt)
        (not (<= xt (- (* 2 yt) 1)))
    ) (g x y z w xt yt zt wt)
)))

(assert (forall ((x Int) (y Int) (z Int) (w Int) (xt Int) (yt Int) (zt Int) (wt Int))
    (=> (and
            (g x y z w xt yt zt wt)
            (= x 7)
            (= y 6)
            (= z -7)
            (= w 0)
            (= xt 12)
            (= yt 6)
            (= zt -4)
            (= wt 1)
        )
        false
    )
))

(check-sat)
(exit)
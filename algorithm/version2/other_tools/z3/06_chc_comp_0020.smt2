(set-logic HORN)

(declare-fun f (Int Int Int Int) Bool)
(declare-fun g (Int Int Int Int) Bool)

(assert (forall ((x Int) (y Int) (xt Int) (yt Int))
    (=> (and
        (= xt x)
        (= yt y)
    ) (f x y xt yt)
)))

; A
(assert (forall ((x Int) (y Int) (xt Int) (yt Int) (xtt Int) (ytt Int))
    (=> (and
        (f x y xt yt)
        (<= xt 99)
        (>= xt 51)
        (= xtt (+ xt 1))
        (= ytt (+ yt 1))
    ) (f x y xtt ytt)
)))

; A
(assert (forall ((x Int) (y Int) (xt Int) (yt Int) (xtt Int) (ytt Int))
    (=> (and
        (f x y xt yt)
        (<= xt 99)
        (<= xt 50)
        (= xtt (+ xt 1))
        (= ytt yt)
    ) (f x y xtt ytt)
)))

(assert (forall ((x Int) (y Int) (xt Int) (yt Int))
    (=> (and
        (f x y xt yt)
        (>= xt 100)
    ) (g x y xt yt)
)))

(assert (forall ((x Int) (y Int) (xt Int) (yt Int))
    (=> (and
            (g x y xt yt)
            (= x 90)
            (= y 1)
            (= xt 100)
            (= yt 11)
        )
        false
    )
))

(check-sat)
(exit)
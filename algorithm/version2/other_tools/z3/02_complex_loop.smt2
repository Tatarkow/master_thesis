(set-logic HORN)

(declare-fun f (Int Int Int Int) Bool)
(declare-fun g (Int Int Int Int) Bool)

(assert (forall ((x Int) (y Int) (xt Int) (yt Int))
    (=> (and
        (= xt x)
        (= yt y)
    ) (f x y xt yt)
)))

; A
(assert (forall ((x Int) (y Int) (xt Int) (yt Int) (xtt Int) (ytt Int))
    (=> (and
        (f x y xt yt)
        (<= xt 999)
        (>= xt 0)
        (>= yt 1)
        (= xtt (* 2 xt))
        (= ytt (- yt 1))
    ) (f x y xtt ytt)
)))

; B
(assert (forall ((x Int) (y Int) (xt Int) (yt Int) (xtt Int) (ytt Int))
    (=> (and
        (f x y xt yt)
        (<= xt 999)
        (>= xt 1)
        (<= yt 0)
        (= xtt (- xt 1))
        (= ytt (- yt 2))
    ) (f x y xtt ytt)
)))

; C
(assert (forall ((x Int) (y Int) (xt Int) (yt Int) (xtt Int) (ytt Int))
    (=> (and
        (f x y xt yt)
        (<= xt 999)
        (<= xt -1)
        (>= yt 0)
        (= xtt (+ xt 1))
        (= ytt (* 3 yt))
    ) (f x y xtt ytt)
)))

; D
(assert (forall ((x Int) (y Int) (xt Int) (yt Int) (xtt Int) (ytt Int))
    (=> (and
        (f x y xt yt)
        (<= xt 999)
        (<= xt 0)
        (<= yt -1)
        (= xtt xt)
        (= ytt (+ yt 1))
    ) (f x y xtt ytt)
)))

; E
(assert (forall ((x Int) (y Int) (xt Int) (yt Int) (xtt Int) (ytt Int))
    (=> (and
        (f x y xt yt)
        (<= xt 999)
        (= xt 0)
        (= yt 0)
        (= xtt xt)
        (= ytt (+ yt 1))
    ) (f x y xtt ytt)
)))

(assert (forall ((x Int) (y Int) (xt Int) (yt Int))
    (=> (and
        (f x y xt yt)
        (>= xt 1000)
    ) (g x y xt yt)
)))

(assert (forall ((x Int) (y Int) (xt Int) (yt Int))
    (=> (and
            (g x y xt yt)
            (= x 50)
            (= y 9)
            (= xt 1600)
            (= yt 4)
        )
        false
    )
))

(check-sat)
(exit)
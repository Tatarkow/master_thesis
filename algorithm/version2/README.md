# README
This project includes an original loop summarization algorithm named WSummarizer and implemented in Python. The code can be found in `/wsummarizer/`. We also include tests/samples, which are present in `tests`. They can be easily executed using `make` (see below). For comparison, in folder `other_tools/` we also include samples for three other tools. How to install the tools and how to run the samples is explained below.

The following instructions are expected to be run on a POSIX system with Python 3 and pip (Python's package manager) installed. For instance, on Windows tools such as Cygwin may be used. 

## WSummarizer
### How to install and run it
Before it is possible to run the samples, it is necessary to create Python environment, activate it and install all the requirements:
```shell script
python -m venv env
source env/bin/activate
pip install -r requirements.txt
```

To run all tests:
```shell script
make
```

To run a specific test (e.g. `01_simple_loop.py`) execute:
```shell script
make TEST=tests/01_simple_loop.py
```

To turn on verbose mode (can be used with a specific test too) append flag `VERBOSE=1`. It will print all state diagrams that are built during the computation. The printed `type` determines how the state diagram will be handled. Star after name of a state means that the state is terminable.

Afterwards, it is possible to disable the environment by executing:
```shell script
deactive
```

### How to use it
Consider example `02_complex_loop.py`. We need to import `WSummarizer`, e.g. by executing:
```python
from wsummarizer import WSummarizer as ws
```
Then it is necessary to define variables. For this there is function `create_variables`, which takes number of variables as a parameter and returns a list of created variables. For instance, in `02_complex_loop.py` we create variables `x` and `y` using:
```python
x, y = ws.create_variables(2)
```
We proceed by defining a loop using function `define_loop`. It takes two parameters. The first is a loop condition (i.e. header of a while cycle), for instance `x <= 999`. The second parameter is a list of branches. Each branch is represented as a tuple.

The first element of the tuple is a branch guard, which is represented by a list of terms of a conjunction. For instance, the guard of the first branch in our example is represented by list `[x >= 0, y >= 1]` that represents guard `x >= 0 and y >= 1`. If there is only a single term, it is possible to omit the square brackets. For instance, in sample `01_simple_loop.py`, the guard of the first branch is represented only by `y >= 1`, not `[y >= 1]`.

The second element of the tuple represents the update matrix associated with a given branch. It is represented as a list of `m` rows, where `m` is number of variables. `i`th row corresponds to update of `i`th variable. Each row is represented by a list of length `m + 1`. Its `j`th element is a coefficient associated with `j`th variable and its `m + 1`th element as a absolute coefficient. For instance, update matrix of the first branch of the example is `[[2, 0, 0], [0, 1, -1]]`, which means that the second variable (i.e. `y`) is in the branch updated using formula `0*x + 1*y + (-1)`, i.e. `y - 1`.

When the loop is defined, we can summarize it by calling function `summarize`:
```python
loop = ws.define_loop(...)
loop.summarize()
```
At this point summarization itself is stored in `loop` object. The summarization can be used to obtain the final values of variables after applying the loop. To do so, it is possible to use function `compute_terminal_values`, which takes the initial values (as many of them as there are variables) and returns a list of the terminal (i.e. final) values
```python
xt, yt = loop.compute_terminal_values(723, 9)
```

## Loop acceleration
Samples for the loop acceleration algorithm can be found in `/other_tools/loat/` folder. On their [GitHub page](https://github.com/aprove-developers/LoAT/tree/tacas20) it is recommended to download a prebuild version from [here](https://github.com/aprove-developers/LoAT/releases). Download the binary, make it executable (e.g. by `chmod +x loat-static`). To run a sample named `{FILENAME}` simply execute:
```shell script
./loat-static {FILENAME}
```

## Z3
Samples for Z3 are present in `/other_tools/z3/` directory. All the important information is at their [GitHub page](https://github.com/Z3Prover/z3). However, if you are using Ubuntu, it can be also installed using `apt install z3`, which is the approach we had chosen.  To run a sample named `{FILENAME}` execute:
```shell script
z3 {FILENAME}
```

## Flata
Samples for Flata can be found in `/other_tools/flata/` folder. The tool can be cloned from its [GitHub page](https://github.com/filipkonecny/flata). Flata depends on Yices, which can be downloaded from [here](https://yices.csl.sri.com/old/download-yices1.html). After downloading both program, navigate into the root folder of Flata. Build the project using `ant compile` command. Once it is built, enter (newly generated) `dist/`. Add path to Yices using `PATH=$PATH:{PATH_TO_YICES}`. Finally, to run a sample named `{FILENAME}` execute:
```shell script
./flata-reachability.sh {FILENAME}
```

#!/usr/bin/env bash

# WS
source env/bin/activate
export PYTHONPATH=./wsummarizer
export VERBOSE=0
export ITERATIONS=0


for J in {1..3}
do
  python ./speed_experiment/08.py
  echo "-"
done

deactivate

# Z3
export X=1000
export Y=1

TIMEFORMAT=%R

for J in {1..3}
do
  for I in {110..201..10}
  do
    export Z=$I

    export XT=$((X-I))
    export YT=$((Y+I))
    export ZT=$Z

    envsubst < ./speed_experiment/08.smt2 > ./speed_experiment/TMP_08.smt2
    (time z3 ./speed_experiment/TMP_08.smt2)
  done
  echo "-"
done

rm ./speed_experiment/TMP_08.smt2

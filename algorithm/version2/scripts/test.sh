#!/usr/bin/env bash

test() {
  printf "*%.0s" {1..40}
  printf "\nTESTING $1\n"
  python "$1"
}

source env/bin/activate
export PYTHONPATH=./wsummarizer
export VERBOSE=$1
export ITERATIONS=$2

if [[ -z $3 ]]
then
  for FILENAME in ./tests/*.py; do
    if [[ "$FILENAME" != ./tests/compare.py ]]
    then
      test "$FILENAME"
    fi
done
else
  test "$3"
fi

deactivate
class Node:
    """
    Represent a single vertex of a loop diagram.
    """

    def __init__(self, state_diagram, is_terminable):
        self.state_diagram = state_diagram
        self.is_terminable = is_terminable

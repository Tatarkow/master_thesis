from copy import deepcopy

from digraph import Digraph
from node import Node
from utils import sand, sor, theta
from variables import Variables
from wtypes import List, Edge


class LoopDiagram(Digraph):
    """
    Represents a loop diagram.
    """

    def __init__(self, state_diagram):
        super().__init__()

        self.nodes = self._get_nodes(state_diagram)
        self.edges = self._get_edges(state_diagram)
        self.state_diagram = state_diagram

    @property
    def vertices(self):
        """
        Returns all vertices, i.e. nodes.

        Returns:
            List of nodes.
        """
        return self.nodes

    def _get_nodes(self, state_diagram) -> List[Node]:
        from state_diagram import StateDiagram
        from state import State

        nodes = []

        for component in state_diagram.components:
            loop = self._get_loop(component, state_diagram)
            states = []
            edges = []
            state_index_mapping = {}
            is_terminable = False

            for state_index in component:
                old_state = state_diagram.states[state_index]
                branches = old_state.branches
                new_state = State(branches, loop.gamma(branches))

                if old_state.is_terminable:
                    is_terminable = True

                state_index_mapping[state_index] = len(states)
                states.append(new_state)

            for edge in state_diagram.edges:
                if edge[0] in component and edge[1] in component:
                    new_edge = (state_index_mapping[edge[0]], state_index_mapping[edge[1]])
                    edges.append(new_edge)

            new_state_diagram = StateDiagram(loop, states=states, edges=edges)
            node = Node(new_state_diagram, is_terminable)
            nodes.append(node)

        return nodes

    @staticmethod
    def _get_edges(state_diagram) -> List[Edge]:
        edges = []
        components = state_diagram.components

        for first_node_index, first_component in enumerate(components):
            for second_node_index, second_component in enumerate(components):
                if first_node_index == second_node_index:
                    continue

                for first_state_index in first_component:
                    for second_state_index in second_component:
                        if (first_state_index, second_state_index) in state_diagram.edges:
                            edges.append((first_node_index, second_node_index))

        return edges

    @staticmethod
    def _get_loop(component, state_diagram):
        branches = []

        for state_index in component:
            state = state_diagram.states[state_index]
            branches += state.branches

        guards = [branch.guard for branch in branches]
        loop = deepcopy(state_diagram.loop)
        loop.loop_condition = sand(loop.loop_condition, sor(*guards))
        loop.branches = branches

        return loop

    def summarize(self):
        """
        Method summarizing the loop diagram. In the thesis it
        corresponds to function `summarizeLoopDiagram`.

        Returns:
            Formula representing the summarization.
        """

        self.state_diagram.print("SPLITTABLE")
        path_summarizations = []

        for start_index, start_node in enumerate(self.nodes):
            for end_index, end_node in enumerate(self.nodes):
                if not end_node.is_terminable:
                    continue

                paths = self._get_paths_between(start_index, end_index)

                for path in paths:
                    path_summarization = self._initialize_path_summarization()

                    for node_index in path:
                        node = self.nodes[node_index]
                        node_summarization = node.state_diagram.summarize()
                        path_summarization = theta(path_summarization, node_summarization)

                    path_summarizations.append(path_summarization)

        return sor(*path_summarizations)

    @staticmethod
    def _initialize_path_summarization():
        parts = []

        for initial, terminal in zip(Variables.initial, Variables.terminal):
            parts.append(initial == terminal)

        return sand(*parts)


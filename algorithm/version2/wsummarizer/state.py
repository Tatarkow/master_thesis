class State:
    """
    Represents a single vertex of a state diagram.
    """

    def __init__(self, branches, is_terminable):
        self.branches = branches
        self.is_terminable = is_terminable

    def __str__(self):
        return "".join([str(branch) for branch in self.branches])

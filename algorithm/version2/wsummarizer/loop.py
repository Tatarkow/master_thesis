import os
from copy import deepcopy
from string import ascii_uppercase

import z3

from branch import Branch
from exceptions import Z3Unknown, InfiniteLoop
from state_diagram import StateDiagram
from utils import ssub, nu, is_satisfiable, sand, snot, sympy_result_vector_to_z3, sor, is_solver_satisfiable
from variables import Variables


class Loop:
    """
    Represents a loop, i.e. a while cycle.
    """

    def __init__(self, loop_condition_parts, branches):
        self.summarization = None
        self.loop_condition = self._get_condition(loop_condition_parts)
        self.branches = []

        for index, (guard_parts, update_matrix) in enumerate(branches):
            name = ascii_uppercase[index]
            guard = self._get_condition(guard_parts)
            branch = Branch(guard, update_matrix, name)
            self.branches.append(branch)

    @staticmethod
    def _get_condition(all_parts):
        if type(all_parts) == list:
            if type(all_parts[0]) == list:
                or_parts = []

                for parts in all_parts:
                    or_parts.append(sand(*parts))

                return sor(*or_parts)
            else:
                return sand(*all_parts)
        else:
            return all_parts

    def summarize(self):
        """
        The most important method of the whole application,
        which can be used to summarized the loop.

        Returns:
            None
        """
        state_diagram = StateDiagram(self)
        state_diagram_summarization = state_diagram.summarize()
        parts = [snot(self.loop_condition)]

        for terminal, initial in zip(Variables.terminal, Variables.initial):
            parts.append(terminal == initial)

        self.summarization = sor(state_diagram_summarization, sand(*parts))

    def gamma(self, branches):
        """
        Decides whether the given path is terminable.

        Args:
            branches: Path represented as a list of branches.

        Returns:
            Boolean representing the terminability.
        """

        u_nu = self._get_u_nu(branches)
        substitutions = self._get_u_nu_substitutions(u_nu)
        r_rho = ssub(self.loop_condition, *substitutions)
        tau = self.tau(branches)

        return is_satisfiable(sand(tau, snot(r_rho)))

    def tau(self, branches):
        """
        Creates a formula representing feasibility of the given path.

        Args:
            branches: Path represented as a list of branches.

        Returns:
            Formula containing the initial values.
        """

        phi = branches[:-1]
        u_nu = self._get_u_nu(phi)
        substitutions = self._get_u_nu_substitutions(u_nu)
        d_rho = ssub(self.loop_condition, *substitutions)
        e_rho = ssub(branches[-1].guard, *substitutions)

        if len(branches) == 1:
            return sand(d_rho, e_rho)
        else:
            return sand(self.tau(phi), d_rho, e_rho)

    @staticmethod
    def _get_u_nu_substitutions(u_nu):
        substitutions = []

        for index, variable in enumerate(Variables.initial):
            substitutions.append((variable, u_nu[index]))

        return substitutions

    @staticmethod
    def _get_u_nu(branches):
        u_nu = nu()

        for branch in branches:
            u_nu = branch.update_matrix * u_nu

        return sympy_result_vector_to_z3(u_nu)

    def compute_terminal_values(self, *args):
        """
        Computes terminal values based on the given initial values
        using the pre-computed summarization. This method is meant
        to be run after calling `summarize` method.

        Args:
            *args: Initial values of the variables ordered in the
                same way as returned by function `create_variables`.

        Returns:
            List of terminal values order in the same way as input.
        """

        if self.summarization is None:
            return

        parts = [self.summarization]

        for index, initial_value in enumerate(args):
            initial_variable = Variables.initial[index]
            parts.append(initial_variable == initial_value)

        solver = self._get_solver(parts)
        solver.check()
        solution = solver.model()
        terminal_values = []

        for terminal_variable in Variables.terminal:
            terminal_value = solution[terminal_variable]
            terminal_values.append(terminal_value)

        return terminal_values

    def _get_solver(self, parts):
        solver = z3.Solver()
        solver.add(sand(*parts))

        try:
            if is_solver_satisfiable(solver):
                return solver
            else:
                raise InfiniteLoop
        except Z3Unknown:
            pass

        iteration_variables = deepcopy(Variables.iteration)
        return self._get_solver_iteration(parts, iteration_variables)

    def _get_solver_iteration(self, parts, iteration_variables):
        max_value = int(os.getenv("ITERATIONS"))
        variable = iteration_variables.pop()

        for value in range(2, max_value+1):
            parts.append(variable == value)

            solver = z3.Solver()
            solver.add(sand(*parts))

            try:
                if is_solver_satisfiable(solver):
                    if int(os.getenv("VERBOSE")) >= 2:
                        print("WARNING: Z3 needed support.")

                    return solver
            except Z3Unknown:
                pass

            if len(iteration_variables) > 0:
                return self._get_solver_iteration(parts, iteration_variables)

            parts.pop()

        raise Z3Unknown

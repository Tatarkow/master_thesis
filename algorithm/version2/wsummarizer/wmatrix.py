from sympy import Matrix, eye

from utils import value_to_z3, get_variable_indices
from variables import Variables


class WMatrix:
    """
    Wrapper class representing a matrix.
    """

    def __init__(self, list_matrix=None, sympy_matrix=None, identity_size=None):
        if list_matrix:
            self.matrix = Matrix(list_matrix)
        elif sympy_matrix:
            self.matrix = sympy_matrix
        elif identity_size:
            self.matrix = eye(identity_size)
        else:
            raise AttributeError

    def __mul__(self, other):
        matrix = self.matrix * other.matrix
        return WMatrix(sympy_matrix=matrix)

    def __pow__(self, exponent):
        """
        Raises the matrix to `exponent`th power.

        Args:
            exponent: To which power is the matrix raised.

        Returns:
            Raised matrix.
        """

        # M = P * J * P^-1
        matrix_p, matrix_j = self.matrix.jordan_form()
        jordan_cells = self._get_jordan_cells(matrix_j)
        powered_jordan_cells = []

        for jordan_cell in jordan_cells:
            powered_jordan_cell = self._power_jordan_cell(jordan_cell, exponent)
            powered_jordan_cells.append(powered_jordan_cell)

        powered_matrix_j = self._compose_jordan_matrix(powered_jordan_cells)
        powered_matrix = matrix_p * powered_matrix_j * matrix_p.inv()

        return WMatrix(sympy_matrix=powered_matrix)

    @staticmethod
    def _compose_jordan_matrix(cells):
        """
        Combines Jordan cells into Jordan matrix.

        Args:
            cells: List of matrices. Technically, it is a
                three dimensional list.

        Returns:
            Composed matrix.
        """

        size = sum([len(cell) for cell in cells])
        matrix = [[0 for column in range(size)] for row in range(size)]
        start = 0

        for cell in cells:
            cell_size = len(cell)

            for row in range(cell_size):
                for column in range(cell_size):
                    element = cell[row][column]
                    matrix[start + row][start + column] = element

            start += cell_size

        return Matrix(matrix)

    @classmethod
    def _power_jordan_cell(cls, cell, exponent):
        """
        Expresses the Jordan `cell` raised to `exponent`th power.
        It uses an explicit formula for the power.
        In other words no matrix multiplication is actually done.

        Args:
            cell: Matrix which is going to be raised.
            exponent: Determines to which power we are raising.

        Returns:

        """

        cell = cls._to_list_matrix(cell.matrix)
        size = len(cell)
        eigenvalue = value_to_z3(cell[0][0])
        powered_cell = []

        for i in range(size):
            row = [0 for _ in range(size)]
            powered_cell.append(row)

        for row in range(size):
            for column in range(row, size):
                index = column - row
                combinatorial_number = cls._combinatorial_number(exponent, index)

                # Optimization.
                if eigenvalue == 0:
                    element = 0
                elif eigenvalue == 1:
                    element = combinatorial_number
                elif eigenvalue == -1:
                    element = 1 - 2*(exponent % 2)
                else:
                    final_exponent = exponent - value_to_z3(index)
                    element = combinatorial_number * eigenvalue ** final_exponent

                    iteration_variable_indices = get_variable_indices(exponent)
                    assert len(iteration_variable_indices) == 1
                    iteration_variable = Variables.all_variables[iteration_variable_indices[-1]]

                    if iteration_variable in Variables.iteration_candidates:
                        if iteration_variable not in Variables.iteration:
                            Variables.iteration.append(iteration_variable)

                powered_cell[row][column] = element

        return powered_cell

    @staticmethod
    def _to_list_matrix(sympy_matrix):
        size = sympy_matrix.shape[0]
        list_matrix = []

        for row_index in range(size):
            row = []

            for column_index in range(size):
                value = float(sympy_matrix[row_index, column_index])
                row.append(value)

            list_matrix.append(row)

        return list_matrix

    @staticmethod
    def _combinatorial_number(top, bottom):
        numerator = 1
        denominator = 1

        for index in range(bottom):
            numerator = numerator * (top - index)
            denominator = denominator * (index + 1)

        return value_to_z3(numerator / denominator)

    def _get_jordan_cells(self, matrix):
        cell_sizes = self._get_jordan_cell_sizes(matrix)
        start = 0
        cells = []

        for cell_size in cell_sizes:
            cell = []

            for row_index in range(start, start + cell_size):
                row = []

                for column_index in range(start, start + cell_size):
                    element = matrix[row_index, column_index]
                    row.append(element)

                cell.append(row)

            ws_matrix = WMatrix(list_matrix=cell)
            cells.append(ws_matrix)
            start += cell_size

        return cells

    @staticmethod
    def _get_jordan_cell_sizes(matrix):
        last_delimiter = 0
        size = matrix.shape[0]
        cell_sizes = []

        for i in range(size - 1):
            if matrix[i, i + 1] != 1:
                cell_size = (i + 1) - last_delimiter
                cell_sizes.append(cell_size)
                last_delimiter = i + 1

        cell_size = size - last_delimiter
        cell_sizes.append(cell_size)

        return cell_sizes

    def row(self, index):
        """
        Returns a row of the matrix.

        Args:
            index: Index of the row to be returned.

        Returns:
            List of elements present in the row.
        """
        return list(self.matrix.row(index))

    def __getitem__(self, item):
        return self.matrix[item]

    def __str__(self):
        return "W" + str(self.matrix)

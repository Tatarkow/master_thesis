from wmatrix import WMatrix


class Branch:
    """
    Represents a single branch of a loop.
    """

    def __init__(self, guard, compact_update_matrix, name):
        self.guard = guard
        self.update_matrix = self._get_update_matrix(compact_update_matrix)
        self.name = name

    @staticmethod
    def _get_update_matrix(compact_update_matrix):
        last_row = [[0 for _ in compact_update_matrix] + [1]]
        update_matrix = compact_update_matrix + last_row
        return WMatrix(list_matrix=update_matrix)

    def __str__(self):
        return self.name

from wtypes import Edge, Set, List, Path


class Digraph:
    """
    Abstract class representing a general digraph.
    """

    def __init__(self):
        self.edges = []

    def _get_reachable_indices(self, vertex_index: int) -> Set[int]:
        edges = self._get_next_edges(vertex_index)
        reachable_indices = set()

        while len(edges) > 0:
            edge = edges.pop()

            if edge[1] not in reachable_indices:
                reachable_indices.add(edge[1])
                next_edges = self._get_next_edges(edge[1])

                for next_edge in next_edges:
                    if next_edge not in edges and next_edge[1] not in reachable_indices:
                        edges.add(next_edge)

        return reachable_indices

    def _get_next_edges(self, vertex_index: int) -> Set[Edge]:
        edges = set()

        for edge in self.edges:
            if edge[0] == vertex_index:
                edges.add(edge)

        return edges

    def _get_previous_edges(self, vertex_index: int) -> Set[Edge]:
        edges = set()

        for edge in self.edges:
            if edge[1] == vertex_index:
                edges.add(edge)

        return edges

    def _get_paths_between(self, start_index, end_index) -> List[Path]:
        if start_index == end_index:
            return [[start_index]]

        paths = []
        next_edges = self._get_next_edges(start_index)

        for next_edge in next_edges:
            next_index = next_edge[1]

            if next_index == start_index:
                continue

            if next_index == end_index:
                path = [start_index, end_index]
                paths.append(path)
            else:
                next_paths = self._get_paths_between(next_index, end_index)

                for next_path in next_paths:
                    path = [start_index] + next_path
                    paths.append(path)

        return paths

    @property
    def vertices(self):
        """
        Returns a list of vertices. Needs to be implemented by
        each child class.
        """

        raise NotImplementedError

    def _is_trivial(self) -> bool:
        return len(self.vertices) == 1 and len(self.edges) == 0

    def _is_simple_cycle(self) -> bool:
        if len(self.vertices) != len(self.edges):
            return False

        first_index = 0
        next_edges = self._get_next_edges(first_index)
        next_edge = next_edges.pop()
        second_index = next_edge[1]
        paths = self._get_paths_between(second_index, first_index)

        if len(paths) != 1:
            return False

        path = paths[0]

        if len(path) != len(self.vertices):
            return False

        return True

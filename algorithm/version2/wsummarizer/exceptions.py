class NotSummarizable(Exception):
    """
    Raised when the monotonicity condition is violated.
    """
    pass


class Z3Unknown(Exception):
    """
    Raised when Z3 cannot decide whether a given formula
    is (or is not) satisfiable.
    """
    pass


class InfiniteLoop(Exception):
    """
    Raised when a user wants to compute the terminal values,
    but the given initial values result in an infinite loop.
    """
    pass

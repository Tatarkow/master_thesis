import z3


class Variables:
    """
    Global class for operating with variables.
    """

    all_variables = []
    initial = []
    terminal = []
    iteration_candidates = []
    iteration = []
    counter = -1

    @classmethod
    def create_variables(cls, count):
        """
        Creates variables, which can be used to define a loop

        Args:
            count: How many variables should be generated.

        Returns:
            List of generated variables.
        """

        for _ in range(count):
            initial = cls.get_variable()
            cls.initial.append(initial)

            terminal = cls.get_variable()
            cls.terminal.append(terminal)

        return cls.initial

    @classmethod
    def _get_variable_name(cls):
        cls.counter += 1
        return f"v_{cls.counter}"

    @classmethod
    def get_variable(cls):
        """
        Creates a new helper variable.

        Returns:
            Variable instance.
        """

        variable = z3.Int(cls._get_variable_name())
        cls.all_variables.append(variable)
        return variable

import re

import z3

from exceptions import Z3Unknown
from variables import Variables


def sand(*formulae):
    """
    Wrapper for logical AND.
    """
    return z3.simplify(z3.And(*formulae))


def sor(*formulae):
    """
    Wrapper for logical OR.
    """
    return z3.simplify(z3.Or(*formulae))


def ssub(formula, *substitutions):
    """
    Wrapper for substitution.
    """

    new_substitutions = []

    for substitution in substitutions:
        first = substitution[0]

        try:
            second = z3.ToInt(substitution[1])
        except (AttributeError, z3.Z3Exception):
            second = substitution[1]

        try:
            second = z3.IntVal(second)
        except (AttributeError, z3.Z3Exception):
            second = second

        new_substitutions.append((first, second))

    return z3.simplify(z3.substitute(formula, *new_substitutions))


def snot(formula):
    """
    Wrapper for logical NOT.
    """
    return z3.simplify(z3.Not(formula))


def nu():
    """
    Returns vector representing the initial values.
    """
    from wmatrix import WMatrix

    column = Variables.initial + [1]
    return WMatrix(list_matrix=column)


def is_satisfiable(formula):
    """
    Decides whether the given formula is satisfiable.

    Args:
        formula: Considered formula.

    Returns:
        Boolean representing the satisfiability.
    """
    solver = z3.Solver()
    solver.add(formula)

    return is_solver_satisfiable(solver)


def is_solver_satisfiable(solver):
    """
    Decides whether the given solver is satisfiable.

    Args:
        solver: Solver holding a formula, whose satisfiability
            is going to be decided.

    Returns:
        Boolean representing the satisfiability.
    """

    result = str(solver.check())

    if result == "sat":
        return True
    elif result == "unsat":
        return False
    else:
        raise Z3Unknown


def theta(first_summarization, second_summarization):
    """
    Creates a concatenated summarization.

    Args:
        first_summarization: First formula to be concatenated.
        second_summarization: Second formula to be concatenated.

    Returns:
        Formula representing the concatenated summarization
    """
    middle_variables = [Variables.get_variable() for _ in Variables.initial]
    first_substitutions = list(zip(Variables.terminal, middle_variables))
    new_first_summarization = ssub(first_summarization, *first_substitutions)
    second_substitutions = list(zip(Variables.initial, middle_variables))
    new_second_summarization = ssub(second_summarization, *second_substitutions)

    return sand(new_first_summarization, new_second_summarization)


def value_to_z3(value):
    """
    Helper function for converting values to instances of Z3 values.

    Args:
        value: Value.

    Returns:
        Instance of Z3 value (if convertible).
    """

    if type(value) == float and int(value) == value:
        value = int(value)

    if type(value) == int:
        return z3.IntVal(value)

    return value


def remove_redundant_toreal(string):
    """
    Helper function cleaning Sympy formula.

    Args:
        string: String representing a Sympy formula

    Returns:
        String representing a Sympy formula without redundant
        `ToReal` calls.
    """
    return re.sub(r'ToReal\((\d+)\)', r'\1', string)


def sympy_result_element_to_z3(result_element):
    """
    Converts a single Sympy element to a Z3 elements.

    Args:
        result_element: Sympy element.

    Returns:
        Z3 element.
    """

    string = str(result_element)
    string = remove_redundant_toreal(string)
    last_index = len(Variables.all_variables) - 1

    for index in range(last_index, -1, -1):
        variable = Variables.all_variables[index]
        variable_reference = f"Variables.all_variables[{index}]"
        string = string.replace(str(variable), variable_reference)

    string = re.sub(r"Mod\(([^,]+), ([^)]+)\)", '(\g<1> % \g<2>)', string)

    return value_to_z3(eval(string))


def sympy_result_vector_to_z3(result_vector):
    """
    Converts vector obtained using Sympy to a list of Z3 elements.

    Args:
        result_vector: Sympy vector.

    Returns:
        List of Z3 elements.
    """

    z3_result_vector = []

    for element in result_vector:
        z3_result_vector.append(sympy_result_element_to_z3(element))

    return z3_result_vector


def get_variable_indices(expression, variables=Variables.all_variables):
    """
    Helper function for obtaining all variables present in
    a given formula.

    Args:
        expression: Formula which should be searched.
        variables: Optional argument for specifying only a
            subset of variables for which we should search.

    Returns:
        List of indices corresponding to the found variables.
    """

    expression = str(expression)
    last_index = len(Variables.all_variables) - 1
    variable_indices = []

    for index in range(last_index, -1, -1):
        variable = Variables.all_variables[index]

        if variable in variables and str(variable) in expression:
            variable_indices.append(index)

        expression.replace(str(variable), "")

    return variable_indices

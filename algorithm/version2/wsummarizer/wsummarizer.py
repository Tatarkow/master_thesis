import warnings

from sympy.utilities.exceptions import SymPyDeprecationWarning

from loop import Loop
from variables import Variables

# SymPy overrides default Python warning behaviour.
warnings.simplefilter("ignore", SymPyDeprecationWarning)


class WSummarizer:
    """
    Main class for external interaction with the program.
    User should not need to import anything else.
    """

    @classmethod
    def create_variables(cls, count):
        """
        Creates variables, which can be used to define a loop

        Args:
            count: How many variables should be generated.

        Returns:
            List of generated variables.
        """
        return Variables.create_variables(count)

    @classmethod
    def define_loop(cls, loop_condition, branches):
        """
        Defines a loop based on the arguments.

        Args:
            loop_condition: "While" condition of the loop.
            branches: List of "branches". Each branch is a pair,
                whose first element is a branch guards and whose
                second element represents an update matrix.

        Returns:
            Loop instance.
        """
        return Loop(loop_condition, branches)

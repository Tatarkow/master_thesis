import os
from copy import deepcopy

import z3

from digraph import Digraph
from exceptions import NotSummarizable, Z3Unknown
from loop_diagram import LoopDiagram
from state import State
from variables import Variables
from wmatrix import WMatrix
from wtypes import Set, Dict
from utils import is_satisfiable, sor, theta, nu, sand, ssub, snot, sympy_result_vector_to_z3, is_solver_satisfiable, \
    get_variable_indices


class StateDiagram(Digraph):
    """
    Represents a state diagram.
    """

    counter = 0

    def __init__(self, loop, states=None, edges=None):
        super().__init__()

        StateDiagram.counter += 1
        self.id = self.counter
        self.loop = loop
        self.states = states or self._get_states()
        self.edges = edges or self._get_edges()
        self.components = None
        self.summarization = None

        self._sort_states_and_edges()

    def _sort_states_and_edges(self):
        """
        Helper function that alphabetically sorts states and edges.
        It is useful for printing/debugging.

        Returns:
            None.
        """

        new_states = sorted(self.states, key=lambda state: str(state.branches))
        index_map = {}

        for old_index, old_state in enumerate(self.states):
            new_index = new_states.index(old_state)
            index_map[old_index] = new_index

        new_edges = []

        for old_edge in self.edges:
            new_edge = (index_map[old_edge[0]], index_map[old_edge[1]])
            new_edges.append(new_edge)

        new_edges.sort(key=lambda edge: (
            str(new_states[edge[0]].branches),
            str(new_states[edge[1]].branches)
        ))

        self.states = new_states
        self.edges = new_edges

    def summarize(self):
        """
        Main summarization method. In the thesis it corresponds
        to `summarizeStateDiagram`.

        Returns:
            Formula representing the summarization.
        """

        if self.summarization is not None:
            return self.summarization

        self._simplify()

        if self._is_trivial():
            self.summarization = self._summarize_trivial()
            return self.summarization
        elif self._is_simple_cycle():
            self.summarization = self._summarize_simple_cycle()
            return self.summarization
        elif self._is_splittable():
            self.summarization = LoopDiagram(self).summarize()
            return self.summarization

        summarization_current_level = self._summarize_current_level()
        state_diagram = self._get_higher_level()
        summarization_higher_level = state_diagram.summarize()

        self.summarization = sor(summarization_current_level, summarization_higher_level)
        return self.summarization

    def _summarize_current_level(self):
        """
        This summarizes only such executions of the loop that
        the number of loop iterations is equal to the current
        level of the state diagram. Note that this summarization
        is meant to be combined with summarization of a state
        diagram of higher level. In the thesis it corresponds
        to `summarizeCurrentLevel`.

        Returns:
            Summarization of certain executions of the loop.
        """

        summarizations = []

        for state in self.states:
            if not state.is_terminable:
                continue

            identity_size = len(Variables.initial) + 1
            matrix = WMatrix(identity_size=identity_size)

            for branch in state.branches:
                matrix = branch.update_matrix * matrix

            result = sympy_result_vector_to_z3(matrix * nu())
            parts = [self.loop.tau(state.branches)]

            for index, terminal in enumerate(Variables.terminal):
                parts.append(terminal == result[index])

            substitutions = []

            for index, initial in enumerate(Variables.initial):
                substitutions.append((initial, result[index]))

            parts.append(snot(ssub(self.loop.loop_condition, *substitutions)))
            summarizations.append(sand(*parts))

        return sor(*summarizations)

    def _get_higher_level(self):
        """
        Build a state diagram of level `N+1`, where `N`
        is the current level.

        Returns:
            State diagram of higher level.
        """

        self.print("HIGHER LEVEL")

        states = self._get_higher_states()
        return StateDiagram(self.loop, states=states)

    def _get_higher_states(self):
        """
        Creates states of a state diagram of level `N+1`,
        where `N` is the current level.

        Returns:
            List of the states.
        """
        states = []

        for state_index, state in enumerate(self.states):
            next_edges = self._get_next_edges(state_index)
            branches = state.branches

            for edge in next_edges:
                next_state = self.states[edge[1]]
                next_branch = next_state.branches[-1]
                all_branches = branches + [next_branch]
                tau = self.loop.tau(all_branches)

                if is_satisfiable(tau):
                    is_terminable = self.loop.gamma(all_branches)
                    state = State(all_branches, is_terminable)
                    states.append(state)

        return states

    def _simplify(self):
        """
        Simplifies the state diagram before we try to summarize it.

        Returns:
            None.
        """

        original_diagram = deepcopy(self)

        self._split_terminable_paths()
        self._remove_redundant_states()
        self._merge_states()
        self._sort_states_and_edges()

        if len(original_diagram.states) != len(self.states) or len(original_diagram.edges) != len(self.edges):
            original_diagram.print("SIMPLIFIABLE")

    def _remove_redundant_states(self):
        """
        One of the methods for simplifying the state diagram.
        It removes all the states from which is no terminable
        state reachable. In the thesis it corresponds to
        `removeRedundantStates`.

        Returns:
            None.
        """

        state_index_map = {}
        necessary_indices = []
        queue = []

        for state_index, state in enumerate(self.states):
            if state.is_terminable:
                queue.append(state_index)

        while len(queue) > 0:
            state_index = queue.pop()
            state_index_map[state_index] = len(necessary_indices)
            necessary_indices.append(state_index)
            edges = self._get_previous_edges(state_index)

            for edge in edges:
                if edge[0] not in necessary_indices and edge[0] not in queue:
                    queue.append(edge[0])

        new_states = [self.states[index] for index in necessary_indices]
        new_edges = []

        for edge in self.edges:
            if edge[0] in necessary_indices and edge[1] in necessary_indices:
                new_edges.append((state_index_map[edge[0]], state_index_map[edge[1]]))

        self.states = new_states
        self.edges = new_edges

    def _merge_states(self):
        """
        One of the methods for simplifying the state diagram.
        After splitting the terminable paths, some state are
        duplicated. This methods merges them back together.
        In the thesis it corresponds to `mergeDuplicatedStates`.

        Returns:
            None.
        """

        state_indices_by_branches = self._get_state_indices_by_branches()
        state_index_map = {}
        new_states = []

        for new_state_index, branches in enumerate(state_indices_by_branches):
            old_state_indices = state_indices_by_branches[branches]
            is_terminable = False

            for old_state_index in old_state_indices:
                state_index_map[old_state_index] = new_state_index
                old_state = self.states[old_state_index]

                if old_state.is_terminable:
                    is_terminable = True

            old_state_index = old_state_indices.pop()
            old_state = self.states[old_state_index]
            new_state = State(old_state.branches, is_terminable)
            new_states.append(new_state)

        new_edges = []

        for old_edge in self.edges:
            new_edge = (state_index_map[old_edge[0]], state_index_map[old_edge[1]])

            if new_edge not in new_edges:
                new_edges.append(new_edge)

        self.states = new_states
        self.edges = new_edges

    def _get_state_indices_by_branches(self):
        state_indices_by_branches = {}

        for state_index, state in enumerate(self.states):
            branches = str(state)

            if branches not in state_indices_by_branches:
                state_indices_by_branches[branches] = set()

            state_indices_by_branches[branches].add(state_index)

        return state_indices_by_branches

    def _split_terminable_paths(self):
        """
        One of the methods for simplifying the state diagram.
        It "splits" such state path ending with a terminable state
        that the loop cannot terminate at the terminable state after
        executing the path. In the thesis it corresponds
        to `splitTerminablePaths`.

        For instance, consider the following three
        states AB -> BC -> CD*, where * symbolizes that state CD is
        terminable. Although, the loop can generally terminate
        at state CD, it can happen that it cannot terminate after
        executing sequence of branches A, B, C and D. In other words,
        it cannot terminate at state CD directly after executing
        states AB and BC.

        Assuming that the loop can terminate at state CD after executing
        state BC, we would split the path to two paths: AB -> BC -> CD
        and BC -> CD*. Notice, that state CD is marked as temrinable only
        at the later path.

        Returns:
            None.
        """

        simple_paths = self._get_simple_paths()
        maximal_simple_paths = self._remove_nonmaximal_paths(simple_paths)  # T

        while len(maximal_simple_paths) > 0:
            path = maximal_simple_paths.pop(0)  # P
            last_state_index = path[-1]
            last_state = self.states[last_state_index]
            branches = last_state.branches  # B
            new_states = [deepcopy(last_state)]
            first_new_state_index = len(self.states)
            new_edges = []

            for path_index in range(len(path) - 2, -1, -1):
                state_index = path[path_index]
                state = self.states[state_index]
                branch = state.branches[0]  # B
                branches = [branch] + branches  # B

                if not self.loop.gamma(branches):
                    self.states += new_states
                    self._add_edges_after_split(new_edges, path, first_new_state_index)

                    last_state.is_terminable = False
                    maximal_simple_paths = self._update_simple_paths(path, maximal_simple_paths)
                    break

                new_states.append(deepcopy(state))
                new_edges.append((state_index, path[path_index+1]))

    def _add_edges_after_split(self, new_edges, path, first_new_state_index):
        """
        Takes care of adding/duplicating edges when the split occurs.
        In the thesis it corresponds to `updateEdgesAfterSplit`.

        Args:
            new_edges: List of edges belonging to the split path.
                More precisely these edges form the new path.
            path: State path which is going to be split.
            first_new_state_index: Index of the newly created
                terminable state.

        Returns:
            None.
        """
        self.edges += new_edges

        for edge in self._get_next_edges(path[-1]):
            self.edges.append((first_new_state_index, edge[1]))

            if edge[0] == edge[1]:
                self.edges.append((first_new_state_index, first_new_state_index))

                if self._should_add_backward_edge(edge[1], path):
                    self.edges.append((edge[1], first_new_state_index))

    def _should_add_backward_edge(self, state_index, path):
        """
        Helper method for `_split_terminable_paths`.
        Decides whether we should add an edge from the original
        terminable state to newly created terminable state.
        In the thesis it partially corresponds to
        `shouldAddBackwardEdge`.

        Args:
            state_index: Index of the originally terminable state.
            path: State path which was split.

        Returns:
            Boolean representing whether we should add the edge.
        """

        loop_condition_variable_indices = get_variable_indices(self.loop.loop_condition, variables=Variables.initial)

        first_state = self.states[path[0]]
        branches = first_state.branches[:-1]

        for path_index in path:
            state = self.states[path_index]
            branch = state.branches[-1]
            branches.append(branch)

        tau = self.loop.tau(branches)

        last_state = self.states[state_index]
        last_branch = last_state.branches[-1]
        last_update_matrix = last_branch.update_matrix

        for variable_index in loop_condition_variable_indices:
            update_vector = last_update_matrix.row(variable_index)

            if self._can_variable_change(variable_index, update_vector, tau):
                return True

        return False

    def _can_variable_change(self, variable_index, update_vector, tau):
        """
        Helper method for `_should_add_backward_edge`.
        It decides whether some variable contained in the loop
        condition may change at state the last state of the path
        after executing the path. In the thesis it partially
        corresponds to `shouldAddBackwardEdge`.

        Args:
            variable_index: Variable whose update
                is going to be checked.
            update_vector: Update that is going to
                be checked.
            tau: Tau function representing the path.

        Returns:
            Boolean whether the variable may change.
        """

        if self._is_trivial_update(variable_index, update_vector):
            return False

        variable = Variables.all_variables[variable_index]
        solver = z3.Solver()
        solver.add(sand(tau, variable != 0))

        if not self._is_variable_affected_by_others(variable_index, update_vector):
            if not is_solver_satisfiable(solver):
                return False

        return True

    @staticmethod
    def _is_trivial_update(variable_index, update_vector):
        """
        Helper method for `_can_variable_change`.
        Returns true iff the update does not change the
        variable with the given index.

        Args:
            variable_index: Variable whose update
                is going to be checked.
            update_vector: Update that is going to
                be checked.

        Returns:
            Boolean whether the update changes the variable.
        """

        for index, update in enumerate(update_vector):
            if index == variable_index:
                if update != 1:
                    return False
            else:
                if update != 0:
                    return False

        return True

    @staticmethod
    def _is_variable_affected_by_others(variable_index, update_vector):
        """
        Is by applying the update the given variable affected by
        other variables (or the constant)?

        Args:
            variable_index: Variable whose update
                is going to be checked.
            update_vector: Update that is going to
                be checked.

        Returns:
            Boolean whether the variable is affected.
        """

        for index, update in enumerate(update_vector):
            if index != variable_index and update != 0:
                return True

        return False

    def _get_simple_path_candidates(self, maximal_simple_paths, resolved_path):
        """
        Returns current maximal simple paths that might be prolongated.

        Args:
            maximal_simple_paths: List of remaining maximal simple paths.
            resolved_path: The just split path.

        Returns:
            Dictionary of list of the paths index by their first states.
        """

        edges = self._get_next_edges(resolved_path[-1])
        next_state_indices = [edge[1] for edge in edges]
        all_candidates = {}

        for path in maximal_simple_paths:
            if path[0] in next_state_indices:
                if path[0] not in all_candidates:
                    all_candidates[path[0]] = []

                all_candidates[path[0]] = path

        return all_candidates

    def _update_simple_paths(self, resolved_path, maximal_simple_paths):
        """
        Helper method for `_split_terminable_paths`.
        It updates, the simple paths. More precisely, it may prolongate
        the existing (until now maximal) simple paths. In the thesis
        it corresponds to `updateSimplePaths`.

        Args:
            resolved_path: The just split path.
            maximal_simple_paths: List of remaining maximal simple paths.

        Returns:
            Updates list of maximal simple paths.
        """

        all_candidates = self._get_simple_path_candidates(maximal_simple_paths, resolved_path)
        new_maximal_simple_paths = maximal_simple_paths

        for old_start_index in all_candidates:
            candidates = all_candidates[old_start_index]

            if len(candidates) > 1:
                continue

            old_path = candidates[0]

            if len(all_candidates) == 1:
                new_path = resolved_path + old_path
            else:
                new_path = [resolved_path[-1]] + old_path

            new_maximal_simple_paths.append(new_path)

        return self._remove_nonmaximal_paths(new_maximal_simple_paths)

    @staticmethod
    def _remove_nonmaximal_paths(paths):
        """
        Helper method which filters out paths
        that are in fact not maximal.

        Args:
            paths: List of candidates for maximal paths.

        Returns:
            List of maximal paths.
        """

        maximal_paths = []

        for path in paths:
            should_add = True

            for index, maximal_path in enumerate(maximal_paths):
                if len(path) < len(maximal_path):
                    start = len(maximal_path) - len(path)

                    if maximal_path[start:] == path:
                        should_add = False
                        break
                elif len(path) > len(maximal_path):
                    start = len(path) - len(maximal_path)

                    if path[start:] == maximal_path:
                        maximal_paths[index] = path
                        break

            if should_add:
                maximal_paths.append(path)

        return maximal_paths

    def _get_simple_paths(self):
        """
        Helper method for `_split_terminable_paths`.

        Returns:
            List of all simple paths.
        """

        paths = []

        for state_index, state in enumerate(self.states):
            if not state.is_terminable:
                continue

            paths += self._get_simple_paths_ending_at(state_index)

        return paths

    def _get_simple_paths_ending_at(self, terminal_state_index):
        """
        Helper method for `_get_simple_paths`.

        Args:
            terminal_state_index: Index of a state.

        Returns:
            List of simple paths ending at that state.
        """

        paths = []
        edges = self._get_previous_edges(terminal_state_index)

        for state_index, _ in edges:
            path = [terminal_state_index]
            state = self.states[state_index]

            if state.is_terminable:
                if path not in paths:
                    paths.append(path)
                continue

            edges = self._get_previous_edges(state_index)
            path = [state_index] + path

            while len(edges) == 1:
                edge = edges.pop()
                state_index = edge[0]

                if state_index == edge[1]:
                    break

                path = [state_index] + path
                state = self.states[state_index]

                if state.is_terminable:
                    break

                edges = self._get_previous_edges(state_index)

            paths.append(path)

        return paths

    @property
    def vertices(self):
        """
        Returns all vertices, i.e. states.

        Returns:
            List of states.
        """
        return self.states

    def _get_states(self):
        """
        Creates states for the given loop.

        Returns:
            List of states.
        """

        states = []

        for branch in self.loop.branches:
            is_terminable = self.loop.gamma([branch])
            state = State([branch], is_terminable)
            states.append(state)

        return states

    def _get_edges(self):
        """
        Creates edges for the given loop.

        Returns:
            List of edges.
        """

        edges = []

        for first_index, first_state in enumerate(self.states):
            branches = first_state.branches

            for second_index, second_state in enumerate(self.states):
                if branches[1:] != second_state.branches[:-1]:
                    continue

                next_branch = second_state.branches[-1]
                all_branches = branches + [next_branch]

                tau = self.loop.tau(all_branches)

                if is_satisfiable(tau):
                    edges.append((first_index, second_index))

        return edges

    def _is_splittable(self) -> bool:
        """
        Decides whether to state diagram is splittable, i.e.
        whether it can be dived into more (more or less)
        independent parts. In the thesis it corresponds to
        `isSplittable`.

        Returns:
            Boolean indicating whether it is splittable.
        """

        reachable_indices_info = self._get_reachable_indices_info()
        resolved_indices = set()
        self.components = []

        for start_index in reachable_indices_info:
            if start_index in resolved_indices:
                continue

            component_indices = {start_index}
            reachable_indices = reachable_indices_info[start_index]

            for reachable_index in reachable_indices:
                if start_index in reachable_indices_info[reachable_index]:
                    component_indices.add(reachable_index)

            resolved_indices = resolved_indices.union(component_indices)
            self.components.append(component_indices)

        return len(self.components) > 1

    def _get_reachable_indices_info(self) -> Dict[int, Set[int]]:
        """
        Helper method for `_is_splittable`.
        For each state it determines set of reachable indices.

        Returns:
            Reachable indices stored in a dictionary indexed by
            state indices.
        """

        reachable_indices_info = {}

        for state_index in range(len(self.states)):
            reachable_indices_info[state_index] = self._get_reachable_indices(state_index)

        return reachable_indices_info

    def print(self, diagram_type: str) -> None:
        if not int(os.getenv("VERBOSE")):
            return

        print("id:", self.id)
        print("type:", diagram_type)
        print("\nstates:")

        for state in self.states:
            if state.is_terminable:
                print(state, "*")
            else:
                print(state)

        print("\nedges:")

        for edge in self.edges:
            first_state = self.states[edge[0]]
            second_state = self.states[edge[1]]
            print(f"{first_state} -> {second_state}")

        print("-" * 40)

    def _summarize_trivial(self):
        """
        Summarizes a trivial state diagram.
        In the thesis it corresponds to `summarizeTrivial`.

        Returns:
            Summarization formula.
        """

        self.print("TRIVIAL")

        state = self.states[0]
        branches = state.branches
        first_branch = branches[0]
        matrix = first_branch.update_matrix

        for branch in branches[1:]:
            matrix = branch.update_matrix * matrix

        result = sympy_result_vector_to_z3(matrix * nu())
        tau = self.loop.tau(branches)

        parts_1 = []

        for terminal, initial in zip(Variables.terminal, Variables.initial):
            parts_1.append(terminal == initial)

        o_1 = sand(snot(tau), *parts_1)
        parts_2 = []

        for index, terminal in enumerate(Variables.terminal):
            parts_2.append(terminal == result[index])

        o_2 = sand(tau, *parts_2)
        return sor(o_1, o_2)

    def _summarize_simple_cycle(self):
        """
        Summarizes a simple cycle state diagram.
        In the thesis it corresponds to `summarizeSimpleCycle`.

        Returns:
            Summarization formula.
        """

        self.print("SIMPLE CYCLE")

        summarizations = []

        for start_index in range(len(self.states)):
            for end_index in range(len(self.states)):
                end_state = self.states[end_index]

                if not end_state.is_terminable:
                    continue

                branches, parts = self._summarize_first_phase(start_index, end_index)
                tau = self.loop.tau(branches)
                p_1 = sand(tau, *parts)

                o_0 = self._summarize_second_phase_skip()
                o_1 = self._summarize_second_phase_once(end_index)
                o_2 = self._summarize_second_phase_repeat(end_index, branches)

                summarization = theta(p_1, sor(o_0, o_1, o_2))
                summarizations.append(summarization)

        return sor(*summarizations)

    def _summarize_first_phase(self, start_index, end_index):
        """
        Helper method for `_summarize_simple_cycle`.
        Summarization of a simple cycle is divided into two phases.
        The first is associates with the phase before reaching a point,
        from which the loop pattern will periodically repeat. The first
        phase is handled by this function.

        Args:
            start_index: Index of a state in which the loop starts.
            end_index: Index of a state in which the loop terminates.

        Returns:
            Pair whose first element is a list of branches that are
            in the first phase executed and whose second element is
            a formula describing the values of variables after the
            first phase.
        """

        paths = self._get_paths_between(start_index, end_index)
        path = paths[0][1:]  # T
        first_state = self.states[start_index]
        initial_branches = deepcopy(first_state.branches)
        branches = self._get_simple_cycle_branches(path, initial_branches=initial_branches)  # B
        result = self._get_simple_cycle_result(branches)  # r
        parts = []

        for index, terminal in enumerate(Variables.terminal):
            parts.append(terminal == result[index])

        return branches, parts

    def _get_simple_cycle_branches(self, path, initial_branches=None):
        """
        Extracts branches that are actually executed when the given
        state path is executed. It is similar to function
        `statePathEffect` from the thesis (even though this method
        returns branches and not matrix).

        If we provided initial
        branches, it can also serve as an alternative to function
        `stateEffect` from the thesis.

        Args:
            path: State path, whose last branches are going to be
                returned.
            initial_branches: Optional list of branches, which is going
                to be prefixed to the list of last branches.

        Returns:
            List of branches.
        """

        branches = initial_branches or []

        for state_index in path:
            state = self.states[state_index]
            branch = state.branches[-1]
            branches.append(branch)

        return branches

    def _summarize_second_phase_skip(self):
        """
        Helper method for `_summarize_simple_cycle`.
        Summarization of a simple cycle is divided into two phases.
        The second one covers repeated execution of the full cycle
        and it has three possible cases.

        This function covers the first one, i.e. when the second
        phase is skipped completely. In the thesis it corresponds
        to function `summarizeSecondPhaseSkip`.

        Returns:
            Summarization formula.
        """

        parts = []

        for terminal, initial in zip(Variables.terminal, Variables.initial):
            parts.append(terminal == initial)

        return sand(snot(self.loop.loop_condition), *parts)

    @staticmethod
    def _get_simple_cycle_result(branches):
        """
        Computes total effect of the given branches.

        Args:
            branches: List of branches.

        Returns:
            Vector representing the total effect using
            the initial variables.
        """

        update_matrices = [branch.update_matrix for branch in branches]  # P
        total_update_matrix = update_matrices[0]  # M

        for update_matrix in update_matrices[1:]:
            total_update_matrix = update_matrix * total_update_matrix

        result = total_update_matrix * nu()  # r
        return sympy_result_vector_to_z3(result)

    def _summarize_second_phase_once(self, end_index):
        """
        Helper method for `_summarize_simple_cycle`.
        Summarization of a simple cycle is divided into two phases.
        The second one covers repeated execution of the full cycle
        and it has three possible cases.

        This function covers the second one, i.e. when the second
        phase is executed exactly once. In the thesis it corresponds
        to function `summarizeSecondPhaseOnce`.

        Returns:
            Summarization formula.
        """

        path = self._get_second_phase_path(end_index)  # T
        branches = self._get_simple_cycle_branches(path)  # B
        result = self._get_simple_cycle_result(branches)  # r
        substitutions = []

        for index, initial in enumerate(Variables.initial):
            substitutions.append((initial, result[index]))

        substituted_loop_condition = ssub(self.loop.loop_condition, *substitutions)  # d
        tau = self.loop.tau(branches)
        parts = []

        for index, terminal in enumerate(Variables.terminal):
            parts.append(terminal == result[index])

        return sand(tau, snot(substituted_loop_condition), *parts)

    def _summarize_second_phase_repeat(self, end_index, first_phase_branches):
        """
        Helper method for `_summarize_simple_cycle`.
        Summarization of a simple cycle is divided into two phases.
        The second one covers repeated execution of the full cycle
        and it has three possible cases.

        This function covers the third one, i.e. when the second
        phase is executed at least twice. In the thesis it corresponds
        to function `summarizeSecondPhaseRepeat`.

        Returns:
            Summarization formula.
        """

        first_phase_tau = self.loop.tau(first_phase_branches)
        path = self._get_second_phase_path(end_index)  # T
        i = Variables.get_variable()
        d_1 = self._get_simple_cycle_d(path, 1, i)
        d_2 = self._get_simple_cycle_d(path, 2, i)

        try:
            if is_satisfiable(sand(d_1, snot(d_2), i >= 2, first_phase_tau)):
                raise NotSummarizable
        except Z3Unknown:
            anti_monotonicity_condition = self._get_anti_monotonicity_condition(path, first_phase_branches)

            if is_satisfiable(anti_monotonicity_condition):
                raise NotSummarizable

        iota = Variables.get_variable()
        Variables.iteration_candidates.append(iota)
        total_update_matrix = self._combine_updates(path, iota, len(path) - 1)  # M
        result = sympy_result_vector_to_z3(total_update_matrix * nu())  # r

        e_substitutions = []

        for index, initial in enumerate(Variables.initial):
            e_substitutions.append((initial, result[index]))

        e = ssub(self.loop.loop_condition, *e_substitutions)
        h = self._get_simple_cycle_d(path, 1, iota)
        parts = []

        for index, terminal in enumerate(Variables.terminal):
            parts.append(terminal == result[index])

        return sand(h, snot(e), iota >= 2, *parts)

    def _get_anti_monotonicity_condition(self, path, first_phase_branches):
        """
        This builds the approximation of the monotonicity condition mentioned
        in the thesis in Section 4.5.1 in the case when Z3 is unable to
        decide the monotonicity condition.

        Essentially, it prevents the algorithm from falsely assuming that
        the loop satisfies certain monotonicity property.

        Args:
            path: Path representing the full cycle of the second phase.
            first_phase_branches: List of branches associated with the
                first phase.

        Returns:
            Formula representing the approximation of the monotonicity
            condition.
        """

        and_parts = []
        or_parts = []
        identity_size = len(Variables.initial) + 1

        # Expresses values of variables in i-th iteration of the full cycle.
        first_matrix = WMatrix(identity_size=identity_size)

        # Expresses values of variables in (i+1)-th iteration of the full cycle.
        second_matrix = WMatrix(identity_size=identity_size)

        for state_index in path:
            state = self.states[state_index]
            branch = state.branches[-1]
            second_matrix = branch.update_matrix * second_matrix

        for path_index, state_index in enumerate(path):
            state = self.states[state_index]
            branch = state.branches[-1]
            update_matrix = branch.update_matrix
            guard = branch.guard

            first_result = sympy_result_vector_to_z3(first_matrix * nu())
            second_result = sympy_result_vector_to_z3(second_matrix * nu())

            first_substitutions = []
            second_substitutions = []

            for index, initial in enumerate(Variables.initial):
                first_substitutions.append((initial, first_result[index]))
                second_substitutions.append((initial, second_result[index]))

            first_loop_part = ssub(self.loop.loop_condition, *first_substitutions)
            first_guard_part = ssub(guard, *first_substitutions)
            or_part = snot(sand(first_loop_part, first_guard_part))
            or_parts.append(or_part)

            second_loop_part = ssub(self.loop.loop_condition, *second_substitutions)
            second_guard_part = ssub(guard, *second_substitutions)
            and_part = sand(second_loop_part, second_guard_part)
            and_parts.append(and_part)

            first_matrix = update_matrix * first_matrix
            second_matrix = update_matrix * second_matrix

        first_phase_substitutions = self._get_first_phase_substitutions(first_phase_branches)
        return ssub(sand(sor(*or_parts), *and_parts), *first_phase_substitutions)

    @staticmethod
    def _get_first_phase_substitutions(first_phase_branches):
        """
        Helper method for `_get_anti_monotonicity_condition`.
        It returns substitutions of the initial variables
        with their values after executing the first phase.

        Args:
            first_phase_branches: List of branches associated
                with the first phase.

        Returns:
            List of pairs, each of which has the initial
            variable as its first element and its value
            represented using the initial variables as
            its second element.
        """

        identity_size = len(Variables.initial) + 1
        first_phase_update_matrix = WMatrix(identity_size=identity_size)

        for branch in first_phase_branches:
            first_phase_update_matrix = branch.update_matrix * first_phase_update_matrix

        result = sympy_result_vector_to_z3(first_phase_update_matrix * nu())
        substitutions = []

        for index, initial in enumerate(Variables.initial):
            substitutions.append((initial, result[index]))

        return substitutions

    def _get_next_state(self, path_index, path):
        next_index = (path_index + 1) % len(path)
        return self.states[next_index]

    def _get_simple_cycle_d(self, path, k, i):
        """
        Returns parts of the monotonicity condition.
        It corresponds to function `monotonicityCondition`
        from the thesis. The difference is that this method
        returns only one element of that tuple.

        Essentially this function build a formula which
        determines whether the full cycle `path` can be
        executed at some point of time (determined by `k`
        and `i`).

        Args:
            path: Full cycle associated with the second phase.
            k: Index shifting iteration of the full cycle.
                The expected values are 1 and 2, where 2 is
                associated with the iteration of the full cycle
                before the iteration associated with index 1.
            i: Variable parametrizing the iteration of the full
                cycle.

        Returns:
            Formula representing the monotonicity condition.
        """

        e = self._get_continue_relation(path, i-k, len(path)-1)
        parts = [e]

        for j in range(len(path) - 1):
            e = self._get_continue_relation(path, i - k + 1, j)
            parts.append(e)

        return sand(*parts)

    def _combine_updates(self, path, u, v: int):
        """
        Returns a matrix which represents the values after
        execution of `v`th matrix in `u`th iteration of the
        full cycle.

        Note that in the thesis v ranges from 1 to |T|.
        Here it ranges from 0 to len(path)-1.

        Args:
            path: Path representing the full cycle.
            u: Integer representing iteration of the
                full cycle.
            v: Integer representing position in the last
                iteration of the full cycle.

        Returns:
            Matrix representing the execution.
        """

        identity_size = len(Variables.initial) + 1
        m = WMatrix(identity_size=identity_size)
        n = WMatrix(identity_size=identity_size)

        for index in range(len(path)):
            state_index = path[index]
            state = self.states[state_index]
            branch = state.branches[-1]
            update_matrix = branch.update_matrix
            m = update_matrix * m

            if index <= v:
                n = update_matrix * n

        return n * m**(u-1)

    def _get_continue_relation(self, path, u: int, v: int):
        """
        Returns a formula whose satisfiability means that it
        is possible to proceed with the execution of the full
        cycle after executing `v`th state in `u`th iteration
        of the full cycle.

        Note that in the thesis v ranges from 1 to |T|.
        Here it ranges from 0 to len(path)-1.

        Args:
            path: Path representing the full cycle.
            u: Integer representing iteration of the
                full cycle.
            v: Integer representing position in the last
                iteration of the full cycle.

        Returns:
            Formula representing the possibility to continue.
        """

        m = self._combine_updates(path, u, v)
        result = sympy_result_vector_to_z3(m * nu())

        next_state_index = path[(v+1) % len(path)]
        next_state = self.states[next_state_index]
        next_branch = next_state.branches[-1]
        next_guard = next_branch.guard

        substitutions = []

        for index, initial in enumerate(Variables.initial):
            substitutions.append((initial, result[index]))

        d = ssub(self.loop.loop_condition, *substitutions)
        e = ssub(next_guard, *substitutions)

        return sand(d, e)

    def _get_second_phase_path(self, end_index):
        """
        Returns path representing the full cycle associated
        with the second phase of executing a simple cycle.

        Args:
            end_index: Index of the terminal state of the
                full cycle.

        Returns:
            List of state indices.
        """

        next_edges = self._get_next_edges(end_index)
        next_edge = next_edges.pop()
        after_end_index = next_edge[1]
        paths = self._get_paths_between(after_end_index, end_index)
        return paths[0]

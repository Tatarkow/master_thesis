import os

from exceptions import InfiniteLoop


def compare(input_loop, loop, all_initial_values):
    loop.summarize()
    verbose = int(os.getenv("VERBOSE"))

    if verbose:
        print(f"\nRESULTS:\ninput\t\t| obtained\t| expected\t| path")
        print("-"*54)

    for initial_values in all_initial_values:
        infinite_loop = False

        if initial_values[-1] is None:
            infinite_loop = True
            initial_values = initial_values[:-1]

        if verbose:
            print(f"{initial_values}  \t| ", end="")

        if infinite_loop:
            try:
                loop.compute_terminal_values(*initial_values)
                assert False, "TEST FAILED"
            except InfiniteLoop:
                if verbose:
                    print(f"-\t\t| -\t\t| [INFINITE PATH]")
        else:
            terminal_values = loop.compute_terminal_values(*initial_values)
            *expected_terminal_values, path = input_loop(*initial_values)

            if verbose:
                print(f"{terminal_values} \t| {expected_terminal_values} \t| {path}")

            assert terminal_values == expected_terminal_values, "TEST FAILED"

    if verbose:
        print()

    print("TEST PASSED")

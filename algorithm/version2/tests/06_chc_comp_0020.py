from compare import compare
from wsummarizer import WSummarizer as ws


def input_loop(x, y):
    path = ""

    while x <= 99:
        if x >= 51:
            x = x + 1
            y = y + 1
            path += "A"
        else:
            x = x + 1
            path += "B"

    return [x, y, path]


x, y = ws.create_variables(2)

loop = ws.define_loop(x <= 99, [
    (x >= 51, [
        [1, 0, 1],
        [0, 1, 1],
    ]),
    (x <= 50, [
        [1, 0, 1],
        [0, 1, 0],
    ]),
])

all_initial_values = [
    [1000, -2],
    [99, 1],
    [90, 1],
    [45, 1],
]

compare(input_loop, loop, all_initial_values)

from compare import compare
from wsummarizer import WSummarizer as ws


def input_loop(x, y, z):
    path = ""

    while x >= 1 and y <= z:
        if x >= 1 and y <= z:
            x = x - 1
            y = y + 1
            path += "A"

    return [x, y, z, path]


x, y, z = ws.create_variables(3)

loop = ws.define_loop([x >= 1, y <= z], [
    ([x >= 1, y <= z], [
        [1, 0, 0, -1],
        [0, 1, 0, 1],
        [0, 0, 1, 0],
    ]),
])

all_initial_values = [
    [0, 2, 3],
    [1, -4, 5],
    [10, 3, 4],
    [3, -6, 8],
]

compare(input_loop, loop, all_initial_values)

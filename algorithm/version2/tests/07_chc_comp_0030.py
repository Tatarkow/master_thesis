from compare import compare

from exceptions import NotSummarizable
from wsummarizer import WSummarizer as ws


def input_loop(x, y, z, w):
    path = ""

    while x >= 1:
        if x >= 1:
            x = x - y
            y = y - z
            z = -z
            w = w - 1
            path += "A"

    return [x, y, path]


x, y, z, w = ws.create_variables(4)

loop = ws.define_loop(x >= 1, [
    (x >= 1, [
        [1, -1, 0, 0, 0],
        [0, 1, -1, 0, 0],
        [0, 0, -1, 0, 0],
        [1, 0, 0, 0, 1],
    ]),
])

all_initial_values = [
    [1, 1, 1, 1],
]

# For instance (x_0, y_0, z_0, w_0) = (0, -3, 1, 1)
# and (x_1, y_1, z_1, w_1) = (x_0 - y_0, y_0 - z_0, -z_0, w_0 - 1) = (3, -4, -1, 0)
# violates the condition from function `_summarize_second_phase_repeat`.
try:
    loop.summarize()
except NotSummarizable:
    print("TEST SKIPPED")

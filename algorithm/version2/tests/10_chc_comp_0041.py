from compare import compare
from wsummarizer import WSummarizer as ws


def input_loop(x, y, z, w, v):
    path = ""

    while w <= 2*x - 1:
        if v == 0:
            y = y + 1
            w = w + 1
            v = 1
            path += "A"
        else:
            z = z + 1
            w = w + 1
            v = 0
            path += "B"

    return [x, y, z, w, v, path]


x, y, z, w, v = ws.create_variables(5)

loop = ws.define_loop(w <= 2*x - 1, [
    (v == 0, [
        [1, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 1],
        [0, 0, 1, 0, 0, 0],
        [0, 0, 0, 1, 0, 1],
        [0, 0, 0, 0, 0, 1],
    ]),
    (v != 0, [
        [1, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 1],
        [0, 0, 0, 1, 0, 1],
        [0, 0, 0, 0, 0, 0],
    ]),
])

all_initial_values = [
    [4, 0, 0, 8, 0],
    [2, -3, 1, 3, 5],
    [7, 1, 2, 5, 0],
    [-2, 0, -5, -9, 2],
]

compare(input_loop, loop, all_initial_values)

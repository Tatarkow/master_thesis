from compare import compare
from wsummarizer import WSummarizer as ws


def input_loop(x, y):
    path = ""

    while x <= 999:
        if x >= 0 and y >= 1:
            x = 2*x
            y = y - 1
            path += "A"
        elif x >= 1 and y <= 0:
            x = x - 1
            y = y - 2
            path += "B"
        elif x <= -1 and y >= 0:
            x = x + 1
            y = 3*y
            path += "C"
        elif x <= 0 and y <= -1:
            y = y + 1
            path += "D"
        elif x == 0 and y == 0:
            y = y + 1
            path += "E"

    return [x, y, path]


x, y = ws.create_variables(2)

loop = ws.define_loop(x <= 999, [
    ([x >= 0, y >= 1], [
        [2, 0, 0],
        [0, 1, -1],
    ]),
    ([x >= 1, y <= 0], [
        [1, 0, -1],
        [0, 1, -2],
    ]),
    ([x <= -1, y >= 0], [
        [1, 0, 1],
        [0, 3, 0],
    ]),
    ([x <= 0, y <= -1], [
        [1, 0, 0],
        [0, 1, 1],
    ]),
    ([x == 0, y == 0], [
        [1, 0, 0],
        [0, 1, 1],
    ]),
])

all_initial_values = [
    [1000, -2],
    [500, 1],
    [723, 9],
    [300, 9],
    [200, 9],
    [100, 9],
    [50, 9],
    [300, -1, None],
    [-200, 5, None],
    [0, -100, None],
    [0, 0, None],
]

compare(input_loop, loop, all_initial_values)

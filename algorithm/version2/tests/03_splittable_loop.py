from compare import compare
from wsummarizer import WSummarizer as ws


def input_loop(x, y):
    path = ""

    while x >= 1:
        if y <= -1:
            y = y + 1
            path += "A"
        elif y == 0:
            x = x + 2
            y = 3
            path += "B"
        else:
            x = x - y
            y = y - 1
            path += "C"

    return [x, y, path]


x, y = ws.create_variables(2)

loop = ws.define_loop(x >= 1, [
    (y <= -1, [
        [1, 0, 0],
        [0, 1, 1],
    ]),
    (y == 0, [
        [1, 0, 2],
        [0, 0, 3],
    ]),
    (y >= 1, [
        [1, -1, 0],
        [0, 1, -1],
    ]),
])

all_initial_values = [
    [-4, 3],
    [2, 5],
    [1, 0],
    [2, 0],
    [4, 0],
    [1, -1],
    [2, -4],
    [12, -2],
]

compare(input_loop, loop, all_initial_values)

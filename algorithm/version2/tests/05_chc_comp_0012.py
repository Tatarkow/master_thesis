from compare import compare
from wsummarizer import WSummarizer as ws


def input_loop(x, y):
    path = ""

    while x <= y:
        if x <= y:
            x = x + 2
            y = y - 1
            path += "A"

    return [x, y, path]


x, y = ws.create_variables(2)

loop = ws.define_loop(x <= y, [
    (x <= y, [
        [1, 0, 2],
        [0, 1, -1],
    ]),
])

all_initial_values = [
    [10, 3],
    [4, 4],
    [-7, -7],
    [2, 8],
    [-3, 18],
]

compare(input_loop, loop, all_initial_values)

from compare import compare
from wsummarizer import WSummarizer as ws


def input_loop(x, y, z, w):
    path = ""

    while x <= 2*y - 1:
        if w == 0:
            x = x + 1
            z = z + 1
            w = 1
            path += "A"
        else:
            x = x + 1
            w = 0
            path += "B"

    return [x, y, z, w, path]


x, y, z, w = ws.create_variables(4)

loop = ws.define_loop(x <= 2*y - 1, [
    (w == 0, [
        [1, 0, 0, 0, 1],
        [0, 1, 0, 0, 0],
        [0, 0, 1, 0, 1],
        [0, 0, 0, 0, 1],
    ]),
    (w != 0, [
        [1, 0, 0, 0, 1],
        [0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0],
    ]),
])

all_initial_values = [
    [4, 2, 0, 0],
    [7, 4, -7, 0],
    [7, 6, -7, 0],
    [-1, 0, 2, 1],
    [-5, 3, 2, 1],
]

compare(input_loop, loop, all_initial_values)

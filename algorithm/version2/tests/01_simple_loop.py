from compare import compare
from wsummarizer import WSummarizer as ws


def input_loop(x, y):
    path = ""

    while x >= 1:
        if y >= 1:
            y = x + y
            x = x - 1
            path += "A"
        else:
            y = x - y
            path += "B"

    return [x, y, path]


x, y = ws.create_variables(2)

loop = ws.define_loop(x >= 1, [
    (y >= 1, [
        [1, 0, -1],
        [1, 1, 0],
    ]),
    (y <= 0, [
        [1, 0, 0],
        [1, -1, 0],
    ]),
])

all_initial_values = [
    [-3, 5],
    [1, 3],
    [5, 4],
    [7, 3],
    [2, -3],
    [12, -3],
]

compare(input_loop, loop, all_initial_values)

from compare import compare
from wsummarizer import WSummarizer as ws


def input_loop(x, y):
    path = ""

    while x <= -1:
        if x <= -1:
            x = x + y
            y = y + 1
            path += "A"

    return [x, y, path]


x, y = ws.create_variables(2)

loop = ws.define_loop(x <= -1, [
    (x <= -1, [
        [1, 1, 0],
        [0, 1, 1],
    ]),
])

all_initial_values = [
    [10, 3],
    [-1, 7],
    [-10, 3],
    [-5, -2],
]

compare(input_loop, loop, all_initial_values)

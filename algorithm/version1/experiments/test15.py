from z3 import *


class WSummarizer:
    @staticmethod
    def get_variables(*args):
        variables = {}

        for variable in args:
            variables[variable] = Int(variable)

        return variables

    @staticmethod
    def is_satisfiable(condition):
        solver = Solver()
        solver.add(condition)
        result = str(solver.check())

        if result == "sat":
            return True
        elif result == "unsat":
            return False
        else:
            raise Exception


class Branch:
    def __init__(self, condition, updates):
        self.condition = condition
        self.updates = updates


class Diagram:
    pass


class Loop:
    def __init__(self, variables, condition, branches):
        # self.validate()

        self.variables = variables
        self.condition = condition
        self.branches = branches
        self.solver = None

    def substitute(self, condition, updates):
        substitutions = []

        for variable_name in self.variables:
            variable = self.variables[variable_name]
            substitution = (variable, updates[variable_name])
            substitutions.append(substitution)

        return substitute(condition, substitutions)

    def solver_single_branch(self):
        pass

    def summarize(self):
        if self.solver:
            return self.solver

        if len(self.branches) == 1:
            return self.solve_single_branch()

        options = []

        # The loop is not executed at all.
        options.append(Not(self.condition))

        # ---------------------------------------------------------------------
        # Terminates after the first iteration.
        for branch in self.branches:
            # Condition to begin the second iteration.
            next_condition = self.substitute(self.condition, branch.updates)

            # Condition that the loop terminates after the first iteration.
            terminal_condition = And(self.condition, Not(next_condition))

            # Can the loop terminate after executing this branch once?
            is_terminal = WSummarizer.is_satisfiable(terminal_condition)

            print(is_terminal)

            options.append(terminal_condition)

        # ---------------------------------------------------------------------
        # Compute feasibility of transitions.
        for start_index, start_branch in enumerate(self.branches):
            for end_index, end_branch in enumerate(self.branches):
                tau = self.get_tau(start_branch, end_branch)
                print(start_index, end_index, WSummarizer.is_satisfiable(tau))

        solver = Solver()
        solver.add(Or(*options))
        print(solver)
        print(solver.check())
        # self.summarization = s

        # while True:
        #     if (dead_branches):
        #         pass ## cut them
        #     elif (simple_circle):
        #         pass ## analyzie cases
        #     elif (isolated_branches):
        #         pass ## split loop

    def get_tau(self, start_branch: Branch, end_branch: Branch):
        c = self.condition
        c_i = start_branch.condition
        d_i = z3.simplify(self.substitute(self.condition, start_branch.updates))
        d_i_j = z3.simplify(self.substitute(end_branch.condition, start_branch.updates))

        return z3.simplify(And(c, c_i, d_i, d_i_j))

    def compute(self, initial_values):
        pass


x = "x"
y = "y"
v = WSummarizer.get_variables(x, y)

loop = Loop(
    v,
    v[x] >= 1,
    [
        Branch(
            v[y] >= 1,
            {
                x: v[x] - 1,
                y: v[x] + v[y],
            }
        ),
        Branch(
            v[y] <= 0,
            {
                x: v[x],
                y: v[x] - v[y],
            }
        )
    ]
)

loop.summarize()
loop.compute({x: 3, y: 4})

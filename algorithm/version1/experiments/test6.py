from z3 import *

x = Int('x')
y = Int('y')

# Terminals.
xt = Int('xt')
yt = Int('yt')

c = x >= 1  # while condition

c_a = y >= 1  # state A condition
px_a = x - 1  # effect on x of state A
py_a = x + y  # effect on y of state A

c_b = y <= 0  # state B condition
px_b = x  # effect on x of state B
py_b = x - y  # effect on y of state B

# tau_aa = Solver()
# d_a = substitute(c, (x, px_a), (y, py_a))
# d_a_a = substitute(c_a, (x, px_a), (y, py_a))
# tau_aa.add(c, c_a, d_a, d_a_a)
# print(tau_aa)
# print(tau_aa.check())
# print()
#
# tau_ab = Solver()
# # d_a = substitute(c, (x, px_a), (y, py_a))
# d_a_b = substitute(c_b, (x, px_a), (y, py_a))
# tau_ab.add(c, c_a, d_a, d_a_b)
# print(tau_ab)
# print(tau_ab.check())
# print()

tau_ba = Solver()
d_b = substitute(c, (x, px_b), (y, py_b))
d_b_a = substitute(c_a, (x, px_b), (y, py_b))
tau_ba.add(c, c_b, d_b, d_b_a)
print(tau_ba)
print(tau_ba.check())
print()

result_x_0 = xt == x
result_y_0 = yt == y

effect_a_x = x - 1
effect_a_y = x + y

effect_b_x = x
effect_b_y = x - y

# result_x_1 = substitute(px_b, (x, result_x_0))
result_x_1 = xt == effect_a_x
result_y_1 = yt == effect_a_y

result_x_2 = substitute(result_x_1, (x, effect_b_x), (x, effect_b_y))
result_y_2 = substitute(result_y_1, (x, effect_b_x), (x, effect_b_y))
# result_x_2 = xt == substitute(effect_a_x, (x, effect_b_x), (x, effect_b_y))


print("RM")
print(result_x_1)
print(result_y_1)
print(result_x_2)
print(result_y_2)

tau_ba.add(result_x_2)
tau_ba.add(result_y_2)
print(tau_ba)
print(tau_ba.check())
print()

tau_ba.add(x == 3)
tau_ba.add(y == -2)
print(tau_ba)
print(tau_ba.check())
print(tau_ba.model())

# tau_bb = Solver()
# # d_b = substitute(c, (x, px_b), (y, py_b))
# d_b_b = substitute(c_b, (x, px_b), (y, py_b))
# tau_bb.add(c, c_b, d_b, d_b_b)
# print(tau_bb)
# print(tau_bb.check())
# print()


# Will the loop execute at least once?
# solver_loop = Solver()
# solver_loop.add(c)
# print(solver_loop.check())

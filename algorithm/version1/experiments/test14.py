from z3 import *

f = Function('f', IntSort(), IntSort(), IntSort(), BoolSort())
i = Function('i', IntSort(), IntSort(), BoolSort())

x = Int('x')
xt = Int('xt')
n = Int('n')

fp = Fixedpoint()
# fp.set(engine='pdr')

fp.declare_var(x, xt)
fp.register_relation(f)
fp.register_relation(i)

# fp.fact(n >= 1)

fp.rule(
    i(x, xt),
    And(x >= 1, xt == x - 1),
)

fp.rule(
    f(x, xt, n),
    Or(
        And(
            i(x, xt),
            xt <= 0,
            # n == 1,
        ),
        And(
            x >= 1,
            f(x-1, xt, n),
        )
    )
)

print(fp.query(f(x, xt, n)))
print(fp.get_answer())

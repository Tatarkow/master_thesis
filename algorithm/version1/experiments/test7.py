from z3 import *

x = Int('x')
y = Int('y')
n = Int('n')

f_a_x = Function('f_a_x', IntSort(), IntSort(), IntSort())
f_a_y = Function('f_a_y', IntSort(), IntSort(), IntSort())

solve(f_a_x(x, y) == x - 1)
solve(f_a_y(x, y) == x + y)

g_a_x = Function('g_a_x', IntSort(), IntSort(), IntSort(), IntSort())
g_a_y = Function('g_a_y', IntSort(), IntSort(), IntSort(), IntSort())

solve(g_a_x(x, y, n) == x if n >= 2 else y)

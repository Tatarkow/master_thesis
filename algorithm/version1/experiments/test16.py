# import numpy as np
# from numpy.linalg import svd, matrix_power
#
# M = np.array([
#     [ 2, 0, 0, 1],
#     [-1, 3, 0, 2],
#     [ 4,-2,-3,-5],
#     [ 0, 0, 0, 1],
# ])
#
# print(M)
#
# U, d, VT = svd(M)
# D = np.diag(d)
#
# print(VT.dot(U))
#
# print(U.dot(D.dot(VT)).astype(int))
# print(matrix_power(M, 2))
# print(matrix_power(M, 3))

# Reconstruct SVD
from numpy import array
from numpy import diag
from numpy import dot
from numpy import zeros
from numpy.linalg import svd, eig, matrix_power, inv
import numpy as np

# define a matrix
# A = array([[1, 2], [3, 4], [5, 6]])
# print(A)
# # Singular-value decomposition
# U, s, VT = svd(A)
# # create m x n Sigma matrix
# Sigma = zeros((A.shape[0], A.shape[1]))
# # populate Sigma with n x n diagonal matrix
# Sigma[:A.shape[1], :A.shape[1]] = diag(s)
# # reconstruct matrix
# B = U.dot(Sigma.dot(VT))
# print(B)
#
# print(VT.dot(U))


M = np.array([
    [1, 0, -1],
    [1, 1, 0],
    [0, 0, 1],
])
# d, V = eig(M)
# D = diag(d)
#
# print(V)
# print(D)
# print(inv(V))
# print(V.dot(D).dot(inv(V)).astype(int))

from sympy import Matrix

S = Matrix(M)
print(S)
P, J = S.jordan_form()

print(P)
print(J)
print(P.inv())
print(P*J*P.inv())

# print(matrix_power(M, 3))
# print(matrix_power(M, 4))
# print("---")
# values, vectors = eig(M)
# D = np.diag(values)
# print(D)
# print("---")
# print(vectors)
# print(vectors.dot(vectors.transpose()))
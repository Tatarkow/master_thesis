from z3 import *

s = Solver()
A = Int('A')
n = Int('n')
k = Int('k')
y4 = Function('f', IntSort(), IntSort())

s.add(A > 0)
s.add(
    ForAll(
        [n],
        Implies(
            n >= 0,
            y4(n + 1) == If(
                y4(n) <= A,
                y4(n) + 1,
                y4(n) - 1,
            )
        )
    )
)

s.add(y4(0) == 0)
s.add(Not(ForAll([n],Exists([k],Implies(And(k>0,n>=0),y4(n)==y4(n+k))))))

# print(s.sexpr())
print(s.check())

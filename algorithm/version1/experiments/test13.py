from z3 import *

f = Function('fa', IntSort(), IntSort(), BoolSort())
i = Function('ia', IntSort(), IntSort(), BoolSort())

x = Int('x')
xt = Int('xt')

fp = Fixedpoint()

fp.declare_var(x, xt)
fp.register_relation(f)
fp.register_relation(i)

fp.rule(
    i(x, xt),
    And(x >= 1, xt == x - 1),
)

fp.rule(
    f(x, xt),
    Or(
        And(
            i(x, xt),
            xt <= 0,
        ),
        And(
            x >= 1,
            f(x-1, xt),
        )
    )
)

print(fp.query(f(x, xt)))
print(fp.get_answer())

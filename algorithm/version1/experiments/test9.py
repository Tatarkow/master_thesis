from z3 import *

# Loop execution when only A is visited
fa = Function('fa', IntSort(), IntSort(), IntSort(), IntSort(), BoolSort())

# One iteration in A.
ia = Function('ia', IntSort(), IntSort(), IntSort(), IntSort(), BoolSort())


x = Int('x')
y = Int('y')
xt = Int('xt')
yt = Int('yt')
# n = Int('n')

fp = Fixedpoint()

fp.declare_var(x, y, xt, yt)
fp.register_relation(fa)
fp.register_relation(ia)

fp.rule(
    ia(x, y, xt, yt),
    And(x >= 1, y >= 1, xt == x - 1, yt == x + y),
)

fp.rule(
    fa(x, y, xt, yt),
    Or(
        ia(x, y, xt, yt),
        And(
            ia(x, y, xt, yt),
            fa(x-1, x+y, xt, yt),
        )
    )
)

print(fp.query(fa(x, y, xt, yt), And(x == 3, y == 4, xt == 1, yt == 9)))
print(fp.get_answer())
# print(fp.get_rules_along_trace())

# mc = Function('mc', IntSort(), IntSort(), BoolSort())
# n = Int('n')
# m = Int('m')
# p = Int('p')
#
# fp.rule(mc(m, m - 10), m > 100)
# fp.rule(mc(m, n), [m <= 100, mc(m + 11, p), mc(p, n)])
#
# print(fp.query(And(mc(m, n), n < 90)))
# print(fp.get_answer())
#
# print(fp.query(And(mc(m, n), n < 91)))
# print(fp.get_answer())
#
# print(fp.query(And(mc(m, n), n < 92)))
# print(fp.get_answer())
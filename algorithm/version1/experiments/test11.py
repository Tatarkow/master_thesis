from z3 import *

s = Solver()

x = Int('x')
i = Int('i')
n = Int('n')

# fi = Function('fi', IntSort(), IntSort())
# fl = Function('fl', IntSort(), IntSort())
f = Function('f', IntSort(), IntSort(), IntSort())

print("B")
s.add(f(x, 1) == x - 1)
print("A")
s.add(ForAll(i <= 3, f(x, i) == f(x, i-1) - 1))


# s.add(f(x, i) == f(x, i-1) - 1)
# s.add(f(x, n) <= 0)
#
# s.add(Implies(i <= n - 1, f(x, i) >= 1))
# s.add(n >= 0)

# s.add()

# s.add(y4(0) == 0)
# add(Not(ForAll([n],Exists([k],Implies(And(k>0,n>=0),y4(n)==y4(n+k))))))
#
# # print(s.sexpr())
print(s.check())
print(s.model())
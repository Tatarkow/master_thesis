import numpy as np
from sympy import Matrix
import z3

x = z3.Int("x")

M = np.array([
    [2*x, 0],
    [1, 1],
])

S = Matrix(M)
print(S)

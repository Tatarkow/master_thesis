from wsummarizer import get_variables, Loop, Branch

x = "x"
y = "y"
v = get_variables(x, y)

loop = Loop(
    v,
    [[v[x] <= 999]],
    [
        Branch(
            [v[x] >= 0, v[y] >= 1],
            {
                x: 2*v[x],
                y: v[y] - 1,
            },
            name="A"
        ),
        Branch(
            [v[x] >= 1, v[y] <= 0],
            {
                x: v[x] - 1,
                y: v[y] - 2,
            },
            name="B"
        ),
        Branch(
            [v[x] <= -1, v[y] >= 0],
            {
                x: v[x] + 1,
                y: 3*v[y],
            },
            name="C"
        ),
        Branch(
            [v[x] <= 0, v[y] <= -1],
            {
                x: v[x],
                y: v[y] + 1,
            },
            name="D"
        ),
        Branch(
            [v[x] == 0, v[y] == 0],
            {
                x: v[x],
                y: v[y] + 1,
            },
            name="E"
        ),
    ]
)

loop.summarize()
loop.compute({x: 3, y: 4})

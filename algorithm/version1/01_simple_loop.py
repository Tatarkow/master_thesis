from wsummarizer import get_variables, Loop, Branch

x = "x"
y = "y"
v = get_variables(x, y)

loop = Loop(
    v,
    [[v[x] >= 1]],
    [
        Branch(
            [v[y] >= 1],
            {
                x: v[x] - 1,
                y: v[x] + v[y],
            },
            name="A"
        ),
        Branch(
            [v[y] <= 0],
            {
                x: v[x],
                y: v[x] - v[y],
            },
            name="B"
        ),
    ]
)

loop.summarize()
loop.compute({x: 3, y: -4})

from wsummarizer import get_variables, Loop, Branch

x = "x"
y = "y"
v = get_variables(x, y)

loop = Loop(
    v,

    # Each element is a list that represents conjunction. The conjunctions will be combined as a disjunction.
    [[v[x] >= 1, v[y] >= 1]],

    [
        Branch(
            [v[y] >= 1],
            {
                x: v[x] - 1,
                y: v[x] + v[y],
            }
        ),
    ]
)

loop.summarize()
loop.compute({x: 3, y: 4})

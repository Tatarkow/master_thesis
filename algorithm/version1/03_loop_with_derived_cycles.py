from wsummarizer import get_variables, Loop, Branch

x = "x"
y = "y"
v = get_variables(x, y)

loop = Loop(
    v,
    [[v[x] >= 1]],
    [
        Branch(
            [v[y] <= -1],
            {
                x: v[x],
                y: v[y] + 1,
            },
            name="A"
        ),
        Branch(
            [v[y] == 0],
            {
                x: v[x] + 2,
                y: 0*v[x] + 3,
            },
            name="B"
        ),
        Branch(
            [v[y] >= 1],
            {
                x: v[x] - v[y],
                y: v[y] - 1,
            },
            name="C"
        ),
    ]
)

loop.summarize()
loop.compute({x: 19, y: 5})

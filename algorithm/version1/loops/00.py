def loop(x, y):
    n = 0

    while x >= 1:
        n += 1

        if y >= 1:
            y = x + y
            x = x - 1
        else:
            y = x - y

    return x, y, n


print(loop(3, -4))

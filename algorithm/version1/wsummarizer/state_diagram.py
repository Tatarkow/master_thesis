import z3

import wsummarizer as ws
from wsummarizer.digraph import Digraph
from wsummarizer.loop_diagram import LoopDiagram
from wsummarizer.utils import is_satisfiable


class StateNode:
    def __init__(self, branches, can_be_terminal, index, tau, previous_node=None):
        self.branches = branches
        self.index = index
        self.can_be_terminal = can_be_terminal
        self.tau = tau
        self.previous_node = previous_node

    def __str__(self):
        return "".join([str(branch) for branch in self.branches])

    def get_update_matrix(self):
        all_updates = [branch.updates for branch in self.branches]
        first_updates = all_updates[0]
        matrix = ws.WSMatrix(updates=first_updates)

        if len(all_updates) > 1:
            for updates in all_updates[1:]:
                matrix = ws.WSMatrix(updates=updates) * matrix

        return matrix

    def get_substitutions(self, variables):
        """
        Returns a list of pairs. The first element of the pair is a variable
        and the second is an expression representing the value of the
        variable after executing the branches of this state.
        """

        matrix = self.get_update_matrix()
        updates = matrix.get_updated_variables()
        return ws.updates_to_substitutions(variables, updates)

    def get_did_not_terminated_option(self, variables, condition):
        substitutions = self.get_substitutions(variables)
        while_condition = ws.ssub(condition, *substitutions)

        if self.previous_node:
            previous_while_condition = self.previous_node.get_did_not_terminated_option(variables, condition)
            return ws.sand(while_condition, previous_while_condition)
        else:
            return while_condition

    def get_did_not_terminated_earlier_option(self, variables, condition):
        if self.previous_node:
            return self.previous_node.get_did_not_terminated_option(variables, condition)
        else:
            return True


class StateDiagram(Digraph):
    def __init__(self, precondition, condition, nodes, structure):
        super().__init__()

        self.precondition = precondition
        self.condition = condition
        self.nodes = nodes

        self.variables = structure.variables
        self.terminal_variables = structure.terminal_variables
        self.iteration_count = structure.iteration_count

        self.level = len(nodes[0].branches)

        self._set_edges()

        # self.early_terminations = []

    def _set_edges(self):
        self.taus = []

        for start_index, start_node in enumerate(self.nodes):
            for end_index, end_node in enumerate(self.nodes):
                if not self._are_nodes_connectable(start_node, end_node):
                    continue

                tau = self._get_tau(start_node, end_node)

                if is_satisfiable(tau):
                    self.edges.append((start_index, end_index))
                    self.taus.append(tau)

    def _are_nodes_connectable(self, start_node, end_node):
        for i in range(self.level - 1):
            if start_node.branches[i + 1] != end_node.branches[i]:
                return False

        return True

    def _get_tau(self, start_node, end_node):
        if start_node.tau is None:
            return self._get_state_tau(start_node, end_node)
        else:
            return self._get_path_tau(start_node, end_node=end_node)

    def _get_state_tau(self, start_node, end_node):
        """
        Computes tau of state. For instance tau(B).
        """

        # Each state has only one branch.
        start_branch = start_node.branches[0]
        end_branch = end_node.branches[0]

        # Effect of the start state/branch.
        substitutions = start_branch.get_substitutions()

        c = z3.simplify(self.condition)
        c_i = z3.simplify(start_branch.condition)
        d_i = ws.ssub(self.condition, substitutions)
        d_i_j = ws.ssub(end_branch.condition, substitutions)

        return z3.simplify(z3.And(c, c_i, d_i, d_i_j))

    def _get_path_tau(self, start_node, end_node=None, end_branch=None):
        """
        Computes tau of path of length at least two. For instance tau(BC).
        """

        substitutions = start_node.get_substitutions(self.variables)

        if not end_branch:
            end_branch = end_node.branches[-1]

        tau_rho = start_node.tau
        d_rho = ws.ssub(self.condition, *substitutions)
        d_rho_i = ws.ssub(end_branch.condition, *substitutions)

        return ws.sand(tau_rho, d_rho, d_rho_i)

    def get_option(self):
        self.simplify()

        if self._is_trivial():
            self._print_step("TRIVIAL")
            return self._get_trivial_option()

        if self.is_simple_cycle():
            self._print_step("SIMPLE CIRCLE")
            return self.get_simple_cycle_option()

        if self.is_splitable():
            self._print_step("SPLITABLE")
            return LoopDiagram(self).get_option()

        self._print_step("HIGHER LEVEL")
        early_termination_option = self._get_early_termination_option()
        higher_level_state_diagram = self.get_higher_level()
        higher_level_option = higher_level_state_diagram.get_option()

        all_earlier_while_conditions = []

        for node in self.nodes:
            earlier_while_conditions = node.get_did_not_terminated_earlier_option(self.variables, self.condition)
            all_earlier_while_conditions.append(earlier_while_conditions)

        return ws.sor(
            early_termination_option,
            ws.sand(
                self.iteration_count > self.level,
                higher_level_option,
                ws.sand(*all_earlier_while_conditions),
            ),
        )

    def _print_step(self, step_type):
        step = ws.HelperVariableCounter.get_step()

        print("-" * 40)
        print("Step: ", step)
        print("Type: ", step_type)
        print()

        self._print_nodes()
        self._print_edges()

    def _print_nodes(self):
        print("Nodes:")

        for index, node in enumerate(self.nodes):
            print(f"{node} ({index})")

        print()

    def _print_edges(self):
        if len(self.edges) == 0:
            return

        print("Edges:")

        for edge in self.edges:
            print(edge)

        print()

    def _is_trivial(self):
        """
        Returns True iff the state diagram has only one node and no edge.
        """
        return len(self.nodes) == 1 and len(self.edges) == 0

    def _get_trivial_option(self):
        """
        Get option representing that the sequence of branches in
        the first and only node (of this diagram) will be executed.
        """

        node = self.nodes[0]
        branch_count = len(node.branches)
        matrix = node.get_update_matrix()
        updated_variables = matrix.get_updated_variables()
        terminal_conditions = []

        for variable_index, updated_variable in enumerate(updated_variables):
            variable_name = ws.VariableSorter.indices_to_names[variable_index]
            terminal_name = ws.get_terminal_name(variable_name)
            terminal_variable = self.terminal_variables[terminal_name]
            terminal_conditions.append(terminal_variable == updated_variable)

        return ws.sand(
            self.iteration_count >= branch_count,
            self.precondition,
            ws.sand(*terminal_conditions),
        )

    def get_simple_cycle_option(self):
        options = []

        for start_index in range(len(self.nodes)):
            for terminal_index, terminal_node in enumerate(self.nodes):
                if terminal_node.can_be_terminal:
                    after_terminal_index = self._get_next_index(terminal_index)
                    path = self._get_simple_cycle_path(start_index, terminal_index)
                    full_path = self._get_simple_cycle_path(after_terminal_index, terminal_index)
                    option = self._get_sc_path_option(path, full_path)
                    options.append(option)

        branch_count = len(self.nodes[0].branches)

        return ws.sand(self.iteration_count >= branch_count, self.precondition, ws.sor(*options))

    def _get_sc_path_option(self, path, full_path):
        """
        Argument `path` represents sequence of executions from the start node
        to the first visit of terminal node. For instance, assume that
        we have a cycle A -> B -> C -> D -> A, B is the start node
        and C is the terminal node. Than `path` would represent [B, C].

        Argument `full_path` represents sequence of executions from the node
        directly after the terminal, back to the terminal node.
        Following the example, `full_path` would represent [D, A, B, C].
        """

        path_matrix = self._get_path_matrix(path)
        full_path_matrix = self._get_full_path_matrix(full_path)
        combined_matrix = self._get_combined_matrix(path_matrix, full_path_matrix)

        self._check_sc_computability(full_path, full_path_matrix)

        i = z3.Int("i")
        updated_variables = combined_matrix.get_updated_variables(i=i)

        counter = ws.HelperVariableCounter.get_counter()
        n_path = z3.Int(f"n_{counter}")

        counter = ws.HelperVariableCounter.get_counter()
        n_full_path = z3.Int(f"n_{counter}")

        terminal_substitutions = self._get_sc_substitutions(updated_variables, i, n_full_path)
        preterminal_substitutions = self._get_preterminal_substitutions(full_path, combined_matrix, i, n_full_path)

        # To loop will not terminated prematurely.
        path_tau = self._get_sequence_tau(path)

        # The loop terminates right after reaching the terminal state.
        path_only_option = ws.sand(
            n_full_path == 0,
            ws.snot(ws.ssub(self.condition, *terminal_substitutions)),
        )

        # The full path will be executed at least once.
        full_path_option = ws.sand(
            n_full_path >= 1,
            ws.snot(ws.ssub(self.condition, *terminal_substitutions)),
            ws.ssub(self.condition, *preterminal_substitutions),
        )

        n = self.iteration_count
        path_branch_count = len(path) + self.level - 1
        full_path_branch_count = len(full_path) + self.level - 1

        options = self._get_sc_terminal_value_options(updated_variables, i, n_full_path)

        return ws.sand(
            n_path == path_branch_count,
            n == n_path + n_full_path*full_path_branch_count,
            ws.sor(path_only_option, full_path_option),
            path_tau,
            *options,
        )

    def _get_preterminal_substitutions(self, full_path, combined_matrix, i, n_full_path):
        if len(full_path) == 1:
            preterminal_combined_matrix = combined_matrix
        else:
            preterminal_path = full_path[:-1]
            preterminal_matrix = self._get_path_matrix(preterminal_path)
            preterminal_combined_matrix = preterminal_matrix * combined_matrix

        preterminal_updated_variables = preterminal_combined_matrix.get_updated_variables(i=i)
        preterminal_substitutions = self._get_sc_substitutions(preterminal_updated_variables, i, n_full_path - 1)

        return preterminal_substitutions

    def _get_sc_terminal_value_options(self, updated_variables, i, n_full_path):
        options = []

        for index, variable_name in enumerate(self.variables):
            terminal_name = ws.get_terminal_name(variable_name)
            terminal_variable = self.terminal_variables[terminal_name]
            updated_variable = updated_variables[index]
            terminal_value = ws.ssub(updated_variable, (i, n_full_path))
            options.append(terminal_variable == terminal_value)

        return options

    def _get_sc_substitutions(self, updated_variables, i, iteration_counter):
        substitutions = []

        for variable_index, variable_name in enumerate(self.variables):
            updated_variable = updated_variables[variable_index]
            updated_value = ws.ssub(updated_variable, (i, iteration_counter))
            variable = self.variables[variable_name]
            substitutions.append((variable, updated_value))

        return substitutions

    def _check_sc_computability(self, full_path, full_path_matrix):
        """
        By our method, a loop can be summarized only if
        the while condition holds for the first i
        iterations if and only if it holds for the ith
        iteration.
        """
        # TODO: This might be improved by loop acceleration.
        # TODO: Implement.
        pass

    @staticmethod
    def _get_combined_matrix(path_matrix, full_path_matrix):
        """
        Returns a matrix that represents combined effect of
        first applying once `path_matrix` and than applying
        i times `full_path_matrix`. The combined matrix thus
        contains string elements including "i".
        """

        matrix_p, matrix_j = full_path_matrix.jordan_form()
        jordan_cells = matrix_j.get_jordan_cells()
        powered_jordan_cells = []

        for jordan_cell in jordan_cells:
            powered_jordan_cell = jordan_cell.power_jordan_cell()
            powered_jordan_cells.append(powered_jordan_cell)

        powered_matrix_j = ws.WSMatrix.compose_jordan_matrix(powered_jordan_cells)
        powered_full_path_matrix = matrix_p * powered_matrix_j * matrix_p.inv()

        return powered_full_path_matrix * path_matrix

    def _get_path_matrix(self, path):
        first_index = path[0]
        first_node = self.nodes[first_index]
        matrix = first_node.get_update_matrix()

        for next_node_index in path[1:]:
            next_node = self.nodes[next_node_index]
            next_branch = next_node.branches[-1]
            next_updates = next_branch.updates
            matrix = ws.WSMatrix(updates=next_updates) * matrix

        return matrix

    def _get_full_path_matrix(self, full_path):
        first_index = full_path[0]
        first_node = self.nodes[first_index]
        first_branch = first_node.branches[-1]
        first_updates = first_branch.updates
        matrix = ws.WSMatrix(updates=first_updates)

        for next_node_index in full_path[1:]:
            next_node = self.nodes[next_node_index]
            next_branch = next_node.branches[-1]
            next_updates = next_branch.updates
            matrix = ws.WSMatrix(updates=next_updates) * matrix

        return matrix

    def _get_sequence_tau(self, path):
        """
        Computes tau for the sequence of branches derived from
        the path. For instance providing path representing nodes
        N and M, where N has branches [A, B] and M had branches
        [B, C], will yield tau(ABC).
        """

        node_index = path[0]
        node = self.nodes[node_index]
        tau = node.tau

        if len(path) == 1:
            return tau

        for next_node_index in path[1:]:
            next_node = self.nodes[next_node_index]
            next_branch = next_node.branches[-1]
            combined_branches = node.branches + [next_branch]
            tau = self._get_path_tau(node, end_branch=next_branch)
            node = StateNode(combined_branches, None, None, tau)

        return tau

    def _get_tau_from_indices(self, node_index, next_node_index):
        for edge, tau in zip(self.edges, self.taus):
            if edge[0] == node_index and edge[1] == next_node_index:
                return tau

    def simplify(self):
        """
        Deletes nodes from which it is impossible to reach some terminal state.
        """
        # TODO
        pass

    @staticmethod
    def _extract_option(solver):
        options = solver.assertions()
        return z3.And(*options)

    def debug(self, option):
        s = z3.Solver()
        s.add(option)
        s.add(self.variables["x"] == 2)
        s.add(self.variables["y"] == 2)
        print(s)
        print("DEBUG", s.check())
        try:
            print(s.model())
        except:
            pass
        print()

    def _get_terminal_equations(self, substitutions):
        terminal_equations = []

        for variable, updated_variable in substitutions:
            variable_id = variable.get_id()
            variable_name = ws.VariableSorter.ids_to_names[variable_id]
            terminal_name = ws.get_terminal_name(variable_name)
            terminal = self.terminal_variables[terminal_name]
            terminal_equations.append(terminal == updated_variable)

        return terminal_equations

    def _get_early_termination_option(self):
        options = []

        for node in self.nodes:
            substitutions = node.get_substitutions(self.variables)
            terminal_equations = self._get_terminal_equations(substitutions)
            while_condition = ws.ssub(self.condition, *substitutions)

            option = ws.sand(
                self.iteration_count == self.level,
                ws.snot(while_condition),
                *terminal_equations,
            )

            options.append(option)

        return ws.sor(*options)

    def get_higher_level(self):
        new_nodes = []
        new_taus = []

        for edge, tau in zip(self.edges, self.taus):
            first_node = self.nodes[edge[0]]
            second_node = self.nodes[edge[1]]
            can_be_terminal = self._can_be_terminal(first_node, second_node)
            branches = first_node.branches + [second_node.branches[-1]]
            new_node = StateNode(branches, can_be_terminal, len(new_nodes), tau, previous_node=first_node)
            new_nodes.append(new_node)
            new_taus.append(tau)

        precondition = z3.simplify(z3.Or(*new_taus))
        diagram = StateDiagram(precondition, self.condition, new_nodes, self)

        return diagram

    def _can_be_terminal(self, first_node, second_node):
        """
        Determines whether it is possible to start in the first
        node and terminate in the second node. Only pair of nodes
        whose tau is satisfiable are expected.
        """

        if not second_node.can_be_terminal:
            return False

        all_updates = [branch.updates for branch in first_node.branches]
        last_branch = second_node.branches[-1]
        all_updates += [last_branch.updates]
        first_updates = all_updates[0]
        matrix = ws.WSMatrix(updates=first_updates)

        for updates in all_updates[1:]:
            matrix = ws.WSMatrix(updates=updates) * matrix

        last_updated_variables = matrix.get_updated_variables()
        last_substitutions = ws.updates_to_substitutions(self.variables, last_updated_variables)
        last_option = ws.snot(ws.ssub(self.condition, *last_substitutions))

        return is_satisfiable(last_option)

import z3


def is_z3_variable(expression):
    return len(expression.children()) == 0 and type(expression) == z3.ArithRef


def is_z3_constant(expression):
    return expression.num_args() == 0 and type(expression) == z3.IntNumRef


def is_z3_term(expression):
    children = expression.children()

    if len(children) != 2:
        return False

    if type(expression) != z3.ArithRef:
        return False

    first_type = type(children[0])
    second_type = type(children[1])

    coefficient_first = first_type == z3.IntNumRef and second_type == z3.ArithRef
    coefficient_second = first_type == z3.ArithRef and second_type == z3.IntNumRef

    if coefficient_first or coefficient_second:
        return True
    else:
        return False

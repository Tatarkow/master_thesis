from copy import deepcopy

import z3

import wsummarizer as ws


class LoopNode:
    def __init__(self, state_diagram, index):
        self.state_diagram = state_diagram
        self.option = None
        self.index = index

        self.can_be_terminal = self._can_be_terminal()

    def _can_be_terminal(self):
        for node in self.state_diagram.nodes:
            if node.can_be_terminal:
                return True

        return False

    def get_option(self):
        if self.option is None:
            self.option = self.state_diagram.get_option()

        return self.option


class LoopDiagram(ws.Digraph):
    def __init__(self, state_diagram):
        super().__init__()

        self.condition = state_diagram.condition
        self.precondition = state_diagram.precondition
        self.variables = state_diagram.variables
        self.terminal_variables = state_diagram.terminal_variables
        self.iteration_count = state_diagram.iteration_count

        self._set_nodes(state_diagram.components)
        self._set_edges(state_diagram.edges)

    def _get_node(self, index):
        for node in self.nodes:
            if node.index == index:
                return node

    def _set_nodes(self, components):
        for component in components:
            state_nodes = []
            all_branches = []
            preconditions = []

            for node in component.nodes:
                all_branches += node.branches
                state_nodes.append(node)
                preconditions.append(node.tau)

            branch_conditions = [branch.condition for branch in all_branches]
            precondition = ws.sand(self.precondition, ws.sor(*preconditions))
            new_condition = ws.sand(self.condition, ws.sor(*branch_conditions))
            state_diagram = ws.StateDiagram(precondition, new_condition, state_nodes, self)
            loop_node = ws.LoopNode(state_diagram, len(self.nodes))

            self.nodes.append(loop_node)

    def _set_edges(self, edges):
        loop_edges = set()
        for start_index, loop_node in enumerate(self.nodes):
            for state_node in loop_node.state_diagram.nodes:
                reachable_state_nodes = self._get_reachable_nodes(state_node.index, edges)

                for reachable_state_node in reachable_state_nodes:
                    for end_index, other_loop_node in enumerate(self.nodes):
                        if start_index == end_index:
                            continue

                        for other_state_node in other_loop_node.state_diagram.nodes:
                            if reachable_state_node == other_state_node.index:
                                loop_edges.add((start_index, end_index))

        self.edges = list(loop_edges)

    def _get_terminal_indices(self):
        terminal_indices = []

        for index, node in enumerate(self.nodes):
            if node.can_be_terminal:
                terminal_indices.append(index)

        return terminal_indices

    def get_option(self):
        options = []
        terminal_indices = self._get_terminal_indices()

        for start_index, start_node in enumerate(self.nodes):
            for terminal_index in terminal_indices:
                paths = self._get_all_paths(start_index, terminal_index)

                for path in paths:
                    option = self._get_path_option(path)
                    options.append(option)

        return ws.sor(*options)

    def _get_path_option_condition(self):
        substitutions = []

        for variable_name in self.variables:
            variable = self.variables[variable_name]
            terminal_name = ws.get_terminal_name(variable_name)
            terminal = self.terminal_variables[terminal_name]
            substitutions.append((variable, terminal))

        return ws.ssub(self.condition, *substitutions)

    def debug(self, option):
        s = z3.Solver()
        s.add(option)
        s.add(self.variables["x"] == 2)
        s.add(self.variables["y"] == 2)
        print("DEBUG", s.check())

    def _get_path_option(self, path):
        """
        Returns a relation between input and output of an execution
        sequence of components (i.e. nodes of a loop diagram).
        """

        first_index = path[0]
        first_node = self.nodes[first_index]

        condition = self._get_path_option_condition()

        if len(path) == 1:
            path_option = first_node.get_option()
            return ws.sand(ws.snot(condition), path_option)

        path_option = first_node.get_option()

        # self.debug(path_option)

        for second_index in path[1:]:
            second_node = self.nodes[second_index]
            second_option = second_node.get_option()
            # self.debug(second_option)
            path_option = self._get_connected_option(path_option, second_option)

        # self.debug(path_option)

        return ws.sand(ws.snot(condition), path_option)

    def _get_connected_option(self, first_option, second_option):
        counter = ws.HelperVariableCounter.get_counter()
        first_substitutions, second_substitutions = self._get_connected_substitutions(counter)

        n = self.iteration_count
        n_first = z3.Int(f"n__{counter}_1")
        n_second = z3.Int(f"n__{counter}_2")

        first_substitutions.append((n, n_first))
        second_substitutions.append((n, n_second))

        first_option = ws.ssub(first_option, *first_substitutions)
        second_option = ws.ssub(second_option, *second_substitutions)

        return ws.sand(n == n_first + n_second, first_option, second_option)

    def _get_connected_substitutions(self, counter):
        first_substitutions = []
        second_substitutions = []

        for variable_name, terminal_name in zip(self.variables, self.terminal_variables):
            middle_name = f"{variable_name}__{counter}"
            middle_variable = z3.Int(middle_name)

            variable = self.variables[variable_name]
            terminal_variable = self.terminal_variables[terminal_name]

            first_substitutions.append((terminal_variable, middle_variable))
            second_substitutions.append((variable, middle_variable))

        return first_substitutions, second_substitutions

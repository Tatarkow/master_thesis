import z3
from sympy import Matrix
from z3 import z3types

import wsummarizer as ws


class WSMatrix:
    variables = []
    ids_to_names = {}

    @classmethod
    def prepare(cls, variables):
        cls.variables = ws.VariableSorter.sort(variables)

    def __init__(self, sympy_matrix=None, updates=None):
        if not sympy_matrix:
            sympy_matrix = self._get_sympy_matrix(updates)

        self.sympy_matrix = sympy_matrix
        self.size = sympy_matrix.shape[0]

    def __mul__(self, other):
        product_matrix = self.sympy_matrix * other.sympy_matrix
        return WSMatrix(sympy_matrix=product_matrix)

    def _get_sympy_matrix(self, updates):
        variable_names = [name for name in updates]
        matrix = []
        size = len(variable_names) + 1

        for variable_name in variable_names:
            update = updates[variable_name]
            row = self._get_sympy_row(update, size)
            matrix.append(row)

        row = [0 for _ in range(size)]
        row[-1] = 1
        matrix.append(row)

        return Matrix(matrix)

    def _get_sympy_row(self, update, size):
        row = [0 for _ in range(size)]

        if ws.is_z3_variable(update):
            return self._variable_to_row(row, update)

        if ws.is_z3_constant(update):
            return self._constant_to_row(row, update)

        for child_update in update.children():
            if ws.is_z3_variable(child_update):
                row = self._variable_to_row(row, child_update)
            elif ws.is_z3_constant(child_update):
                row = self._constant_to_row(row, child_update)
            elif ws.is_z3_term(child_update):
                coefficient, variable = self._get_coefficient_and_variable(child_update)
                self._variable_to_row(row, variable, coefficient=coefficient)
            else:
                raise Exception

        return row

    @classmethod
    def _variable_to_row(cls, row, variable, coefficient=1):
        name = ws.VariableSorter.ids_to_names[variable.get_id()]
        index = ws.VariableSorter.names_to_indices[name]
        row[index] = coefficient
        return row

    @staticmethod
    def _constant_to_row(row, constant):
        row[-1] = constant.as_long()
        return row

    @staticmethod
    def _get_coefficient_and_variable(expression):
        children = expression.children()

        if type(children[0]) == z3.IntNumRef:
            return children[0].as_long(), children[1]
        else:
            return children[1], children[0].as_long()

    def get_updated_variables(self, i=None):
        """
        Returns a list, whose elements are expressions representing
        values of the variables after applying the matrix.

        Argument `i` is may be part of `coefficient`, and thus
        it is necessary for the `eval`. It represents the iteration
        counter.
        """

        updated_variables = []

        for variable_index, variable in enumerate(self.variables):
            updated_variable = 0

            for term_index in range(self.size):
                coefficient = self.sympy_matrix[variable_index, term_index]
                coefficient = eval(str(coefficient))
                term = self._get_term(term_index, coefficient)
                updated_variable += term

            updated_variables.append(updated_variable)

        return self._update_variables_to_int(updated_variables)

    def _get_term(self, term_index, coefficient):
        if term_index == self.size - 1:
            return coefficient
        else:
            return coefficient * self.variables[term_index]

    @staticmethod
    def _update_variables_to_int(updated_variables):
        """
        TODO: Find a better way to fix the types.
        """

        for index, updated_variable in enumerate(updated_variables):
            try:
                updated_variables[index] = z3.ToInt(updated_variable)
            except z3types.Z3Exception:
                pass

        return updated_variables

    def jordan_form(self):
        matrix_p, matrix_j = self.sympy_matrix.jordan_form()
        matrix_p = WSMatrix(sympy_matrix=matrix_p)
        matrix_j = WSMatrix(sympy_matrix=matrix_j)

        return matrix_p, matrix_j

    def get_jordan_cells(self):
        cell_sizes = self._get_jordan_cell_sizes()
        start = 0
        cells = []

        for cell_size in cell_sizes:
            cell = []

            for row_index in range(start, start + cell_size):
                row = []

                for column_index in range(start, start + cell_size):
                    element = self.sympy_matrix[row_index, column_index]
                    row.append(element)

                cell.append(row)

            symsy_matrix = Matrix(cell)
            ws_matrix = WSMatrix(sympy_matrix=symsy_matrix)
            cells.append(ws_matrix)
            start += cell_size

        return cells

    def _get_jordan_cell_sizes(self):
        last_delimiter = 0
        cell_sizes = []

        for i in range(self.size - 1):
            if self.sympy_matrix[i, i + 1] != 1:
                cell_size = (i + 1) - last_delimiter
                cell_sizes.append(cell_size)
                last_delimiter = i + 1

        cell_size = self.size - last_delimiter
        cell_sizes.append(cell_size)

        return cell_sizes

    def power_jordan_cell(self):
        powered_jordan_cell = []

        for i in range(self.size):
            row = ["0" for _ in range(self.size)]
            powered_jordan_cell.append(row)

        eigenvalue = float(self.sympy_matrix[0, 0])

        for row in range(self.size):
            for column in range(row, self.size):
                index = column - row
                combinatorial_number = self._combinatorial_number("i", index)

                # This is essential for Z3 to be able to compute it.
                if eigenvalue == 0:
                    element = "0"
                elif eigenvalue == 1:
                    element = combinatorial_number
                else:
                    element = f"{combinatorial_number} * {eigenvalue}**(i - {index})"

                powered_jordan_cell[row][column] = element

        return powered_jordan_cell

    @staticmethod
    def _combinatorial_number(top: str, bottom: int):
        numerator = "1"
        denominator = "1"

        for index in range(bottom):
            numerator += f" * ({top} - {index})"
            denominator += f" * ({index} + 1)"

        fraction = f"({numerator}) / ({denominator})"
        return fraction

    @staticmethod
    def compose_jordan_matrix(powered_jordan_cells):
        size = 0

        for cell in powered_jordan_cells:
            size += len(cell)

        matrix = [
            ["0" for i in range(size)]
            for j in range(size)
        ]

        start = 0

        for cell in powered_jordan_cells:
            size = len(cell)

            for row in range(size):
                for column in range(size):
                    element = cell[row][column]
                    matrix[start + row][start + column] = element

            start += size

        sympy_matrix = Matrix(matrix)
        return WSMatrix(sympy_matrix=sympy_matrix)

    def inv(self):
        sympy_matrix = self.sympy_matrix.inv()
        return WSMatrix(sympy_matrix=sympy_matrix)

    def __str__(self):
        return str(self.sympy_matrix)

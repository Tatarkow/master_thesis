from copy import deepcopy

import wsummarizer as ws


class Digraph:
    def __init__(self):
        self.nodes = []
        self.edges = []

        self.components = None

    def _get_next_index(self, index):
        for edge in self.edges:
            if edge[0] == index:
                return edge[1]

    def _get_source(self, nodes, edges):
        """
        Returns a node to which no edges leads.
        """

        source_node_indices = {node.index for node in nodes}

        for edge in edges:
            source_node_indices.remove(edge[1])

        if len(source_node_indices) > 0:
            index = source_node_indices.pop()
            return nodes[index]
        else:
            # Cannot happen when applied on a loop diagram.
            return None

    def _remove_node(self, nodes, edges, node_to_be_removed):
        """
        Removes the node and recomputes edges.
        """

        index_to_be_removed = node_to_be_removed.index

        new_nodes = []
        new_edges = []

        for node in nodes:
            if node.index != index_to_be_removed:
                new_nodes.append(node)

        for edge in edges:
            for i in range(2):
                if edge[i] == index_to_be_removed:
                    continue
                elif edge[i] > index_to_be_removed:
                    edge[i] -= 1

            new_edges.append(edge)

    def _get_next_nodes(self, nodes, edges, start_node):
        """
        Returns a set of nodes to which it is possible to
        get directly from the given starting node.
        """

        next_nodes = set()

        for edge in edges:
            if edge[0] == start_node.index:
                next_nodes.add(nodes[edge[1]])

        return next_nodes

    def _get_reachable_nodes(self, node, all_edges):
        """
        Returns a set of nodes to which it is possible to get from
        the given node.
        """

        edges = []
        reachable_nodes = set()

        for edge in all_edges:
            if edge[0] == node:
                edges.append(edge)

        while len(edges) > 0:
            edge = edges.pop()

            if edge[1] not in reachable_nodes:
                reachable_nodes.add(edge[1])

                for next_edge in all_edges:
                    if edge[1] != next_edge[0]:
                        continue

                    if next_edge in edges:
                        continue

                    if next_edge[1] in reachable_nodes:
                        continue

                    edges.append(next_edge)

        return reachable_nodes

    def _get_reachable_indices(self, node_index):
        """
        Returns a set of indices. Each index corresponds to a node
        to which it is possible to get from the node with the given index.
        """

        edges = []
        reachable_indices = set()

        for edge in self.edges:
            if edge[0] == node_index:
                edges.append(edge)

        while len(edges) > 0:
            edge = edges.pop()

            if edge[1] not in reachable_indices:
                reachable_indices.add(edge[1])

                for next_edge in self.edges:
                    if edge[1] != next_edge[0]:
                        continue

                    if next_edge in edges:
                        continue

                    if next_edge[1] in reachable_indices:
                        continue

                    edges.append(next_edge)

        return reachable_indices

    def _get_next_indices(self, start_index):
        """
        Returns a set of indices. Each index represents a node
        to which leads an edge from the node represented by the
        given index.
        """

        next_indices = set()

        for edge in self.edges:
            if edge[0] == start_index:
                next_indices.add(edge[1])

        return next_indices

    def _get_all_paths(self, start_index, terminal_index):
        """
        Returns a list of all possible paths from the node represented
        by the given `start_index` to the node represented by the given
        `terminal_index`. A path is represented as a list of indices.
        May be applied only on a diagram without loops, e.g. a loop diagram.
        """

        if start_index == terminal_index:
            return [[start_index]]

        paths = []
        next_indices = self._get_next_indices(start_index)

        for next_index in next_indices:
            if next_index == terminal_index:
                path = [start_index, terminal_index]
                paths.append(path)
            else:
                next_paths = self._get_all_paths(next_index, terminal_index)

                for next_path in next_paths:
                    path = [start_index] + next_path
                    paths.append(path)

        return paths

    def _get_simple_cycle_path(self, start_index, terminal_index):
        path = [start_index]

        if start_index == terminal_index:
            return path

        for edge in self.edges:
            if edge[0] == start_index:
                next_node = edge[1]

        while next_node != terminal_index:
            for edge in self.edges:
                if edge[0] == next_node:
                    path.append(next_node)
                    next_node = edge[1]

        path.append(next_node)

        return path

    def is_simple_cycle(self):
        """
        Returns True iff the diagram is a simple cycle, i.e. a cycle
        through all the nodes and there are no additional edges (not
        even self-edges). One node with a self-edge is considered to
        be a simple cycle. For instance  A -> B -> A is a simply
        cycle.
        """

        edges = deepcopy(self.edges)

        if len(edges) == 0:
            return False

        if len(self.nodes) == 1 and len(edges) == 1:
            edge = edges[0]

            if edge[0] == 0 and edge[1] == 0:
                return True

        for edge in edges:
            if edge[0] == edge[1]:
                return False

        start_node, next_node = edges.pop()
        used_nodes = {start_node}

        while len(edges) > 1:
            for edge in edges:
                if edge[0] == next_node:
                    used_nodes.add(next_node)
                    next_node = edge[1]
                    edges.remove(edge)
                    break
            else:
                return False

        last_edge = edges[0]

        if last_edge[0] == next_node and last_edge[1] == start_node:
            return True
        else:
            return False

    def is_splitable(self):
        """
        Determines whether there is a subset of nodes such that
        once we leave it, we can never get back to it.
        """

        reachable_indices = {}

        for node_index, node in enumerate(self.nodes):
            reachable_indices[node_index] = self._get_reachable_indices(node_index)

        resolved_indices = set()
        self.components = []

        for start_index in reachable_indices:
            if start_index in resolved_indices:
                continue

            component_indices = {start_index}

            for reachable_index in reachable_indices[start_index]:
                if start_index in reachable_indices[reachable_index]:
                    component_indices.add(reachable_index)

            resolved_indices = resolved_indices.union(component_indices)
            component_nodes = [node for node in self.nodes if node.index in component_indices]
            component = ws.Component(component_nodes)
            self.components.append(component)

        return len(self.components) > 1
import z3

import wsummarizer as ws
from .utils import get_terminal_name, is_solver_satisfiable, is_satisfiable


class Loop:
    def __init__(self, variables, condition, branches):
        self._validate()

        self.variables = variables
        self.condition = self._get_condition(condition)
        self.branches = self._get_branches(branches)

        self.solver = z3.Solver()
        self.iteration_count = z3.Int("n")
        self.terminal_variables = self._get_terminal_variables(variables)

        ws.WSMatrix.prepare(variables)

    def _validate(self):
        # TODO:
        # - unsatisfiable conditions
        # - no branch
        # - conditions are not disjunctive
        # etc.
        pass

    @staticmethod
    def _get_condition(condition):
        or_conditions = [ws.sand(and_condition) for and_condition in condition]
        return ws.sor(*or_conditions)

    @staticmethod
    def _get_branches(branches):
        """
        Simplifies updates of all branches.
        """

        for branch_index, branch in enumerate(branches):
            updates = branch.updates

            for variable_name in updates:
                update = updates[variable_name]
                updates[variable_name] = z3.simplify(update)

            branches[branch_index] = branch

        return branches

    @staticmethod
    def _get_terminal_variables(variables):
        terminal_variables = {}

        for variable_name in variables:
            terminal_name = get_terminal_name(variable_name)
            terminal_variable = z3.Int(terminal_name)
            terminal_variables[terminal_name] = terminal_variable

        return terminal_variables

    def summarize(self):
        option = self._get_option()
        self.solver.add(option)

    def _get_option(self):
        precondition = True
        condition = self.condition
        nodes = self._get_nodes()

        state_diagram = ws.StateDiagram(precondition, condition, nodes, self)
        return state_diagram.get_option()

    def _get_nodes(self):
        nodes = []

        for index, branch in enumerate(self.branches):
            can_be_terminal = self._can_be_terminal(branch, self.condition)
            node = ws.StateNode([branch], can_be_terminal, index, branch.condition)
            nodes.append(node)

        return nodes

    def _can_be_terminal(self, branch, condition):
        substitutions = branch.get_substitutions(self.variables)

        # Condition to begin the second iteration.
        next_condition = ws.ssub(condition, substitutions)

        # Condition that the loop terminates after the first iteration.
        terminal_condition = ws.sand(condition, ws.snot(next_condition))

        # Can the loop terminate after executing this branch once?
        return is_satisfiable(terminal_condition)

    def compute(self, initial_values):
        if not self.solver:
            return

        for variable_name in initial_values:
            variable = self.variables[variable_name]
            value = initial_values[variable_name]
            self.solver.add(variable == value)

        if is_solver_satisfiable(self.solver):
            model = self.solver.model()
            self._print_summarization(model)
        else:
            print("loop will be skipped")

    def _print_summarization(self, model):
        iteration_count = model[self.iteration_count]

        print("=" * 40)
        print(f"Iteration count: {iteration_count}\n")
        print("Summarization:")

        for variable_name, terminal_name in zip(self.variables, self.terminal_variables):
            variable = self.variables[variable_name]
            initial_value = model[variable]

            terminal = self.terminal_variables[terminal_name]
            terminal_value = model[terminal]

            print(f"{variable_name} = {initial_value}\t->\t{terminal_name} = {terminal_value}")

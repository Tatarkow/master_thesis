import z3


def get_variables(*args):
    variables = {}

    for variable in args:
        variables[variable] = z3.Int(variable)

    return variables

def is_satisfiable(condition):
    solver = z3.Solver()
    solver.add(condition)

    return is_solver_satisfiable(solver)


def is_solver_satisfiable(solver):
    result = str(solver.check())

    if result == "sat":
        return True
    elif result == "unsat":
        return False
    else:
        print(result)
        raise Exception


def updates_to_substitutions(variables, updates):
    substitutions = []

    for index, variable_name in enumerate(variables):
        variable = variables[variable_name]
        substitutions.append((variable, updates[index]))

    return substitutions


def get_terminal_name(name):
    return f"{name}'"


def sand(*options):
    return z3.simplify(z3.And(*options))


def sor(*options):
    return z3.simplify(z3.Or(*options))


def ssub(option, *substitutions):
    return z3.simplify(z3.substitute(option, *substitutions))


def snot(option):
    return z3.simplify(z3.Not(option))


class HelperVariableCounter:
    counter = 1
    step = 1

    @classmethod
    def get_counter(cls):
        counter = cls.counter
        cls.counter += 1
        return counter

    @classmethod
    def get_step(cls):
        step = cls.step
        cls.step += 1
        return step


class VariableSorter:
    ids_to_names = {}
    names_to_indices = {}
    indices_to_names = {}

    @classmethod
    def sort(cls, variables):
        sorted_variables = []
        variable_names = list(variables)
        variable_names.sort()

        for index, variable_name in enumerate(variable_names):
            variable = variables[variable_name]
            variable_id = variable.get_id()
            sorted_variables.append(variable)

            cls.ids_to_names[variable_id] = variable_name
            cls.indices_to_names[index] = variable_name
            cls.names_to_indices[variable_name] = index

        return sorted_variables

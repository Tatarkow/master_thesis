class Component:
    """
    Represents node in a loop diagram.
    """

    def __init__(self, component_nodes):
        self.nodes = component_nodes
        self.can_be_terminal = self._can_be_terminal(component_nodes)

    @staticmethod
    def _can_be_terminal(component_nodes):
        for node in component_nodes:
            if node.can_be_terminal:
                return True

        return False

import wsummarizer as ws


class Branch:
    def __init__(self, conditions, updates, name="S"):
        self.condition = ws.sand(*conditions)
        self.updates = updates
        self.name = name

    def __str__(self):
        return self.name

    def get_substitutions(self, variables):
        substitutions = []

        for variable_name in variables:
            variable = variables[variable_name]
            substitution = (variable, self.updates[variable_name])
            substitutions.append(substitution)

        return substitutions

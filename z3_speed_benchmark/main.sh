#!/usr/bin/env bash

BASE=../algorithm/version2

source ${BASE}/env/bin/activate
export PYTHONPATH=${BASE}/wsummarizer
export VERBOSE=0
export ITERATIONS=0

python ./08_chc_comp_0032_extended.py

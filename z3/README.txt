test.txt
	exaple from their website

test2.txt
	the code
		while (x > 0) {
			y = y + 1
			x = x - 2
		}

	`(query loop)`
		tests whether there is some input/output, i.e. whether it terminates for some input

	`(query q1)`
		x = 4
		y = 7

		while (x > 0) {
			y = y + 1
			x = x - 2
		}

		assert x == 0
		assert y == 9

	`(query q2)`
		x = 4
		y = 7

		while (x > 0) {
			y = y + 1
			x = x - 2
		}

		assert x == 0
		assert y == 8
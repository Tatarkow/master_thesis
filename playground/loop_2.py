from math import ceil, log

def loop(x, y):
	while x <= 999:
		if x >= 0 and y >= 1:
			x = 2*x
			y = y - 1
		elif x >= 1 and y <= 0:
			x = x - 1
			y = y - 2
		elif x <= -1 and y >= 0:
			x = x + 1
			y = 3*y
		elif x <= 0 and y <= -1:
			y = y + 1
		elif x == 0 and y == 1:
			y = y + 1

	return x, y

x = 0
y = 5
n = ceil(3*log(10, 2) - log(x, 2))


print(2**n * x >= x and 2**(n-1) * x <= 999 and x >= 0 and y >= n)
print(2**n * x, y - n)
print(loop(x, y))
from math import ceil, sqrt
from time import time

def original(x, y):
	while x > 0:
		if y < 0:
			y = y + 1
		elif y == 0:
			x = x + 2
			y = 3
		else:
			x = x - y
			y = y - 1

	return x, y

def summmarized(x, y):
	if x > 0 and y < 0:
		y = 0

	if x > 0 and y > 0:
		d = (y+0.5)**2 - 2*x
		
		if d < 0.25:
			n = y
		else:
			n = ceil(y + 0.5 - sqrt(d))

		x = 0.5 * n**2 - (y + 0.5)*n + x
		y = y - n

	if (x > 0):
		if (x % 4 == 0):
			x = 0
			y = 0
		elif (x % 4 == 1):
			x = 0
			y = 2
		elif (x % 4 == 2):
			x = -1
			y = 1
		else:
			x = 0
			y = 1

	return int(x), y

values = []
counter = 0
start1 = 1234560
end1 =   1234561
start2 = 1980
end2 =   1981
# start1 = 1234560000000
# end1 =   1234560000001
# start2 = 1980000
# end2 =   1980001
iterations = (end1 - start1)*(end2 - start2)

for x in range(start1, end1):
	for y in range(start2, end2):
		values.append((x, y))

for x, y in values:
	counter += 1

	if counter % 10000 == 0:
		print(f"{counter/iterations} ({counter}/{iterations})")

	t = time()
	o = original(x, y)
	print(time() - t)

	t = time()
	s = summmarized(x, y)
	print(time() - t)

	if o != s:
		print(f"NOK\t({x}, {y}) => {o} | {s}")
		break
	else:
		print(f"OK\t({x}, {y}) => {o} | {s}")

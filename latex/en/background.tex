\chapter{Background}
	\label{chap:background}

	\indent In this chapter we introduce various approaches for loop summarization and briefly discuss couple of possible applications. Then we describe three different tools and algorithms capable of loop summarization. We explain their approaches from high level perspective and discuss their limitations. In the fourth section we present fundamental properties of our summarization technique, discuss its limitations and compare it to the other algorithms. \par

	There are three main approaches for analyzing loops \cite{PDA}. The first one is called loop unwinding. It simply extracts the body of a loop and repeats it appropriately many times. This obviously cannot be used if the number of iterations is unknown. The second approach is to find invariants of a loop \cite{INVARIANTS}, i.e.~variables or properties that are not affected by the loop. Unfortunately, we often need to include all variables into the analysis, and thus this technique cannot be used. In such a case we need to apply the third approach, which is called loop summarization. This method tries to find a relation between the initial values and the terminal values, i.e.~the values after execution of a given loop. There are various types of loop summarizations \cite{PDA, FLATA, LoAT, SUM1, SUM2, SUM3, SUM4, SUM5, FAST}, each with some advantages and disadvantages. \par

	Loop summarization is in general an undecidable problem \cite{INVARIANTS}. Hence, we have decided to simplify the problem by only considering loops operating over integers with multiple branches (i.e.~$\mathbf{if}$ statements). We rely on the existence of an oracle for solving the relation obtained by our summarization technique. In practice, it is possible to use an SMT solver \footnote{Satisfiability modulo theories (SMT) is essentially a decision problem whether a first-order logic formula containing expressions and inequalities is satisfiable.}. For our implementation we have chosen Z3 \cite{z3}, which is a tool developed by Microsoft Research. \par

	Let us discuss a couple of the aforementioned applications. Loop summarization can be used for deciding a reachability problem. For instance, consider an error state following a loop and guarded by an $\mathbf{if}$ statement. Deciding reachability of the error state becomes straight forward after the summarization. Loop summarization can also enable (e.g.~compiler) optimization. For example, summarization could replace instructions adding number one to a certain variable $n$ times by simply adding $n$ once. Summarization algorithm might be used for automatic test generation as well, because using the summarization it is possible to obtain input values leading to certain output values, i.e.~to a certain part of a program that is supposed to be tested. Depending on the form of summarization, it can also be used for deciding termination of a cycle.		

	\section{FLATA}
		\indent FLATA is a state of the art tool developed to demonstrate algorithm for reachability and termination analysis of integer programs \cite{FLATA}. It outperforms couple other modern tools such as FAST \cite{FAST} at several benchmarks (e.g.~\cite{BENCH1} or \cite{BENCH2}). \par

		Their algorithm is based on relations and digraphs. Let us explain it in more detail. They begin by building a digraph, whose vertices correspond to statements (i.e.~lines of code) and whose edges correspond to possible transitions between those statements. Each edge is associated with a relation between its input and output values and possibly with a condition determining whether the given edge is enabled. For instance, Program \ref{program:trivial_program_with_if} would be parsed by FLATA into the digraph pictured at Figure~\ref{fig:flata_digraph}. Notice that the edge on the left contains not only the branch guard (i.e.~$x \ge 1$), but also relation between the input (i.e.~$x$ and $y$) and the output values (i.e.~$x'$ and $y'$). Note that the digraph does not contain redundant lines, i.e.~lines 3 and 5. \par

		\begin{lstlisting}[float,label={program:trivial_program_with_if},caption={Trivial program with an $\mathbf{if}$ statement.}]
if (x >= 1) {
	y = y - 2;
} else {
	y = y + 3;
}
		\end{lstlisting}

		\begin{figure}
			\centering
			   \begin{tikzpicture}[thick, dot/.style={circle,draw,outer sep=2pt,inner sep=0pt,minimum size=1.5mm,fill}]
					\node [dot, label=above:line 1] (0) at (0,1) {};
					\node [dot, label=below:line 2] (1) at (-2,0) {};
					\node [dot, label=below:line 4] (2) at (2,0) {};

					\draw[->] (0) -- (1);
					\draw[->] (0) -- (2);
					
					\draw (0) -- (1) node [midway, above left, fill=white] {$x \ge 1 \land x' = x \land y' = y - 2$};
					\draw (0) -- (2) node [midway, fill=white] {$x \le 0 \land x' = x \land y' = y + 3$};
				\end{tikzpicture}
			\caption{Digraph associated with Program \ref{program:trivial_program_with_if}.}
			\label{fig:flata_digraph}
		\end{figure}

		The algorithm proceeds by eliminating vertices without self-loops. See Figure~\ref{fig:flata_elimination_1} for an example. Edges leading to a removed vertex ($c$ in Figure~\ref{fig:flata_elimination_1}) are combined with the outgoing edges using relation composition. Note that this may result in a multidigraph. The next step is elimination of vertices with self-edges. This is visualized in Figure~\ref{fig:flata_elimination_2}. Since the instructions associated with a self-edge may be executed repeatedly, the algorithm computes a transitive closure of the relation associated with the self-loop. More precisely, if there is a vertex with a single self-edge associated with relation $R$, they compute a transitive closure $R^{+} = \bigcup^\infty_{i=1} R^i$, where $R^{i+1} = R^i \circ R$. The situation is more complicated when there are multiple self-edges. In this case, it is necessary to cover all possible interleavings. For instance, assume that there are two self-edges respectively associated with relations $R$ and $S$. Then the algorithm has to compute not only relations $R^{+}$ and $S^{+}$, but also $R^{+} \circ S^{+}$, $S^{+} \circ R^{+} \circ S^{+}$ and so on. \par

		\begin{figure}
			\centering
			\subcaptionbox{Before.}[.4\linewidth]{
			  	\begin{subfigure}[]{0.4\textwidth}
			  		\centering
					\begin{tikzpicture}[thick, dot/.style={circle,draw,outer sep=2pt,inner sep=0pt,minimum size=1.5mm,fill}]
						\node [dot, label=above:a] (a) at (-2,1) {};
						\node [dot, label=below:b] (b) at (-2,-1) {};
						\node [dot, label=above:c] (c) at (0,0) {};
						\node [dot, label=above:d] (d) at (2,1) {};
						\node [dot, label=below:e] (e) at (2,-1) {};

						\draw[->] (a) -- (c) node [midway, above] {$R_a$};
						\draw[->] (b) -- (c) node [midway, below] {$R_c$};
						\draw[->] (c) -- (d) node [midway, above] {$R_d$};
						\draw[->] (c) -- (e) node [midway, below] {$R_e$};
					\end{tikzpicture}
				\end{subfigure}
			}
			\quad
			\subcaptionbox{After.}[.4\linewidth]{
			  	\begin{subfigure}[]{0.4\textwidth}
					\centering
					\begin{tikzpicture}[thick, dot/.style={circle,draw,outer sep=2pt,inner sep=0pt,minimum size=1.5mm,fill}]
						\node [dot, label=above:a] (a) at (-2,1) {};
						\node [dot, label=below:b] (b) at (-2,-1) {};
						\node [dot, label=above:d] (d) at (2,1) {};
						\node [dot, label=below:e] (e) at (2,-1) {};

						\draw[->] (a) -- (d) node [midway, above] {$R_d \circ R_a$};
						\draw[->] (a) -- (e) node [midway, above left, shift={(-1.25, 0.2)}] {$R_e \circ R_a$};
						\draw[->] (b) -- (d) node [midway, below left, shift={(-1.25,-0.2)}] {$R_d \circ R_b$};
						\draw[->] (b) -- (e) node [midway, below ] {$R_e \circ R_b$};
					\end{tikzpicture}
				\end{subfigure}
			}
			\caption{Elimination of vertices without self-loops.}
			\label{fig:flata_elimination_1}
		\end{figure}

		\begin{figure}
			\centering
			\subcaptionbox{Before.}[.4\linewidth]{
			  	\begin{subfigure}[]{0.4\textwidth}
			  		\centering
					\begin{tikzpicture}[thick, dot/.style={circle,draw,outer sep=2pt,inner sep=0pt,minimum size=1.5mm,fill}]
						\node [dot, label=above:a] (a) at (-2,0) {};
						\node [dot] (b) at (0,0) {};
						\node [dot, label=above:b] (c) at (2,0) {};

						\draw[->] (a) -- (b) node [midway, above] {};
						\draw[->] (b) -- (c) node [midway, below] {};

						\path[->] (b) edge [in=45,out=135,loop] node {$R$} (b);
						\path[->] (b) edge [in=-45,out=-135,loop, below] node {$S$} (b);
					\end{tikzpicture}
				\end{subfigure}
			}
			\quad
			\subcaptionbox{After.}[.4\linewidth]{
			  	\begin{subfigure}[]{0.4\textwidth}
					\centering
					\begin{tikzpicture}[thick, dot/.style={circle,draw,outer sep=2pt,inner sep=0pt,minimum size=1.5mm,fill}]
						\node [dot, label=above:a] (a) at (-2,0) {};
						\node [dot] (b1) at (-1,0) {};
						\node [dot] (b2) at (1,0) {};
						\node [dot, label=above:b] (c) at (2,0) {};

						\draw[->] (a) -- (b1) node [midway, above] {};
						\draw[->] (b2) -- (c) node [midway, below] {};

						\path[->] (b1) edge [bend left=90, above] node {$R^{+}$} (b2);
						\path[->] (b1) edge [bend left=15, above] node {$S^{+}$} (b2);
						\path[->] (b1) edge [bend left=-15, below] node {$R^{+} \circ S^{+}$} (b2);
						\path[->] (b1) edge [bend left=-90,draw=none] node [auto=false, shift={(0, -0.3)}] {$\vdots$} (b2);
					\end{tikzpicture}
				\end{subfigure}
			}
			\caption{Elimination of self-loops.}
			\label{fig:flata_elimination_2}
		\end{figure}

		In order for the algorithm to terminate, they check whether a newly composed relation is consistent (i.e.~if it is non-empty) and whether it is not equivalent to a relation examined earlier. Notice that the algorithm is still not guaranteed to terminate, because the number of interleavings may be infinite. Assume that the algorithm finishes and that the vertex with self-loop(s) is named $a$. Then vertex $a$ is split into vertices $b$ and $c$. Vertex $b$ has the same incoming edges as vertex $a$ and vertex $c$ has identical outgoing edges as vertex $a$ (excluding the self-edges). For each previously computed relation composition (e.g.~$R^{+} \circ S^{+}$) they introduce an edge from vertex $b$ to vertex $c$. This procedure gradually summarizes the whole program including its loops. \par

		Let us now discuss limitations of this approach. As we have already mentioned, the algorithm may not terminate due to infinite number of possible interleavings. The second major limitation is computation of the transitive closure. In general, transitive closure does not have to be expressible in decidable first-order arithmetic \cite{FLATA}. Therefore, they limit themselves to integer relations whose transitive closure is definable in Presburger arithmetic, which is decidable. There are three main classes of such relations, namely difference bounds relations \cite{PRESBURGER_1_1, PRESBURGER_1_2}, octagonal relations \cite{PRESBURGER_2_1} and finite monoid affine relations \cite{PRESBURGER_3_1, PRESBURGER_3_2}. For instance, an assignment as simple as $x = 2x$ fits into neither of these three categories and its transitive closure is outside Presburger arithmetic. Therefore, programs (or just loops) containing this assignment cannot be summarized by FLATA. \par

	\section{Path dependency analysis}
		\indent Path dependency analysis \cite{PDA} is an advanced technique for summarizing relatively complex integer programs. It begins by transforming a program into a so-called control flow graph. It is a digraph whose vertices correspond to either assignments or logic switches and whose edges correspond to conditions under which the following vertex can be reached. For instance, consider the $\mathbf{while}$ cycle from Program \ref{program:trivial_program_with_while}. The algorithm would create the control flow graph visualized in Figure~\ref{figure:cfg}. Beside the nodes corresponding to lines of code (i.e.~nodes $L_1$, $L_2$, $L_3$ and $L_5$), there is an initial node $I$ and a terminal node $T$. The initial node can be used for restricting the initial values. \par

		\begin{lstlisting}[float,label={program:trivial_program_with_while},caption={Trivial program with a $\mathbf{while}$ statement.}]
while (x >= 1)
	if (y >= 1) {
		x = x - 1;
	} else {
		x = x - 2;
	}
}
		\end{lstlisting}

		\begin{figure}
			\centering
			\subcaptionbox{Control flow graph. \label{figure:cfg}}[.52\linewidth]{
			  	\begin{subfigure}[]{0.52\textwidth}
			  		\centering
					\begin{tikzpicture}
						\node[draw,initial,minimum width=1.2cm,minimum height=0.8cm,label=above:$I$] (rect) at (0, 0) (s) {};
						\node[draw,minimum width=1.2cm,minimum height=0.8cm,label=above left:$L_1$] (rect) at (0,-1.5) (l1) {};
						\node[draw,accepting,minimum width=1.2cm,minimum height=0.8cm,label=above:$T$] (rect) at (2.5,-1.5) (e) {};
						\node[draw,minimum width=1.2cm,minimum height=0.8cm,label=below:$L_2$] (rect) at (-2.5,-3) (l2) {};
						\node[draw,minimum width=1.2cm,minimum height=0.8cm,label=above:$L_3$] (rect) at (-2.5,-1.5) (l3) {$x = x - 1$};
						\node[draw,minimum width=1.2cm,minimum height=0.8cm,label=below:$L_5$] (rect) at (2.1,-3) (l5) {$x = x - 2$};

						\path[->] 
							(s)
								edge node {} (l1)
							(l1)
								edge node [midway, right] {$x \ge 1$} (l2)
								edge node [midway, above] {$x \le 0$} (e)
							(l2)
								edge node [midway, left] {$y \ge 1$} (l3)
								edge node [midway, below] {$y \le 0$} (l5)
							(l3)
								edge node {} (l1)
							(l5)
								edge node {} (l1);

					\end{tikzpicture}
				\end{subfigure}
			}
			\quad
			\subcaptionbox{Path dependency analysis. \label{figure:pda}}[.38\linewidth]{
			  	\begin{subfigure}[]{0.38\textwidth}
					\centering
					\begin{tikzpicture} 
						\node[state,initial] at (0, 0) (r1) {$\rho_1$};
						\node[state,accepting] at (2, 0) (r2) {$\rho_2$};
						\node[state] at (0, -2) (r3) {$\rho_3$};
						\node[state] at (2, -2) (r4) {$\rho_4$};
					 	
					 	\path[->] 
							(r1)
								edge node {} (r2)
								edge node {} (r3)
								edge node {} (r4)
							(r3)
								edge [bend left=15] node {} (r4)
								edge node {} (r2)
							(r4)
								edge [bend left=15] node {} (r3)
								edge node {} (r2);

					\end{tikzpicture}
				\end{subfigure}
			}
			\caption{Structures associated with Program \ref{program:trivial_program_with_while}.}
		\end{figure}

		The algorithm proceeds by identifying paths in the control flow graph. In our example, it would yield paths $\rho_1 \equiv I \rightarrow L_1$, $\rho_2 \equiv L_1 \rightarrow T$, $\rho_3 \equiv L_1 \rightarrow L_2 \rightarrow L_3 \rightarrow L_1$ and $\rho_4 \equiv L_1 \rightarrow L_2 \rightarrow L_5 \rightarrow L_1$. From the control flow graph and paths, they create a so-called path dependency analysis, which is a digraph, whose vertices are paths and whose edges are feasible transitions between the paths. Example of a path dependency analysis obtained from the control flow graph from Figure~\ref{figure:cfg} is shown in Figure~\ref{figure:pda}. Each node may represent repeated execution of the associated path. For instance, path $\rho_3$ can be executed multiple times in row. Notice the node associated with path $\rho_1$ is marked as initial. This is because path $\rho_1$ contains node $I$ of the control flow graph. Similarly, the node related with path $\rho_2$ is labeled as terminal, because it contains node $T$. \par

		The next step is to find all possible traces, i.e.~paths from the initial node to the terminal node of a path dependency analysis. Note that paths in a trace may repeat arbitrary number of times. For instance, in our example a valid trace is $\rho_1 \rho_3 \rho_3 \rho_3 \rho_4 \rho_3 \rho_4  \rho_2$. Similarly to FLATA, summarization of a trace is a relation between the input and the output values conjuncted with conditions guaranteeing that the trace may be actually executed. The basic idea is to summarize the whole program as a disjunction of summarizations of all traces. \par

		The difficulty of obtaining a trace summarization depends on a path dependency analysis. In general, summarization of loops is the most troublesome part of the entire summarization process. The article distinguishes four types of loops. The type depends on complexity of loop conditions and on path interleaving patterns in a path dependency analysis. They only provide an algorithm for summarizing loops of the simplest type, i.e.~loops with relatively simple conditions and traces with no or regular cycles in a path dependency analysis. Although, this is the simplest type of loops, the summarization is still very challenging and rather technical. Instead of providing a proper algorithm for the other three types, they show how it is possible to approximate it. Approximated summarization is not suitable for bug finding, however it is still useful for proving safety or termination analysis \cite{PDA}. \par

		Let us summarize their approach. They begin by transforming a program into a diagram. Based on the structure and expressions associated with its nodes and edges, they determine type. For the simplest type they compute precise summarization, while for the other types they only present approximation techniques. There is also an additional major assumption that the input program always terminates. \par

	\section{Loop acceleration}
		\label{section:loop_acceleration}

		\indent Loop acceleration \cite{LoAT} significantly differs from FLATA and path dependency analysis. It is a summarization technique as well; however, it can only be used on loops operating over integers without any branching. This implies that unlike the previous two techniques, it cannot be used on entire programs. Furthermore, it could not be even used on Program \ref{program:trivial_program_with_while}, because of the $\mathbf{if}$ statement. \par

		Because they do not consider branching, they do not need to work with diagrams and the input loop can be represented as $\textbf{while } \varphi \textbf{ do } x \leftarrow a$, where $\varphi$ is a condition, $x$ is a vector of variables and $a$ determines how the variables are updated in each iteration of the cycle. Example of such a loop is
		\begin{equation}
			\label{loop_acceleration_loop}
			\textbf{while } x_1 > 0 \land x_2 > 0 \textbf{ do }
			\begin{pmatrix}
				x_1\\
				x_2\\
			\end{pmatrix}
			\leftarrow
			\begin{pmatrix}
				x_1 - 1\\
				x_2 + 1\\
			\end{pmatrix}
		\end{equation} \par

		Similarly to the previous techniques, the result of loop acceleration is a formula including relation between the initial and the final values. However, unlike the summarizations above, loop acceleration includes iteration variable, which may be used to obtain not only the terminal variables (i.e.~the values after the loop terminates), but also values after arbitrary iteration of the loop. In fact, loop summarization does not aim to determine the terminal values. For instance, by accelerating loop from Equation~\ref{loop_acceleration_loop}, we obtain
		\begin{equation}
			\label{loop_acceleration_formula}
			\begin{pmatrix}
				x_1'\\
				x_2'\\
			\end{pmatrix}
			=
			\begin{pmatrix}
				x_1 - n\\
				x_2 + n\\
			\end{pmatrix}
			\land
			x_2 > 0 \land x_1 - n + 1 > 0
		\end{equation}
		where $x_1$ and $x_2$ are the initial values, $x_1'$ and $x_2'$ are the final values and $n$ is the iteration counter. For instance, let us assume that $x_1 = 2$ and $x_2 = 1$. If we are interested in the values after the second iteration, we obtain $x_1' = x_1 - n = 2 - 2 = 0$ and $x_2' = x_2 + n = 1 + 2 = 3$. However, we cannot determine values after the third iteration, because $x_1 - n + 1 = 2 - 3 + 1 = 0$, and thus $x_1 - n + 1 > 0$ (which is part of Equation~\ref{loop_acceleration_formula}) does not hold. This is natural, because the loop represented by Equation~\ref{loop_acceleration_loop} would with such initial values terminate after the second iteration. \par

		The article presents several theorems that describe which summarization techniques may be applied on which types of loops. For instance, one of the most trivial theorems states that if $\varphi(a(x)) \implies \varphi(x)$, then the summarization is $x' = a^n(x) \land \varphi(a^{n-1}(x))$. This is easy to see, since $\varphi(a^{n-1}(x))$ together with $\varphi(a(x)) \implies \varphi(x)$ implies that the loop can iterate at least $n$ times (after which it may terminate). There are other theorems whose assumptions and summarizations are obviously more complicated. One of the main outcomes of the article is modularity of this approach, which enables combining the presented techniques. In other words, it is possible to first simplify the problem by applying one technique and then proceed with other techniques. \par

		Let us conclude this technique. The most apparent and significant limit of their approach is that loop acceleration method can be applied only on loops without branching. On the other hand their approach is able to summarize quite complex assignments. More precisely, in order for the algorithm to work, variable $a$ from $\textbf{while } \varphi \textbf{ do } x \leftarrow a$ must be of form
		\begin{equation*}
			a = 
			\begin{pmatrix}
				c_1 \cdot x_1 + p_1 \\
				\vdots \\
				c_m \cdot x_m + p_m \\
			\end{pmatrix}
		\end{equation*}
		where $c_i \in \mathbb{N}$ and each $p_i$ is a polynomial over variables $x_1, \dots, x_{i-1}$. Furthermore, using loop acceleration it is possible to obtain values at arbitrary point of time before termination. \par

	\section{Our approach}
		\indent As all the other listed techniques, we only consider programs operating over integers. As the title of this thesis suggests we aim to summarize loops with multiple branches. Therefore, our approach is more similar to FLATA and path dependency analysis than to loop acceleration. Unlike the two techniques, our goal is only summarization of a single loop, not an entire program containing a loop (or even multiple loops). Furthermore, a loop which can be summarized by our algorithm may only contain branches with assignments. That being said, our approach cannot be directly applied on nested loops. The assignments have to be relatively (e.g.~with respect to loop acceleration) simple. Formally, it will be defined in Section~\ref{section:fundamentals}; however, basically the new values can be obtained only by summing variables multiplied by constants and an additional constant. For instance, assignment $x = y * z$ cannot be handled by our technique. \par

		Our approach is generally closer to path dependency analysis than to FLATA. Instead of developing an algorithm, which would be able to summarize loops containing complicated conditions, we focus on loops with complex branch interleavings. In other words, our algorithm is able to summarize (some) loops belonging to two types of loops discussed in \cite{PDA}. \par

		Let us consider a loop which satisfies our criteria. Even in this case there are still two issues which may prevent our algorithm from successfully finishing the summarization. First, in the case of complex branch interleavings, the algorithm might never finish due to infinite recursion. The second cause of failure might be the lack of monotonicity (see Section~\ref{section:sum_functions}). Unlike in the first case, the lack of monotonicity can be recognized. In such a case the execution is immediately stopped as unsuccessful. Note that this monotonicity requirement is similar to condition $\varphi(a(x)) \implies \varphi(x)$ mentioned in Section~\ref{section:loop_acceleration}. \par

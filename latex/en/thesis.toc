\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}%
\contentsline {chapter}{\numberline {2}Background}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}FLATA}{3}{section.2.1}%
\contentsline {section}{\numberline {2.2}Path dependency analysis}{5}{section.2.2}%
\contentsline {section}{\numberline {2.3}Loop acceleration}{7}{section.2.3}%
\contentsline {section}{\numberline {2.4}Our approach}{8}{section.2.4}%
\contentsline {chapter}{\numberline {3}Analysis}{9}{chapter.3}%
\contentsline {section}{\numberline {3.1}Fundamentals}{9}{section.3.1}%
\contentsline {section}{\numberline {3.2}Simple loop}{13}{section.3.2}%
\contentsline {section}{\numberline {3.3}Complex loop}{17}{section.3.3}%
\contentsline {section}{\numberline {3.4}Splittable loop}{20}{section.3.4}%
\contentsline {chapter}{\numberline {4}Algorithm}{28}{chapter.4}%
\contentsline {section}{\numberline {4.1}Algorithm core}{28}{section.4.1}%
\contentsline {section}{\numberline {4.2}Digraph algorithms}{29}{section.4.2}%
\contentsline {section}{\numberline {4.3}Simplifying function}{31}{section.4.3}%
\contentsline {section}{\numberline {4.4}Summarization functions}{35}{section.4.4}%
\contentsline {section}{\numberline {4.5}Implementation}{45}{section.4.5}%
\contentsline {subsection}{\numberline {4.5.1}Overview}{45}{subsection.4.5.1}%
\contentsline {subsection}{\numberline {4.5.2}Structure description}{47}{subsection.4.5.2}%
\contentsline {chapter}{\numberline {5}Evaluation}{49}{chapter.5}%
\contentsline {chapter}{\numberline {6}Conclusion}{53}{chapter.6}%
\contentsline {chapter}{Bibliography}{54}{chapter*.39}%
\contentsline {chapter}{\numberline {A}Attachments}{56}{appendix.A}%
\contentsline {section}{\numberline {A.1}Technical details for \nameref {section:loop_2}}{56}{section.A.1}%
\contentsline {section}{\numberline {A.2}Technical details for \nameref {section:loop_3}}{57}{section.A.2}%
\contentsline {section}{\numberline {A.3}Benchmarks used in \nameref {chap:evaluation}}{61}{section.A.3}%
\contentsline {section}{\numberline {A.4}User documentation}{62}{section.A.4}%

\chapter{Evaluation}
	\label{chap:evaluation}

	\indent In this chapter we compare the implementation of our algorithm with similar tools, namely Flata \cite{FLATA}, Loop Acceleration Tool (LoAT) \cite{LoAT} and Z3 \cite{z3}. Proteus, i.e.~the implementation of path dependency analysis mentioned in Chapter~\ref{chap:background}, is missing, since to our best knowledge the tool has not been published (yet). \par

	We compare summarization of the loops discussed in Chapter~\ref{chapter:analysis}. Using the name convention from Chapter~\ref{chapter:analysis}, Table~\ref{table:tool_comparison} refers to the simple loop, the complex loop and the splittable loop using IDs 1, 2 and 3, respectively. The remaining seven loops are defined in Section~\ref{section:appendix_comparison} and based on benchmark CHC\_COMP \footnote{\url{https://github.com/chc-comp/chc-comp19-benchmarks/tree/master/lra-ts}}. Columns of the table correspond to individual tools. Implementation of our algorithm is named WSummarizer. ``Y'' means that the tool is capable of summarizing the loop and ``N'' means the opposite. In case of LoAT, we also use ``-'' to label loops with branches, i.e.~loops on which the tool cannot be used by design. \par

	\begin{table}[H]
		\small
		\begin{center}
			\begin{tabular}{|c|c|c|c|c|}
				\hline
				Loop ID & WSummarizer & LoAT & FLATA & Z3 \\
				\hline
				\hline
				1 & Y & - & N & Y \\
				\hline
				2 & Y & - & N & Y \\
				\hline
				3 & Y & - & N & Y \\
				\hline
				4 & Y & Y & N & Y \\
				\hline
				5 & Y & Y & Y & Y \\
				\hline
				6 & Y & - & Y & Y \\
				\hline
				7 & N & Y & N & Y \\
				\hline
				8 & Y & Y & Y & Y \\
				\hline
				9 & Y & - & N & Y \\
				\hline
				10 & Y & - & N & Y \\
				\hline

			\end{tabular}
			\caption{Comparison of various summarization tools.}
			\label{table:tool_comparison}
		\end{center}
	\end{table}

	\subsection*{WSummarizer}
		\indent As mentioned in Chapter~\ref{chap:background}, there are two possible causes of failure of our algorithm. Loop 7, i.e.~Program \ref{program:benchmark_07}, does not satisfy the monotonicity property for the simple cycle that consists of the only branch present in the loop (and a self-edge). For instance, let us assume that in $i$th iteration of the full cycle (which can be in this case identified with $i$th iteration of the loop) the values are $x^i = 0$, $y^i = -1$, $z^i = 0$ and $w^i = 1$. Then in the next iteration the values would be $x^{i+1} = 1$, $y^{i+1} = -1$, $z^{i+1} = 0$ and $w^{i+1} = 0$. Hence, the loop condition is satisfied in $(i+1)$th iteration, but not in $i$th iteration. This is violation of the monotonicity property. \par

		From these ten examples, Z3 (used as a part of WSummarizer) struggles with solving summarization produced by our algorithm only in case of loop 2. However, using the guessing method mentioned in Section~\ref{section:implementation}, it is possible to obtain the terminal values based on the initial values. Nevertheless, the loop can be summarized. During summarization of loop 2 the algorithm needs (due to the same cause) to use the alternative monotonicity condition mentioned in Section~\ref{section:implementation}. Summarization of the remaining nine loops can be done with the original monotonicity condition. Note that the alternative monotonicity condition would not be sufficient for loops 1, 4 and 6. In other words, the algorithm would falsely mark these three loops as not summarizable. \par

	\subsection*{LoAT}
		\indent The algorithm on which LoAT is based can only be applied on loops with a single branch. Therefore, using this tool we can only try to summarize loops 4, 5, 7 and 8. Each of these four loops can be successfully summarized by LoAT. \par

	\subsection*{FLATA}
		\indent FLATA consists of couple of tools, one of which can solve reachability problems. In order to decide reachability of an error state after a loop, it internally summarizes the loop. That being said, its output is a boolean representing the reachability, not a loop summarization. If the loop cannot be summarized, FLATA simply prints: ``Analysis was not successful.'' Input is encoded as a transition system, where each transition may be guarded by a condition. Converting a standard $\mathbf{while}$ cycle to this format is quite straightforward. Given a loop we can define a transition system in the following way. We begin by creating a state in which we set the initial values. The only transition starting at that state leads to the enter state of the transition system representing the loop. If the loop condition is violated, we enable a transition leading to a state where we check the terminal values. If they differ, we proceed to an error state. Notice that the error state is unreachable, but it cannot be decided without summarizing the loop. In conclusion, using this trick we are able to check whether FLATA is capable of summarizing a given loop. \par

		As we can see in Table~\ref{table:tool_comparison}, FLATA successfully summarized only three (out of ten) loops, namely 5, 6 and 8. The cause of failure of the analysis of loops 1, 2 and 3 is (according to the log) ``non-octagonal loop''. For instance, loop 1 contains update matrix
		\begin{equation*}
			U = 
			\begin{pmatrix}
				1 & 0 \\
				1 & 1 \\
			\end{pmatrix}
		\end{equation*}
		for which the set $\{U^n; n \in \mathbb{N}\}$ is infinite, and thus the associated relation is not supported by FLATA (see Chapter~\ref{chap:background}). Loops 9 and 10 failed because of reaching maximal ``tree depth'', which most likely refers to the tree of all possible combinations of transitive closures mentioned in Chapter~\ref{chap:background}. Note that the analysis of these two loops took significantly longer compared to the other loops. To be more specific, computation of each of loops 9 and 10 took about ten seconds, while other results were printed virtually instantly. FLATA did not state the cause of failure of loops 4 and 7. \par

	\subsection*{Z3}
		\indent Let us finally discuss Z3 as a standalone tool, not just as a module of our algorithm. Instead of summarizing loop explicitly, it reasons about satisfiability of given formulae. Using recursion we can encode loops to formulae supported by Z3. Let us describe this in more detail. We begin by defining a relation between values in $i$th and $(i+k)$th iteration of the loop. This can be done by initializing it as the identity relation and then recursively specify how the values should be added to the relation. For instance, we can initialize relation $R$ representing a loop operating over a single variable $x$ as the set $\{(x, x'); x = x'\}$ and specify that if $(x, x') \in R \land x'' = x' + 3 \land x' < 9$, then $(x, x'') \in R$. This would represent the values computed during the execution of cycle $\mathbf{while}\ (x < 9)\ \{x = x + 3\}$. Using this relation, we can proceed by defining a relation between the input and the terminal values by simply adding the negation of the loop condition. For instance, we can define relation $S$ as $\{(x, x'); (x, x') \in R \land x' \ge 10 \}$. Finally, we can check whether specific input and output values are in relation $S$. Z3 obviously cannot answer this question without analyzing the relation representing the loop. This is in some sense equivalent to loop summarization. \par

		\begin{figure}
		    \begin{tikzpicture}
		        \begin{axis}[
		        	width=0.90\textwidth,
		        	height=8cm,
		        	xlabel=Iterations,
	  				ylabel={Seconds},
	  				xmin=-1,
	  				xmax=101,
	  				legend pos=north west,
	  				legend cell align={left}
		        ]

		            \addplot [
		                mark=*,
		                only marks,
		                mark size=1pt,
		                error bars/.cd,
		                error bar style={line width=0.3pt, gray},
	                    y dir=both,
	                    y explicit,
	                    error mark options={
					      mark size=0pt,
					      line width=0pt
					    },
		            ]
		                table [
		                    x=x,
		                    y=avg,
		                    y error minus expr=\thisrow{avg}-\thisrow{min},
		                    y error plus expr=\thisrow{max}-\thisrow{avg},
		                    col sep=comma,
		                ] {speed_experiment_z3.csv};

		            \addplot [
		                mark=o,
		                only marks,
		                mark size=1pt,
		                error bars/.cd,
		                error bar style={line width=0.3pt, gray},
	                    y dir=both,
	                    y explicit,
	                    error mark options={
					      mark size=0pt,
					      line width=0pt
					    },
		            ]
		                table [
		                    x=x,
		                    y=avg,
		                    y error minus expr=\thisrow{avg}-\thisrow{min},
		                    y error plus expr=\thisrow{max}-\thisrow{avg},
		                    col sep=comma,
		                ] {speed_experiment_ws.csv};

		            \addlegendentry{Z3}
					\addlegendentry{WSummarizer}
		        \end{axis}
		    \end{tikzpicture}
		    \caption{Summarization speed.}
		    \label{plot:z3}
		\end{figure}

		As we can see in Table~\ref{table:tool_comparison}, Z3 is able to successfully analyze all loops in our benchmark set. However, this does not mean that it is generally better than the other tools. For instance, our tool significantly outperforms Z3 in terms of speed\footnote{All experiments have been done using a laptop with Ubuntu 20.04 and processor 8250U made by Intel.}. To demonstrate this we summarized loop 8. More precisely, we analyzed it using Z3 with values $x^0=1000$, $y^0=1$ and $z^0 \in \{0, \dots, 100\}$ (and the appropriate terminal values). We did an analogous experiment using our algorithm, i.e.~we summarized the same loop and then decided the satisfiability of the summarization extended by formula $x = x^0 \land y = y^0 \land z = z^0$, where $x^0=1000$, $y^0=1$ and $z^0 \in \{0, \dots, 100\}$. In other words, we did 101 summarizations (each time with different initial values) for each tool. In fact, each experiment was executed three times. Marks in Figure \ref{plot:z3} represent average values. Notice that in this case the number of loop iterations is equal to $z^0$ (see Program \ref{program:benchmark_08}). The figure clearly shows that speed of our algorithm does not depend on the number of iterations needed for the original loop to terminate. On the other hand, it also shows that Z3 needs significantly more time to finish when the loop would iterate more times. \par

		We also ran the experiment for $z^0 \in \{110, 120, \dots, 200\}$. When the loop needed 200 iterations, Z3 took in average over 32 minutes, while our algorithm still finished well under 1 second. Note that we excluded this from the plot to highlight the difference with smaller number of iterations. \par

		The cause of this speed disparity is a completely different approach. Our algorithm first summarizes a given loop independently of the initial values. In the second step the initial values are assigned to variables (included in the summarization) as constants. Note that despite the fact that the summarization only needs to be done once (i.e.~not for each combination of initial values), we included the time for the summarization itself to the results of our algorithm. The summarization took 0.262 seconds in average, with is the majority of the computation time. In contrast, Z3 tries to explore the definition of the relation in such depth that it would be able to decide the query. Because the depth depends on the number of iterations of the loop (i.e.~on the initial values), the needed time varies. \par
\chapter{Example 28}
			\label{example_28}
			\todo{move to different chapter and possibly rewrite}
			\section{State analysis}
				\paragraph{}
					In this section we will manually summarize the loop in Program \ref{loop_28} and try to derive a general algorithm for loop summarization. Note that FLATA fails to analyze this one because of the same reason as the previous one, i.e. because it is a non-octagonal loop.

					\begin{lstlisting}[float,label={loop_28},caption={Original loop of \nameref{example_28}}]
while (x > 0) {
	if (y < 0) {
		// State A.
		y = y + 1;
	}
	else if (y == 0) {
		// State B.
		x = x + 2;
		y = 3;
	}
	else {
		// State C.
		x = x - y;
		y = y - 1;
	}
}
					\end{lstlisting}

				\paragraph{}
					If $x_0 \le 0$, then the loop can be completely omitted. Let us then assume $x_0 > 0$. Let us denote the first branch as state $\mathbf{A}$, the second branch as $\mathbf{B}$ and the third one as $\mathbf{C}$. In which state we start depends purely on $y_0$. Notice that each of states $\mathbf{A}$, $\mathbf{B}$ and $\mathbf{C}$ may be a starting state. However, since $x$ is decreased neither in $\mathbf{A}$ nor in $\mathbf{B}$, only state $\mathbf{C}$ is terminal (note that in the context of $\mathbf{C}$, $x = x - y$ implies decreasing the value of $x$). The state transitions are visualized in Figure \ref{loop_28_states}.

					\begin{figure}
						\centering
						\begin{tikzpicture} 

						   \node[state,initial] (a)   {$\mathbf{A}$}; 
						   \node[state,initial] (b) [below left=of a] {$\mathbf{B}$}; 
						   \node[state,initial right,accepting] (c) [below right=of a] {$\mathbf{C}$};
						   
						   \path[->] 
							(a) edge [loop above] node {} (a)
								edge node {} (b)
							(b) edge [bend left=15] node {} (c)
							(c) edge [bend left=15] node {} (b)
								edge [loop below] node {} (c);
						\end{tikzpicture}
						\caption{Automaton representing state transitions of Program \ref{loop_28}}
						\label{loop_28_states}
					\end{figure}

				\paragraph{}
					Because we cannot get back to state $\mathbf{A}$, we can extract state $\mathbf{A}$ from the cycle completely and obtain Program \ref{loop_28_1}, which is clearly equivalent to Program \ref{loop_28}. Note that unlike in the previous example, we split the initial loop into two loops, not into an if-statement and a loop. However, we can analyze the cycles virtually independently (we will use input constrains produced by the previous derived loops). 

				\paragraph{}
					To distinguish states of the original Program \ref{loop_28} and the derived loops, we will use subscripts. For instance state $\mathbf{B_2}$ will be a state of the second derived loop, which corresponds to state $\mathbf{B}$ of the original loop from Program \ref{loop_28}. Although, there is a straightforward bijection between states of Program \ref{loop_28} and of Program \ref{loop_28_1}, we will create an automaton representing state transitions of Program \ref{loop_28_1} (see Figure \ref{loop_28_states3}).

					\begin{lstlisting}[float,label={loop_28_1},caption={Equivalent program to Program \ref{loop_28}.}]
// First derived loop.
while (x > 0 && y < 0) {
	// State A_1.
	y = y + 1;
}

// Second derived loop.
while (x > 0) {
	if (y == 0) {
		// State B_2.
		x = x + 2;
		y = 3;
	}
	else {
		// State C_2.
		x = x - y;
		y = y - 1;
	}
}
						\end{lstlisting}

					\begin{figure}
						\centering
						\begin{tikzpicture} 

						   \node[state,initial] (a_1)   {$\mathbf{A_1}$}; 
						   \node[state,initial] (b_2) [below left=of a_1] {$\mathbf{B_2}$}; 
						   \node[state,initial right,accepting] (c_2) [below right=of a_1] {$\mathbf{C_2}$};
						   
						   \path[->] 
							(a_1) edge [loop above] node {} (a_1)
								edge node {} (b_2)
							(b_2) edge [bend left=15] node {} (c_2)
							(c_2) edge [bend left=15] node {} (b_2)
								edge [loop below] node {} (c_2);
						\end{tikzpicture}
						\caption{Automaton representing state transitions of Program \ref{loop_28_1}}
						\label{loop_28_states3}
					\end{figure}

			\section{First derived cycle}
				\paragraph{}
					Let us begin with the first derived cycle, i.e. Program \ref{loop_28_2}. Clearly $x' = x_0$ and

					\begin{equation}
						y' =
						\left \{
							\begin{array}{ll}
								0 & \mbox{if } x_0 > 0 \land y_0 < 0; \\
								y_0 & \mbox{if } x_0 \le 0 \lor y_0 \ge 0.
							\end{array}
						\right.
					\end{equation}

					\begin{lstlisting}[float,label={loop_28_2},caption={First loop of Program \ref{loop_28_1}}]
// First derived loop.
while (x > 0 && y < 0) {
	// State A_1.
	y = y + 1;
}
					\end{lstlisting}

			\subsubsection{Second derived cycle}
				\paragraph{}
					Now we will analyze the second derived loop, i.e. Program \ref{loop_28_3}. Note that the notation and the iteration counter will be reseted, i.e. $y_0$ will denote value of $y$ after the first derived cycle.

					\begin{lstlisting}[float,label={loop_28_3},caption={Second loop of Program \ref{loop_28_1}}]
// Second derived loop.
while (x > 0) {
	if (y == 0) {
		// State B_2.
		x = x + 2;
		y = 3;
	}
	else {
		// State C_2.
		x = x - y;
		y = y - 1;
	}
}
					\end{lstlisting}

				\paragraph{}
					Let us assume that $x_0 > 0$ (otherwise it would be trivial). Let us begin with the case $y_0 = 0$. Then we start in state $\mathbf{B_2}$, and thus $y_1 = 3$. Hence in the second iteration, we will be in $\mathbf{C_2}$. Clearly, to which state we will go depends purely on $y$, which is in state $\mathbf{C_2}$ affected only by $y = y - 1$. Therefore $y > 3$ will hold for exactly $3$ iterations. In the fourth iteration, we will be back in state $\mathbf{B_2}$ and because states depend only on $y$, which is now reseted to the starting value (i.e. $y_4 = 0$), we know that this pattern (i.e. $\mathbf{B_2C_2C_2C_2}$) will repeated as long as $x > 0$.

				\paragraph{}
					Let us now consider the second case, i.e. $y_0 > 0$. Note that $y_0 < 0$ is not possible, since after the first derived cycle $y$ will be non-negative. Clearly, we will start in state $\mathbf{C_2}$ and stay there for $y$ iterations (assuming that the condition of the $\mathbf{while}$ cycle will not be violated earlier). After that $y$ will be zero, and thus we will go to state $\mathbf{B_2}$. Then we will observe repetition of pattern $\mathbf{B_2C_2C_2C_2}$ as in the previous case, i.e. in the case $y_0 = 0$.

				\paragraph{}
					Because we can stay in state $\mathbf{C_2}$ longer than for 3 iterations only at the beginning, we can extract that part from the second derived loop (similarly as we extracted the first derived loop from the original loop). The benefit is that we will gain extra information about the state transitions. By doing this we will obtain Program \ref{loop_28_8} which is equivalent to Program \ref{loop_28_3}. Hence it is easy to see that the original Program \ref{loop_28} is equivalent to Program \ref{loop_28_9}. Note that since we split the second derived cycle into the third and fourth derived loops, we will not use the second derived cycled anymore.

					\begin{lstlisting}[float,label={loop_28_8},caption={Equivalent program to Program \ref{loop_28_3}.}]
// Third derived loop.
while (x > 0 && y > 0) {
	// State C_3.
	x = x - y;
	y = y - 1;
}

// Fourth derived loop.
while (x > 0) {
	if (y == 0) {
		// State B_4.
		x = x + 2;
		y = 3;
	}
	else {
		// State C_4.
		x = x - y;
		y = y - 1;
	}
}
					\end{lstlisting}

					\begin{lstlisting}[float,label={loop_28_9},caption={Equivalent program to Program \ref{loop_28} and Program \ref{loop_28_1}.}]
// First derived loop.
while (x > 0 && y < 0) {
	// State A_1.
	y = y + 1;
}

// Third derived loop.
while (x > 0 && y > 0) {
	// State C_3.
	x = x - y;
	y = y - 1;
}

// Fourth derived loop.
while (x > 0) {
	if (y == 0) {
		// State B_4.
		x = x + 2;
		y = 3;
	}
	else {
		// State C_4.
		x = x - y;
		y = y - 1;
	}
}
					\end{lstlisting}

				\paragraph{}
					We analyzed possible transitions of the states at the beginning. By further analysis of the second derived cycle we have discovered that not all state combinations are possible. For instance, pattern $\mathbf{B_2C_2B_2}$ is impossible. Hence we can update the automaton representing the state transition from Figure \ref{loop_28_states} to Figure \ref{loop_28_states2}.

					\begin{figure}
						\centering
						\begin{tikzpicture} 

						\node[state,initial] (a_1)   {$\mathbf{A_1}$}; 
						\node[state,initial] (b_4) [below left=of a_1] {$\mathbf{B_4}$}; 
						\node[state,initial right,accepting] (c_3) [below right=of a_1] {$\mathbf{C_3}$};
						\node[state,accepting] (c_4^1) [below right=of b_4] {$\mathbf{C_4^1}$};
						\node[state,accepting] (c_4^2) [below left=of c_4^1] {$\mathbf{C_4^2}$};
						\node[state,accepting] (c_4^3) [below left=of b_4] {$\mathbf{C_4^3}$};

						\path[->] 
							(a_1) edge [loop above] node {} (a_1)
								edge node {} (b)
							(b_4) edge node {} (c_4^1)
							(c_3) edge [loop above] node {} (b_3)
							(c_3) edge node {} (b_4)
							(c_4^1) edge node {} (c_4^2)
							(c_4^2) edge node {} (c_4^3)
							(c_4^3) edge node {} (b_4);

						\end{tikzpicture}
						\caption{Automaton representing state transitions of Program \ref{loop_28_9}. We used superscript to distinguish states representing the same part of the code.}
						\label{loop_28_states2}
					\end{figure}

			\section{Third derived cycle}
				\paragraph{}
					Now we will consider the third derived cycle, i.e. Program \ref{loop_28_5}. Let as assume that $x_0 > 0$ and $y_0 > 0$ (otherwise it is trivial).

					\begin{lstlisting}[float,label={loop_28_5},caption={Third derived loop of Program \ref{loop_28_9}}]
// Third derived loop.
while (x > 0 && y > 0) {
	// State C_3.
	x = x - y;
	y = y - 1;
}
					\end{lstlisting}

				\paragraph{}
					It obviously terminates (e.g. because $y$ decreases in each iteration). In order to compute $x'$, we need to determine because of which condition it terminates. Either the loop terminated because of condition $x > 0$ or because of condition $y > 0$. After $n$ iterations (assuming that the condition has been satisfied) $y_n = y_0 - n$ and $x_n = x_0 - ny_0 + (0 + 1 + \dots + (n-1)) = x_0 - ny_0 + \frac{n(n-1)}{2} = \frac{1}{2}n^2 - (y_0 + \frac{1}{2})n + x_0$. Let us assume that $n$ is the number of iterations, after which the loop terminates. 

				\paragraph{}
					By solving quadratic equation $\frac{1}{2}n^2 - (y_0 + \frac{1}{2})n + x_0 = 0$ (where $n$ is the variable) we obtain (possibly imaginary) solutions $n_1 = y_0 + \frac{1}{2} - \sqrt{d}$ and $n_2 = y_0 + \frac{1}{2} + \sqrt{d}$, where $d = (y_0 + \frac{1}{2})^2 - 2x_0$ is the discriminant. Now it is easy to see that $0 \ge x_n = \frac{1}{2}n^2 - (y_0 + \frac{1}{2})n + x_0$ holds if and only if $d \ge 0$ and $n \in [n_1, n_2]$. Because $n$ denotes number of iterations after which the loop terminates, we know that condition $x_i > 0$ was satisfied for all $i \in \{0, \dots, n-1\}$, and therefore $i \notin [n_1, n_2]$. In other words $n$ must be the smallest integer in $[n_1, n_2]$. We have shown that $x_n \le 0$ holds if and only if $n = \lceil n_1 \rceil \le n_2$.

				\paragraph{}
					Let us first assume that $n \ne \lceil n_1 \rceil$. Then $x_n > 0$, and thus $y_n \le 0$, since we assumed that the forth derived cycle terminates after $n$ iterations. Let us now consider the second case and assume that $n = \lceil n_1 \rceil$.

				\paragraph{}
					Let us first assume that $d \ge \frac{1}{4}$. Then $n_1$ and $n_2$ are obviously real numbers. We want to prove that $\lceil n_1 \rceil \le n_2$. Because $y_0$ is an integer, $\lceil n_1 \rceil \le n_2$ holds if and only if $\bigl \lceil \frac{1}{2} - \sqrt{d} \bigr \rceil \le \frac{1}{2} + \sqrt{d}$. Our assumption $d \ge \frac{1}{4}$ implies that $\sqrt{d} \ge \frac{1}{2}$, and thus $\bigl \lceil \frac{1}{2} - \sqrt{d} \bigr \rceil \le 0 < 1 \le \frac{1}{2} + \sqrt{d}$. Hence $\lceil n_1 \rceil \le n_2$, and therefore $0 \ge x_n$.

				\paragraph{}
					Let us now assume that $d \in [0, \frac{1}{4})$. Thus values $n_1$ and $n_2$ are real numbers. We aim to prove $\lceil n_1 \rceil > n_2$. Again, this is equivalent to $\bigl \lceil \frac{1}{2} - \sqrt{d} \bigr \rceil > \frac{1}{2} + \sqrt{d}$. Because $d \in [0, \frac{1}{4})$, we know that $\sqrt{d} \in [0, \frac{1}{2})$. From this it follows that $\bigl \lceil \frac{1}{2} - \sqrt{d} \bigr \rceil = 1 > \frac{1}{2} + \sqrt{d}$. Hence we have shown that $\lceil n_1 \rceil > n_2$, which implies that $x_n > 0$. Therefore $y_n \le 0$.

				\paragraph{}
					Finally, let us assume that $d < 0$. In this case the inequation has no real (and thus no integer) solution. Therefore $x_n \le 0$ cannot be satisfied, which implies $y_n \le 0$.

				\paragraph{}
					Let us summarize this. We have proved that $x' = \frac{1}{2}n^2 - (y_0 + \frac{1}{2})n + x_0$ and $y' = y_0 - n$, where
					\begin{equation}
						n =
						\left \{
							\begin{array}{ll}
								\lceil y_0 + \frac{1}{2} - \sqrt{d} \rceil & \mbox{if } d \ge \frac{1}{4}, \\
								y_0 & \mbox{if } d < \frac{1}{4},
							\end{array}
						\right.
					\end{equation}
					where $d = (y_0 + \frac{1}{2})^2 - 2x_0$.

			\section{Fourth derived cycle}
				\paragraph{}
					Finally, we will analyze the fourth derived cycle, i.e. Program \ref{loop_28_6}. Let as assume that $x_0 > 0$ (otherwise it is trivial). If $y$ was negative when it entered the original loop, then it was set to zero by the first derived cycle and unmodified by the third derived cycle. If $y$ was initially positive, then it was not affected by the first derived cycle and the third derived cycle set it to zero. Hence we can additionally assume that $y_0 = 0$.

				\begin{lstlisting}[float,label={loop_28_6},caption={Fourth derived loop of Program \ref{loop_28_9}}]
// Fourth derived loop.
while (x > 0) {
	if (y == 0) {
		// State B_4.
		x = x + 2;
		y = 3;
	}
	else {
		// State C_4.
		x = x - y;
		y = y - 1;
	}
}
				\end{lstlisting}

				\paragraph{}
					We should make sure that the loop will terminate. For contradiction, let us assume the opposite, therefore $x > 0$ will hold forever. Therefore we can aggregate the statements from states $\mathbf{B_4}$ and $\mathbf{C_4}$ and create Program \ref{loop_28_7}. Program \ref{loop_28_7} is clearly equivalent to setting $y$ to $0$ and subtracting from $x$ constant $4$ ($2 - 3 - 2 - 1$) in each iteration. Obviously $x$ must become negative at some point, which is a contradiction. Hence the cycle will terminate.

				\begin{lstlisting}[float,label={loop_28_7},caption={Combination of state $\mathbf{B_4}$ and three states $\mathbf{C_4}$ in a row.}]
while (x > 0) {
	x = x + 2;
	y = 3;
	x = x - y;
	y = y - 1;
	x = x - y;
	y = y - 1;
	x = x - y;
	y = y - 1;
}
				\end{lstlisting}

				\paragraph{}
					Since $y_0 = 0$, we start in state $\mathbf{B_4}$ and (as we discussed above) pattern $\mathbf{B_4 C_4 C_4 C_4}$ will be repeated as long as $x > 0$. More formally, pattern $\mathbf{B_4C_4C_4C_4}$ will repeat $r$ times, where $r \in \{0, 1, \dots\}$, after which state $\mathbf{C_4}$ will be visited $s$ times, where $s \in \{1, 2, 3\}$. Note that the superscript in Figure \ref{loop_28_states3} corresponds to $s$. Clearly, for number of iterations $n$ holds $n = 4r + s$.

				\paragraph{}
					Let us first consider $s = 1$. It is easy to see that $y' = 2$. In this case $x' = x_0 + (2 - 3 - 2 - 1) \cdot r + 2 - 3 = x_0 - 4r - 1 \le 0$. Furthermore, we know that the loop did not terminate earlier. This implies that when state $\mathbf{C_4}$ was visited the last time (i.e. when $s = 3$), $x$ was positive. Formally, $x_0 - 4(r-1) + 2 - 3 - 2 - 1 = x_0 - 4r > 0$. From this it follows that $x_0 - 4r = 1$. Because 

				\paragraph{}
					Let us now assume $s = 2$. Obviously $y' = 1$ and $x' = x_0 - 4r + 2 - 3 - 2 = x_0 - 4r - 3 \le 0$. Since the loop did not terminate earlier, we know that $x_0 - 4r - 1 > 0$, hence $x_0 - 4r \in \{2, 3\}$.

				\paragraph{}
					Finally, let us assume $s = 3$. Clearly $y' = 0$ and $x' = x_0 - 4r + 2 - 3 - 2 - 1 = x_0 -4r - 4 \le 0$. Since $x > 0$ was satisfied in all the previous iterations, we know that $x' = x_0 -4r - 3 > 0$, and therefore $x_0 - 4r -4 = 0$.

				\paragraph{}
					Let us summarize this part.

					\begin{itemize}
						\item $s = 1 \implies x_0 - 4r = 1 \implies x_0 \equiv 1\ (\textrm{mod}\ 4)$.
						\item $s = 2 \implies x_0 - 4r \in \{2, 3\} \implies x_0 \equiv 2 \lor x_0 \equiv 3\ (\textrm{mod}\ 4)$.
						\item $s = 3 \implies x_0 - 4r = 4 \implies x_0 \equiv 0\ (\textrm{mod}\ 4)$.
					\end{itemize}

				\paragraph{}
					With our assumptions ($x_0 > 0 \land y_0 = 0$) the loop will always terminate and $s \in \{1, 2, 3\}$. Therefore, the implications hold also in the opposite direction. More precisely, the following holds.

					\begin{itemize}
						\item $s = 1 \iff x_0 \equiv 1\ (\textrm{mod}\ 4)$.
						\item $s = 2 \iff x_0 \equiv 2 \lor x_0 \equiv 3\ (\textrm{mod}\ 4)$.
						\item $s = 3 \iff x_0 \equiv 0\ (\textrm{mod}\ 4)$.
					\end{itemize}

				\paragraph{}
					Finally, we can express

					\begin{equation}
						x' =
						\left \{
							\begin{array}{ll}
								-1 & \mbox{if } x_0 \equiv 2\ (\textrm{mod}\ 4) \\
								0 & \mbox{if } x_0 \not \equiv 2 \ (\textrm{mod}\ 4)
							\end{array}
						\right.
					\end{equation}

					and 

					\begin{equation}
						y' =
						\left \{
							\begin{array}{ll}
								2 & \mbox{if } x_0 \equiv 1\ (\textrm{mod}\ 4) \\
								1 & \mbox{if } x_0 \equiv 2 \lor x_0 \equiv 3 \ (\textrm{mod}\ 4) \\
								0 & \mbox{if } x_0 \equiv 0 \ (\textrm{mod}\ 4)
							\end{array}
						\right.
					\end{equation}.

			\section{Synthesis}
				\paragraph{}
					We have summarized all the derived loops. Now we can combine the results and summarize the original loop. Because there are a lot of options, we have decided to show the results only in code (see Program \ref{loop_28_10}).
				
				\begin{lstlisting}[float,label={loop_28_10},caption={Summarized version of Program \ref{loop_28}}]
// First derived loop.
if (x > 0 && y < 0) {
	y = 0
}

// Third derived loop.
if (x > 0 && y > 0) {
	d = (y + 0.5)^2 - 2*x 
	if (d < 0.25) {
		n = y
	}
	else {
		n = ceil(y + 0.5 - sqrt(d))
	}
	x = 0.5*n^2 - (y + 0.5)*n + x
	y = y - n
}

// Fourth derived loop.
if (x > 0) {
	if (x % 4 == 0) {
		x = 0
		y = 0
	}
	else if (x % 4 == 1) {
		x = 0
		y = 2
	}
	else if (x % 4 == 2) {
		x = -1
		y = 1
	}
	else {
		x = 0
		y = 1
	}
}
				\end{lstlisting}
			
			\subsubsection{Floats}
				\paragraph{}
					The summarized loop, i.e. Program \ref{loop_28_10}, is equivalent to Program \ref{loop_28} assuming that real arithmetics is precise. Since real computers violate this assumption, it would be better to use only integer arithmetics.

				\paragraph{}
					We can start by $d$. Note that the following indices will be in the context of the third derived cycle. Instead of $d$ we will define $D = (2y_0 + 1)^2 - 8x_0 = 4d$. Then we can replace condition $d < \frac{1}{4}$ by equivalent condition $D < 1$.

				\paragraph{}
					Unfortunately, we have not managed to remove real arithmetics from the definition of $n$. Since one of the application of loop summarization is verification, we cannot output a program, which will not be equivalent to the input program in case of rounding issues. However, once we obtain $n$ using the real formula, we can check whether it is correct. In our case it is correct if and only if $x_n \le 0$, $x_{n-1} > 0$ and $y_0 \ge n$. If it is not correct, we can stop and raise an error. Optionally, it would be possible to check near values of the computed $n$. For instance, we could check $n-1$ and $n+1$ (note that this would not need a cycle).

				\paragraph{}
					If $D < 1$, then $n = x_0$, and thus we can simplify definition of $x'$ to $x' = \frac{1}{2}n^2 - (n + \frac{1}{2})n + x_0 = x_0 - \frac{1}{2}(n + 1)n$, which can still be computed precisely in integer arithmetics.

				\paragraph{}
					We present the improved version of the summarized program in Program \ref{loop_28_11}.

				\begin{lstlisting}[float,label={loop_28_11},caption={Improved summarized version of Program \ref{loop_28}}]
// First derived loop.
if (x > 0 && y < 0) {
	y = 0
}

// Third derived loop.
if (x > 0 && y > 0) {
	D = (2*y + 1)^2 - 8*x 
	if (D < 1) {
		n = y
		x = x - n*(n + 1)/2
	}
	else {
		n = ceil(y + 0.5 - sqrt(0.25*D))
		x2 = 0.5*(n-1)^2 - (y + 0.5)*(n-1) + x
		x = 0.5*n^2 - (y + 0.5)*n + x
		if (x > 0 || x2 <= 0 || y < n) {
			raise error
		}
	}
	y = y - n
}

// Fourth derived loop.
if (x > 0) {
	if (x % 4 == 0) {
		x = 0
		y = 0
	}
	else if (x % 4 == 1) {
		x = 0
		y = 2
	}
	else if (x % 4 == 2) {
		x = -1
		y = 1
	}
	else {
		x = 0
		y = 1
	}
}
				\end{lstlisting}

			\section{Observations}
				\paragraph{}
					Let us summarized the process. We started by creating an automaton, whose states corresponded to branches of the original loop. We focused on state transitions and realized that state $\mathbf{A}$ cannot be reached from other states. This allowed us to move the related branch in its own cycle before the main cycle. We would probably be able to analyze the program even without this step, but it simplifies it and makes the whole process more modular.

				\paragraph{}
					The crucial step was the analysis of the second derived cycle, in which we discovered that not all paths in the automaton from Figure \ref{loop_28_states} are actually possible in our program. Using this information we were able break the second derived cycle into two new loops, which we could then analyze more easily. Notice that from some perspective we recursively repeated the technique of splitting loops. The result was (more) precise automaton (see \ref{loop_28_states2}).

				\paragraph{}
					To summarize the third derived loop, we expressed the variables using the number of iterations. We managed to express the number of iterations using real arithmetics and we determined because of which condition the $\mathbf{while}$ cycle terminated. We improved our program to prevent incorrect summarization caused by imprecise real arithmetics.

				\paragraph{}
					In the fourth derived cycle, we used the fact that the states are repeated in a certain pattern. This allowed us to analyze each option separately. We also discovered that $y$ does depend only on $x$. In other words value $y$ produced by the previous parts of the program was at this point irrelevant (it was only used to compute value of $x$ at the beginning of the fourth derived loop).

				\paragraph{}
					It is not necessary to create a switch statement with complicated conditions. For the purpose of loop summarization it is fully sufficient to replace cycles.
